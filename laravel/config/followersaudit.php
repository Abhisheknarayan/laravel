<?php

	return [

		'pricing' => [
			'31791' => 'Monthly free',
			'31792' => 'Yearly free',
			'31793' => 'Monthly Basic',
			'31794' => 'Yearly Basic',
			'31795' => 'Monthly Professional',
			'31796' => 'Yearly Professional',
			'31797' => 'Monthly Corporate',
			'31798' => 'Yearly Corporate'
		] 

	];