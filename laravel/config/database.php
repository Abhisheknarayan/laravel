<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],
        'sqlite_testing' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE_TESTING', database_path('testing.database.sqlite')),
            'prefix' => '',
        ],
        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
    	'mysql1' => [
			'driver' => 'mysql',
			'host' => env('DB_HOST1', '127.0.0.1'),
			'port' => env('DB_PORT1', '3306'),
			'database' => env('DB_DATABASE1', 'followanalysis'),
			'username' => env('DB_USERNAME1', 'root'),
			'password' => env('DB_PASSWORD1', 'arjun'),
			'unix_socket' => env('DB_SOCKET', ''),
			'charset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix' => '',
			'strict' => true,
			'engine' => null,
  		],
        'mysql2' => [
            'driver'        => 'mysql',
            'host'          => env('DB_HOST2', '127.0.0.1'),
            'port'          => env('DB_PORT2', '3306'),
            'database'      => env('DB_DATABASE2', 'followersaudit'),
            'username'      => env('DB_USERNAME2', 'superuser'),
            'password'      => env('DB_PASSWORD2', '1234'),
            'unix_socket'   => env('DB_SOCKET2', ''),
            'charset'       => 'utf8',
            'collation'     => 'utf8_unicode_ci',
            'prefix'        => '',
            'strict'        => true,
            'engine'        => null,
        ],
        'mysql3' => [
            'driver'        => 'mysql',
            'host'          => env('DB_HOST3', '127.0.0.1'),
            'port'          => env('DB_PORT3', '3306'),
            'database'      => env('DB_DATABASE3', 'followersearch'),
            'username'      => env('DB_USERNAME3', 'superuser'),
            'password'      => env('DB_PASSWORD3', '1234'),
            'unix_socket'   => env('DB_SOCKET3', ''),
            'charset'       => 'utf8',
            'collation'     => 'utf8_unicode_ci',
            'prefix'        => '',
            'strict'        => true,
            'engine'        => null,
        ],
        'mysql_tmh' => [
            'driver'        =>  'mysql',
            'host'          =>  env('DB_HOST_TMH', '127.0.0.1'),
            'port'          =>  env('DB_PORT_TMH', '3306'),
            'database'      =>  env('DB_DATABASE_TMH', 'trackmyhashtag'),
            'username'      =>  env('DB_USERNAME_TMH', 'root'),
            'password'      =>  env('DB_PASSWORD_TMH', 'arjun'),
            'unix_socket'   =>  env('DB_SOCKET_TMH', ''),
            'charset'       =>  'utf8',
            'collation'     =>  'utf8_unicode_ci',
            'prefix'        =>  '',
            'strict'        =>  true,
            'engine'        =>  null,
        ],
        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],
        'mongodb' => [
    		'driver'   => 'mongodb',
    		'host'     => env('MONGO_DB_HOST', 'localhost'),
    		'port'     => env('MONGO_DB_PORT', 27017),
    		'database' => env('MONGO_DB_DATABASE','tweetkitapi'),
    		'username' => env('MONGO_DB_USERNAME',''),
    		'password' => env('MONGO_DB_PASSWORD',''),
				'options'  => [
    			'database' => env('MONGO_AUTH_DB','admin') // sets the authentica`tion database required by mongo 3
    		]
    	],
    	'twitteraudit' => [
            'driver'   => 'mongodb',
            'host'     => env('MONGO_DB_HOST_TAU', 'localhost'),
            'port'     => env('MONGO_DB_PORT_TAU', 27017),
            'database' => env('MONGO_DB_DATABASE_TAU','twitteraudit'),
            'username' => env('MONGO_DB_USERNAME_TAU',''),
            'password' => env('MONGO_DB_PASSWORD_TAU',''),
            'options'  => [
                    'database' => env('MONGO_AUTH_DB_TAU','admin') // sets the authentica`tion database required by mongo 3
            ]
        ],
        'twitteraudittrack' => [
            'driver'   => 'mongodb',
            'host'     => env('MONGO_DB_HOST_TRK', 'localhost'),
            'port'     => env('MONGO_DB_PORT_TRK', 27017),
            'database' => env('MONGO_DB_DATABASE_TRK','twitteraudittrack'),
            'username' => env('MONGO_DB_USERNAME_TRK',''),
            'password' => env('MONGO_DB_PASSWORD_TRK',''),
            'options'  => [
                    'database' => env('MONGO_AUTH_DB_TRK','admin') // sets the authentica`tion database required by mongo 3
            ]
        ],		
    	'mongodbut' => [
    		'driver'   => 'mongodb',
    		'host'     => env('MONGO_DB_HOST_UT', 'localhost'),
    		'port'     => env('MONGO_DB_PORT_UT', 27017),
    		'database' => env('MONGO_DB_DATABASE_UT','tweetkitapi_ut'),
    		'username' => env('MONGO_DB_USERNAME_UT',''),
    		'password' => env('MONGO_DB_PASSWORD_UT',''),
    		'options'  => [
    				'database' => env('MONGO_AUTH_DB_UT','admin') // sets the authentica`tion database required by mongo 3
    		]
    	],
        'mongodbuserdetails' => [
            'driver'   => 'mongodb',
            'host'     => env('MONGO_DB_HOST_UD', 'localhost'),
            'port'     => env('MONGO_DB_PORT_UD', 27017),
            'database' => env('MONGO_DB_DATABASE_UD','twitter_users'),
            'username' => env('MONGO_DB_USERNAME_UD',''),
            'password' => env('MONGO_DB_PASSWORD_UD',''),
            'options'  => [
                'database' => env('MONGO_AUTH_DB_UD','admin') // sets the authentication database required by mongo 3
            ]
        ],
        'mongodbfsreport' => [ // followersearch report
            'driver'   => 'mongodb',
            'host'     => env('MONGO_DB_HOST_FS', 'localhost'),
            'port'     => env('MONGO_DB_PORT_FS', 27017),
            'database' => env('MONGO_DB_DATABASE_FS','mongodb'),
            'username' => env('MONGO_DB_USERNAME_FS',''),
            'password' => env('MONGO_DB_PASSWORD_FS',''),
            'options'  => [
                'database' => env('MONGO_AUTH_DB_FS','admin') // sets the authentication database required by mongo 3
            ]
        ],
        'mongodbfstracker' => [
            'driver'   => 'mongodb',
            'host'     => env('MONGO_DB_HOST_FS_TRK', 'localhost'),
            'port'     => env('MONGO_DB_PORT_FS_TRK', 27017),
            'database' => env('MONGO_DB_DATABASE_FS_TRK','followersearchdb'),
            'username' => env('MONGO_DB_USERNAME_FS_TRK',''),
            'password' => env('MONGO_DB_PASSWORD_FS_TRK',''),
            'options'  => [
                'database' => env('MONGO_AUTH_DB_FS_TRK','admin')
            ]
        ],
        'historicaldb' => [
            'driver'   => 'mongodb',
            'host'     => env('MONGO_DB_HOST_HS_TW', 'localhost'),
            'port'     => env('MONGO_DB_PORT_HS_TW', 27017),
            'database' => env('MONGO_DB_DATABASE_HS_TW','historicaldb'),
            'username' => env('MONGO_DB_USERNAME_HS_TW',''),
            'password' => env('MONGO_DB_PASSWORD_HS_TW',''),
            'options'  => [
                'database' => env('MONGO_AUTH_DB_HS_TW','admin')
            ]
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
