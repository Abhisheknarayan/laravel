<?php

	return [

		'pricing' => [
			'41791' => 'Monthly free',
			'41792' => 'Yearly free',
			'41793' => 'Monthly Basic',
			'41794' => 'Yearly Basic',
			'41795' => 'Monthly Professional',
			'41796' => 'Yearly Professional',
			'41797' => 'Monthly Enterprise',
			'41798' => 'Yearly Enterprise'
		],
		'link' 	=> env('FOLLOWERSEARCH_LINK','https://www.followersearch.com/'),
	];