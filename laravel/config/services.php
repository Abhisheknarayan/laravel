<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
	'twitter' => [
		'client_id' => env('TWITTER_CONSUMER_KEY','yHTdi8L1t3vu5xOjEUYNSHq4o'),
		'client_secret' => env('TWITTER_CONSUMER_SECRET','AVwd7Fk45ooMRKTlkDcDpnNFRGU5DrFQaFOl9fbkUd6g59qp4V'),
		'redirect' => env('CALLBACK_URL','')
	],
    'twitter_mpt' => [
        'client_id' => env('MPT_TWITTER_CONSUMER_KEY',''),
        'client_secret' => env('MPT_TWITTER_CONSUMER_SECRET',''),
        'redirect' => env('MPT_CALLBACK_URL','')
    ]
    
];