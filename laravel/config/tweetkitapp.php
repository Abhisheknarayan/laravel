<?php
return [
    'mongo_ip'                      =>  env('MONGO_IP','10.132.138.169'),
    'mongo_ut_ip'                   =>  env('MONGO_UT_IP','10.132.115.194'),
    'tweet_time_diff'				=>	env('tweet_time_diff','86400'),
    'pagination'					=>  env('paginate',20),
	'max_order'						=>  env('order','50'),
	'max_saved_tweets'				=>	env('max_saved_tweets',10),
	'api_twitter_pagination'		=>	env('api_twitter_pagination',200),
	'tweetkit_max_track_tweets'		=>	env('tweetkit_max_track_tweets',6000),
	'tweetkit_min_retweet_count'	=>	env('tweetkit_min_retweet_count',5),
	'tweetkit_min_retweet_time'		=>	env('tweetkit_min_retweet_time',14400),	 //4 hours
	'tweet_update_time'				=>  env('tweet_update_time',10800),
	'rt_max_track_time'				=>	env('rt_max_track_time',43200),  //7 days
	'rt_min_track_time'				=>	env('rt_min_track_time',7200),		  // 2 hour
	'inactive_track_time'			=>	env('inactive_track_time',172800),		//2 days
	'twitter_consumer_key'			=>	env('TWITTER_CONSUMER_KEY',''),
	'twitter_consumer_secret'		=>	env('TWITTER_CONSUMER_SECRET',''),
	'twitter_access_token'			=>	env('TWITTER_ACCESS_TOKEN',''),
	'twitter_access_token_secret'	=>	env('TWITTER_ACCESS_TOKEN_SECRET',''),
	'top_tweets_cat_id'				=>	env('top_tweets_cat_id','5a742dcd0f31f74eaf311292'),
	'news_cat_id'					=>	env('news_cat_id','5a742dd40f31f7539a584732'),
	'business_cat_id'				=>	env('business_cat_id','5a742de30f31f7539e31d273'),	
	'reports_dir'					=>	'/srv/tweetkitapi',
	'zip_file_dir'					=>	'/srv/zip',
	'saved_tweets_dir'				=>	'/srv/savedtweets',
	'email_bcc'						=>	env('email_bcc','arjunjain08@gmail.com'),
	'email_from'					=>	env('email_from','tweetkitapp@gmail.com'),
	'ur_log_path'					=>  storage_path().'/ur-logs',
	'fw_log_path'					=>  storage_path().'/fw-logs',

	'fa_base_url'					=>	env('FA_BASE_URL','https://www.followersanalysis.com'),
	'historical_data'               =>  '/srv/tweetkitapi/historical-data/',
    'historical_data_user_id'       =>  env('HISTORICAL_DATA_USER_ID','5ccbc92c0f31f731ca72d232'),
    'lang'                          =>  array(
                                                'af'=>'Afrikaans',
                                                'af-za'=>'Afrikaans (South Africa)',
                                                'ar'=>'Arabic',
                                                'ar-ae'=>'Arabic (U.A.E.)',
                                                'ar-bh'=>'Arabic (Bahrain)',
                                                'ar-dz'=>'Arabic (Algeria)',
                                                'ar-eg'=>'Arabic (Egypt)',
                                                'ar-iq'=>'Arabic (Iraq)',
                                                'ar-jo'=>'Arabic (Jordan)',
                                                'ar-kw'=>'Arabic (Kuwait)',
                                                'ar-lb'=>'Arabic (Lebanon)',
                                                'ar-ly'=>'Arabic (Libya)',
                                                'ar-ma'=>'Arabic (Morocco)',
                                                'ar-om'=>'Arabic (Oman)',
                                                'ar-qa'=>'Arabic (Qatar)',
                                                'ar-sa'=>'Arabic (Saudi Arabia)',
                                                'ar-sy'=>'Arabic (Syria)',
                                                'ar-tn'=>'Arabic (Tunisia)',
                                                'ar-ye'=>'Arabic (Yemen)',
                                                'az'=>'Azeri (Latin)',
                                                'az-az'=>'Azeri (Cyrillic) (Azerbaijan)',
                                                'be'=>'Belarusian',
                                                'be-by'=>'Belarusian (Belarus)',
                                                'bg'=>'Bulgarian',
                                                'bg-bg'=>'Bulgarian (Bulgaria)',
                                                'bs-ba'=>'Bosnian (Bosnia and Herzegovina)',
                                                'ca'=>'Catalan',
                                                'ca-es'=>'Catalan (Spain)',
                                                'cs'=>'Czech',
                                                'cs-cz'=>'Czech (Czech Republic)',
                                                'cy'=>'Welsh',
                                                'cy-gb'=>'Welsh (United Kingdom)',
                                                'da'=>'Danish',
                                                'da-dk'=>'Danish (Denmark)',
                                                'de'=>'German',
                                                'de-at'=>'German (Austria)',
                                                'de-ch'=>'German (Switzerland)',
                                                'de-de'=>'German (Germany)',
                                                'de-li'=>'German (Liechtenstein)',
                                                'de-lu'=>'German (Luxembourg)',
                                                'dv'=>'Divehi',
                                                'dv-mv'=>'Divehi (Maldives)',
                                                'el'=>'Greek',
                                                'el-gr'=>'Greek (Greece)',
                                                'en'=>'English',
                                                'en-au'=>'English (Australia)',
                                                'en-bz'=>'English (Belize)',
                                                'en-ca'=>'English (Canada)',
                                                'en-cb'=>'English (Caribbean)',
                                                'en-gb'=>'English (United Kingdom)',
                                                'en-ie'=>'English (Ireland)',
                                                'en-jm'=>'English (Jamaica)',
                                                'en-nz'=>'English (New Zealand)',
                                                'en-ph'=>'English (Republic of the Philippines)',
                                                'en-tt'=>'English (Trinidad and Tobago)',
                                                'en-us'=>'English (United States)',
                                                'en-za'=>'English (South Africa)',
                                                'en-zw'=>'English (Zimbabwe)',
                                                'eo'=>'Esperanto',
                                                'es'=>'Spanish',
                                                'es-ar'=>'Spanish (Argentina)',
                                                'es-bo'=>'Spanish (Bolivia)',
                                                'es-cl'=>'Spanish (Chile)',
                                                'es-co'=>'Spanish (Colombia)',
                                                'es-cr'=>'Spanish (Costa Rica)',
                                                'es-do'=>'Spanish (Dominican Republic)',
                                                'es-ec'=>'Spanish (Ecuador)',
                                                'es-es'=>'Spanish (Spain)',
                                                'es-gt'=>'Spanish (Guatemala)',
                                                'es-hn'=>'Spanish (Honduras)',
                                                'es-mx'=>'Spanish (Mexico)',
                                                'es-ni'=>'Spanish (Nicaragua)',
                                                'es-pa'=>'Spanish (Panama)',
                                                'es-pe'=>'Spanish (Peru)',
                                                'es-pr'=>'Spanish (Puerto Rico)',
                                                'es-py'=>'Spanish (Paraguay)',
                                                'es-sv'=>'Spanish (El Salvador)',
                                                'es-uy'=>'Spanish (Uruguay)',
                                                'es-ve'=>'Spanish (Venezuela)',
                                                'et'=>'Estonian',
                                                'et-ee'=>'Estonian (Estonia)',
                                                'eu'=>'Basque',
                                                'eu-es'=>'Basque (Spain)',
                                                'fa'=>'Farsi',
                                                'fa-ir'=>'Farsi (Iran)',
                                                'fi'=>'Finnish',
                                                'fi-fi'=>'Finnish (Finland)',
                                                'fo'=>'Faroese',
                                                'fo-fo'=>'Faroese (Faroe Islands)',
                                                'fr'=>'French',
                                                'fr-be'=>'French (Belgium)',
                                                'fr-ca'=>'French (Canada)',
                                                'fr-ch'=>'French (Switzerland)',
                                                'fr-fr'=>'French (France)',
                                                'fr-lu'=>'French (Luxembourg)',
                                                'fr-mc'=>'French (Principality of Monaco)',
                                                'gl'=>'Galician',
                                                'gl-es'=>'Galician (Spain)',
                                                'gu'=>'Gujarati',
                                                'gu-in'=>'Gujarati (India)',
                                                'he'=>'Hebrew',
                                                'he-il'=>'Hebrew (Israel)',
                                                'hi'=>'Hindi',
                                                'hi-in'=>'Hindi (India)',
                                                'hr'=>'Croatian',
                                                'hr-ba'=>'Croatian (Bosnia and Herzegovina)',
                                                'hr-hr'=>'Croatian (Croatia)',
                                                'hu'=>'Hungarian',
                                                'hu-hu'=>'Hungarian (Hungary)',
                                                'hy'=>'Armenian',
                                                'hy-am'=>'Armenian (Armenia)',
                                                'id'=>'Indonesian',
                                                'id-id'=>'Indonesian (Indonesia)',
                                                'is'=>'Icelandic',
                                                'is-is'=>'Icelandic (Iceland)',
                                                'it'=>'Italian',
                                                'it-ch'=>'Italian (Switzerland)',
                                                'it-it'=>'Italian (Italy)',
                                                'ja'=>'Japanese',
                                                'ja-jp'=>'Japanese (Japan)',
                                                'ka'=>'Georgian',
                                                'ka-ge'=>'Georgian (Georgia)',
                                                'kk'=>'Kazakh',
                                                'kk-kz'=>'Kazakh (Kazakhstan)',
                                                'kn'=>'Kannada',
                                                'kn-in'=>'Kannada (India)',
                                                'ko'=>'Korean',
                                                'ko-kr'=>'Korean (Korea)',
                                                'kok'=>'Konkani',
                                                'kok-in'=>'Konkani (India)',
                                                'ky'=>'Kyrgyz',
                                                'ky-kg'=>'Kyrgyz (Kyrgyzstan)',
                                                'lt'=>'Lithuanian',
                                                'lt-lt'=>'Lithuanian (Lithuania)',
                                                'lv'=>'Latvian',
                                                'lv-lv'=>'Latvian (Latvia)',
                                                'mi'=>'Maori',
                                                'mi-nz'=>'Maori (New Zealand)',
                                                'mk'=>'FYRO Macedonian',
                                                'mk-mk'=>'FYRO Macedonian (Former Yugoslav Republic of Macedonia)',
                                                'mn'=>'Mongolian',
                                                'mn-mn'=>'Mongolian (Mongolia)',
                                                'mr'=>'Marathi',
                                                'mr-in'=>'Marathi (India)',
                                                'ms'=>'Malay',
                                                'ms-bn'=>'Malay (Brunei Darussalam)',
                                                'ms-my'=>'Malay (Malaysia)',
                                                'mt'=>'Maltese',
                                                'mt-mt'=>'Maltese (Malta)',
                                                'nb'=>'Norwegian',
                                                'nb-no'=>'Norwegian (Norway)',
                                                'nl'=>'Dutch',
                                                'nl-be'=>'Dutch (Belgium)',
                                                'nl-nl'=>'Dutch (Netherlands)',
                                                'nn-no'=>'Norwegian (Nynorsk) (Norway)',
                                                'ns'=>'Northern Sotho',
                                                'ns-za'=>'Northern Sotho (South Africa)',
                                                'pa'=>'Punjabi',
                                                'pa-in'=>'Punjabi (India)',
                                                'pl'=>'Polish',
                                                'pl-pl'=>'Polish (Poland)',
                                                'ps'=>'Pashto',
                                                'ps-ar'=>'Pashto (Afghanistan)',
                                                'pt'=>'Portuguese',
                                                'pt-br'=>'Portuguese (Brazil)',
                                                'pt-pt'=>'Portuguese (Portugal)',
                                                'qu'=>'Quechua',
                                                'qu-bo'=>'Quechua (Bolivia)',
                                                'qu-ec'=>'Quechua (Ecuador)',
                                                'qu-pe'=>'Quechua (Peru)',
                                                'ro'=>'Romanian',
                                                'ro-ro'=>'Romanian (Romania)',
                                                'ru'=>'Russian',
                                                'ru-ru'=>'Russian (Russia)',
                                                'sa'=>'Sanskrit',
                                                'sa-in'=>'Sanskrit (India)',
                                                'se'=>'Sami (Northern)',
                                                'se-fi'=>'Sami (Inari) (Finland)',
                                                'se-no'=>'Sami (Southern) (Norway)',
                                                'se-se'=>'Sami (Southern) (Sweden)',
                                                'sk'=>'Slovak',
                                                'sk-sk'=>'Slovak (Slovakia)',
                                                'sl'=>'Slovenian',
                                                'sl-si'=>'Slovenian (Slovenia)',
                                                'sq'=>'Albanian',
                                                'sq-al'=>'Albanian (Albania)',
                                                'sr-ba'=>'Serbian (Cyrillic) (Bosnia and Herzegovina)',
                                                'sr-sp'=>'Serbian (Cyrillic) (Serbia and Montenegro)',
                                                'sv'=>'Swedish',
                                                'sv-fi'=>'Swedish (Finland)',
                                                'sv-se'=>'Swedish (Sweden)',
                                                'sw'=>'Swahili',
                                                'sw-ke'=>'Swahili (Kenya)',
                                                'syr'=>'Syriac',
                                                'syr-sy'=>'Syriac (Syria)',
                                                'ta'=>'Tamil',
                                                'ta-in'=>'Tamil (India)',
                                                'te'=>'Telugu',
                                                'te-in'=>'Telugu (India)',
                                                'th'=>'Thai',
                                                'th-th'=>'Thai (Thailand)',
                                                'tl'=>'Tagalog',
                                                'tl-ph'=>'Tagalog (Philippines)',
                                                'tn'=>'Tswana',
                                                'tn-za'=>'Tswana (South Africa)',
                                                'tr'=>'Turkish',
                                                'tr-tr'=>'Turkish (Turkey)',
                                                'tt'=>'Tatar',
                                                'tt-ru'=>'Tatar (Russia)',
                                                'ts'=>'Tsonga',
                                                'uk'=>'Ukrainian',
                                                'uk-ua'=>'Ukrainian',
                                                'ur'=>'Urdu',
                                                'ur-pk'=>'Urdu',
                                                'uz'=>'Uzbek (Latin)',
                                                'uz-uz'=>'Uzbek (Uzbekistan)',
                                                'vi'=>'Vietnamese',
                                                'vi-vn'=>'Vietnamese (Viet Nam)',
                                                'xh'=>'Xhosa',
                                                'xh-za'=>'Xhosa (South Africa)',
                                                'zh'=>'Chinese',
                                                'zh-cn'=>'Chinese (S)',
                                                'zh-hk'=>'Chinese (Hong Kong)',
                                                'zh-mo'=>'Chinese (Macau)',
                                                'zh-sg'=>'Chinese (Singapore)',
                                                'zh-tw'=>'Chinese (T)',
                                                'zu'=>'Zulu',
                                                'zu-za'=>'Zulu (South Africa)'
                                            ),
    'trends'						=>	array(
												'5a72f26e5d622906077e8a51'	=>	array(
														'India'		=>	"23424848",
														"Ahmedabad"	=>	"2295402",
														"Amritsar"	=>	"2295388", 
														"Bangalore"	=>	"2295420",
														"Bhopal"	=>	"2295407",
														"Chennai"	=>	"2295424",
														"Delhi"		=>	"20070458",
														"Hyderabad"	=>	"2295414",
														"Indore"	=>	"2295408", 
														"Jaipur"	=>	"2295401",
														"Kanpur"	=>	"2295378", 
														"Kolkata"	=>	"2295386", 
														"Lucknow"	=>	"2295377",
														"Mumbai"	=>	"2295411",
														"Nagpur"	=>	"2282863",
														"Patna"		=>	"2295381",
														"Pune"		=>	"2295412", 
														"Rajkot"	=>	"2295404",
														"Ranchi"	=>	"2295383",
														"Srinagar"	=>	"2295387",
														"Surat"		=>	"2295405",
														"Thane"		=>	"2295410",
												),
												'5a72f2875d6229060b3fdcd2'	=>	array(
														'United States'		=>	'23424977',		
														"Albuquerque"		=>	"2352824",
														"Atlanta"			=>	"2357024",
														"Austin"			=>	"2357536",
														"Baltimore"			=>	"2358820",
														"Baton Rouge"		=>	"2359991",
														"Birmingham"		=>	"2364559",
														"Boston"			=>	"2367105",
														"Charlotte"			=>	"2378426",
														"Chicago"			=>	"2379574",
														"Cincinnati"		=>	"2380358",
														"Cleveland"			=>	"2381475", 
														"Colorado Springs"	=>	"2383489",
														"Columbus"			=>	"2383660",
														"Dallas-Ft. Worth"	=>	"2388929",
														"Denver"			=>	"2391279",
														"Detroit"			=>	"2391585",
														"El Paso"			=>	"2397816",
														"Fresno"			=>	"2407517",
														"Greensboro"		=>	"2414469",
														"Harrisburg"		=>	"2418046",
														"Honolulu"			=>	"2423945",
														"Houston"			=>	"2424766",
														"Indianapolis"		=>	"2427032",
														"Jackson"			=>	"2428184",
														"Jacksonville"		=>	"2428344",
														"Kansas City"		=>	"2430683",
														"Las Vegas"			=>	"2436704", 
														"Long Beach"		=>	"2441472",
														"Los Angeles"		=>	"2442047",
														"Louisville"		=>	"2442327",
														"Memphis"			=>	"2449323",
														"Mesa"				=>	"2449808",
														"Miami"				=>	"2450022",
														"Milwaukee"			=>	"2451822",
														"Minneapolis"		=>	"2452078",
														"Nashville"			=>	"2457170",
														"New Haven"			=>	"2458410",
														"New Orleans"		=>	"2458833",
														"New York"			=>	"2459115",
														"Norfolk"			=>	"2460389",
														"Oklahoma City"		=>	"2464592",
														"Omaha"				=>	"2465512",
														"Orlando"			=>	"2466256",
														"Philadelphia"		=>	"2471217",
														"Phoenix"			=>	"2471390",
														"Pittsburgh"		=>	"2473224",
														"Portland"			=>	"2475687",
														"Providence"		=>	"2477058",
														"Raleigh"			=>	"2478307",
														"Richmond"			=>	"2480894",
														"Sacramento"		=>	"2486340",
														"Salt Lake City"	=>	"2487610",
														"San Antonio"		=>	"2487796",
														"San Diego"			=>	"2487889",
														"San Francisco"		=>	"2487956",
														"San Jose"			=>	"2488042",
														"Seattle"			=>	"2490383",
														"St. Louis"			=>	"2486982",
														"Tallahassee"		=>	"2503713",
														"Tampa"				=>	"2503863",
														"Tucson"			=>	"2508428",
														"Virginia Beach"	=>	"2512636",
														"Washington"		=>	"2514815",
												),
												'5a72f2815d6229060b3fdcd1'	=>	array(
														"United Kingdom" 	=> 	"23424975",
														"Belfast" 			=> 	"44544",
														"Birmingham" 		=> 	"12723",
														"Blackpool" 		=> 	"12903",
														"Bournemouth" 		=> 	"13383",
														"Brighton" 			=> 	"13911",
														"Bristol" 			=> 	"13963",
														"Cardiff" 			=> 	"15127",
														"Coventry" 			=> 	"17044",
														"Derby" 			=> 	"18114",
														"Edinburgh" 		=> 	"19344",
														"Glasgow" 			=> 	"21125",
														"Hull" 				=> 	"25211",
														"Leeds" 			=> 	"26042",
														"Leicester" 		=> 	"26062",
														"Liverpool" 		=> 	"26734",
														"London" 			=> 	"44418",
														"Manchester" 		=> 	"28218",
														"Middlesbrough" 	=> 	"28869",
														"Newcastle" 		=> 	"30079",
														"Nottingham" 		=> 	"30720",
														"Plymouth" 			=> 	"32185",
														"Portsmouth" 		=> 	"32452",
														"Preston" 			=> 	"32566",
														"Sheffield" 		=> 	"34503",
														"Stoke-on-Trent" 	=>	"36240"
												),
												'5a92b4530f31f76ffc7da8b2'			=>	array(
														'Canada'			=>	"23424775",
														"Calgary" 			=> 	"8775",
														"Edmonton" 			=> 	"8676",
														"Montreal" 			=> 	"3534",
														"Ottawa" 			=> 	"3369",
														"Quebec" 			=> 	"3444",
														"Toronto" 			=> 	"4118",
														"Vancouver"			=> 	"9807",
														"Winnipeg" 			=> 	"2972"
												),	
												'5ad594660f31f702fc41eab2'			=>	array(
														"Indonesia" 		=> 	"23424846",
														"Bandung" 			=> 	"1047180",
														"Bekasi" 			=> 	"1030077",
														"Depok" 			=> 	"1032539",
														"Jakarta" 			=> 	"1047378",
														"Makassar" 			=> 	"1046138",
														"Medan" 			=> 	"1047908",
														"Palembang" 		=> 	"1048059",
														"Pekanbaru" 		=> 	"1040779",
														"Semarang" 			=>	"1048324",
														"Surabaya" 			=> 	"1044316",
														"Tangerang" 		=> 	"1048536"
												),
												'5a72f2795d6229030a1be601'			=>	array(
														'wo'				=>	1,
												)
									)	
		
];