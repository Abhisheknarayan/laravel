<?php
return [
		'Top Tweets'=>'Top Tweets',
		'News'=>'Nieuws',
		'Business'=>'Bedrijf',
		'Politics'=>'Politiek',
		'Entertainment'=>'vermaak',
		'Sports'=>'Sport',
		'Health & Fitness'=>'Gezondheid en fitness',
		'Fashion & Beauty'=>'Mode en schoonheid',
		'Technology'=>'Technologie',
		'Humor'=>'Humor',
		'Food'=>'Eten',
];