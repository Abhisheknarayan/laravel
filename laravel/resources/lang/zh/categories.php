<?php
return [
		'Top Tweets'=>'热门推文',
		'News'=>'新闻',
		'Business'=>'商业',
		'Politics'=>'政',
		'Entertainment'=>'娱乐',
		'Sports'=>'竞技',
		'Health & Fitness'=>'健康与健身',
		'Fashion & Beauty'=>'时尚与美丽',
		'Technology'=>'技术',
		'Humor'=>'幽默',
		'Food'=>'餐饮',
];