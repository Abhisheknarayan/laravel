<?php
return [
		'Top Tweets'=>'Tweet Teratas',
		'News'=>'Berita',
		'Business'=>'Bisnis',
		'Politics'=>'Politik',
		'Entertainment'=>'Hiburan',
		'Sports'=>'Olahraga',
		'Health & Fitness'=>'Kesehatan dan Kebugaran',
		'Fashion & Beauty'=>'Fashion dan Kecantikan',
		'Technology'=>'Teknologi',
		'Humor'=>'Humor',
		'Food'=>'Makanan',
];