<?php
return [
		'Top Tweets'=>'Top Tweets',
		'News'=>'News',
		'Business'=>'Business',
		'Politics'=>'Politics',
		'Entertainment'=>'Entertainment',
		'Sports'=>'Sports',
		'Health & Fitness'=>'Health & Fitness',
		'Fashion & Beauty'=>'Fashion & Beauty',
		'Technology'=>'Technology',
		'Humor'=>'Humor',
		'Food'=>'Food',
];