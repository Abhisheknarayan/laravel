<?php
return [
		'Top Tweets'=>'Top Tweets',
		'News'=>'Noticias',
		'Business'=>'Negocio',
		'Politics'=>'Política',
		'Entertainment'=>'Entretenimiento',
		'Sports'=>'Deportes',
		'Health & Fitness'=>'salud y estado fisico',
		'Fashion & Beauty'=>'Belleza de la moda',
		'Technology'=>'Tecnología',
		'Humor'=>'Humor',
		'Food'=>'Comida',
];