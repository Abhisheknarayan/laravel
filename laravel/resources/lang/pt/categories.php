<?php
return [
		'Top Tweets'=>'Tweets principais',
		'News'=>'Notícia',
		'Business'=>'O negócio',
		'Politics'=>'Política',
		'Entertainment'=>'Entretenimento',
		'Sports'=>'Esportes',
		'Health & Fitness'=>'Saúde',
		'Fashion & Beauty'=>'Moda e Beleza',
		'Technology'=>'Tecnologia',
		'Humor'=>'Humor',
		'Food'=>'Comida',
];