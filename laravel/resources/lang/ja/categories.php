<?php
return [
		'Top Tweets'=>'トップツイート',
		'News'=>'ニュース',
		'Business'=>'ビジネス',
		'Politics'=>'政治',
		'Entertainment'=>'エンターテインメント',
		'Sports'=>'スポーツの',
		'Health & Fitness'=>'ヘルス＆フィットネス',
		'Fashion & Beauty'=>'ファッション＆美容',
		'Technology'=>'テクノロジー',
		'Humor'=>'ユーモア',
		'Food'=>'食料',
];