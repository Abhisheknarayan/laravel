@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Users
@endsection
@section('contentheader_title')
Users
@endsection
    <div style="margin:10px 0">
    	<span class="label label-success">Total: {{ $total_mobileusers }} users</span>  
    	<span class="label label-primary">Active: {{ $total_active_mobileusers }} users</span>
    	<span class="label label-warning">Premium Users: {{ $total_premium_users }} users</span>
    </div>

    {{ Form::open(['method' => 'post','class'=>'searchform form-inline']) }}
    	<div class="form-group">
    		{{ Form::label('Username', null, ['class' => 'search-username']) }}
    		{{ Form::text('usersearch','', array('placeholder'=>'Enter Username')) }}
    	</div>
    	<div class="form-group">
    		{{ Form::label('Twitter Id', null, ['class' => 'search-id']) }}
    		{{ Form::text('twitter_id','', array('placeholder'=>'Enter Twitter Id')) }}
    	</div>
    	<div class="form-group">
    		<label>Verified</label>
    		{{ Form::select('verified', [''=>'select','1' => 'True', '0' => 'False'],'select', array('class'=>'verified-search-dropdown')) }}
    	</div>
    	<div class="form-group">
    		<label>Premium</label>
    		{{ Form::select('premium', [''=>'select','1' => 'True', '0' => 'False'],'select', array('class'=>'premium-search-dropdown')) }}
    	</div>
    	<div class="btn-group">	
    		{{ Form::submit('search',['class'=>'btn btn-warning']) }}
    		<a href="{{ url('/') }}/mpt/mobileusers"  class = "btn btn-primary">Reset</a>
    	</div>
    {{ Form::close() }}
    
	<?php if($search != '' || $twitter_id !='') : ?>
		<div style="margin:10px 0">Search Result : {{$search_count}} users </div>
	<?php endif?>	   
	
	<div class= "msg"></div>
	<div class="list-table table-responsive" style="margin-top:30px">
   		 <table class="table table-striped text-center">
			  <thead>
    			  <tr>
    			    <th>Twitter_id</th>
    		    	<th>Username</th>
    		    	<th>Created At</th>
    		    	<th>Updated At</th>
    		    	<th>Order Id</th>
    		    	<th>Status</th>
    		    	<th>Premium</th>
    		    	<th></th>
    		     </tr>
    		   </thead>
    		   <tbody>
					{!! $data !!}
        	  </tbody>
		  </table>
</div>	 

<?php 
		$query_arr = array();
		if($search != ''){
			$query_arr['usersearch'] = $search;
		}
		if($verified != ''){
			$query_arr['verified'] = $verified;
	 	}
?>
{{ $mu->appends($query_arr)->links() }}
		
<script type="text/javascript">
$(document).ready(function(){
	
   $(".savebtn").on("click",function(){
	    var id 			=	this.id;
    	var token		= 	$("meta[name='csrf-token']").attr("content");    			
    	var status		=   $("#status_"+id).val();	
    	var is_premium	=	$("#premium_"+id).val();
     
		$(".msg").hide();	
  	    $.ajax({
    	    	type:"POST", 
    	    	headers: {'X-CSRF-TOKEN': token},
    	    	url:"/mpt/ajax/savemobileusers",
    	    	data: {id:id,status:status,is_premium:is_premium},
    	    	success:function(data,textstatus,xhr)
    	    	{ 
    	        	if(xhr.status == 200){
    	   		    	$(".msg").html("Saved Successfully");
    	     		    $(".msg").addClass('alert alert-success')
    	     		    $(".msg").show();
    	     		}
    	        	else if(xhr.status == 201){
    	  		    	$(".msg").html("Error Occured While Saving");
    	  		        $(".msg").addClass('alert alert-danger')
    	  		        $(".msg").show();
    	  		   }     		                
    	        }    		     
  		   });    	    			   	

  	     return false;
      });
     
});
</script>	 		
@endsection