@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Subscriptions
@endsection
@section('contentheader_title')
Subscriptions
@endsection
<div class="table-responsive">
    <table class="table-condensed text-center table table-hover table-striped category-table" id="category-table">
    	<thead>
    		<tr>
    			<th>Twitter Id</th>
    			<th>Country</th>
    			<th>Amount</th>
    			<th>Payment State</th>
    			<th>Start Time</th>
    			<th>Expiry Time</th>
    			<th>Last Update</th>
    			<th>Is Cancel</th>
    		</tr>
    	</thead>
    	<tbody>
    	@foreach($subscriptions as $subs)
    		<tr>
    			<td>{{ 	$subs->twitter_id		}}</td>
    			<td>{{	$subs->country_code		}}</td>
    			<td>{{	($subs->price_amount/1000000) .' '.$subs->price_currency 		}}</td>
    			@if($subs->payment_state == 0)
    				<td>Pending</td>				
    			@elseif($subs->payment_state == 1)
    				<td>Received</td>
    			@elseif($subs->payment_state == 2)
    				<td>Free Trial</td>
    			@endif
    			<td>{{	date('Y-m-d H:i:s',($subs->start_time_millis/1000))	}}</td>
    			<td>{{ 	date('Y-m-d H:i:s',($subs->expiry_time/1000))		}}</td>
    			<td>{{  $subs->updated_at		}}</td>
    			<td>{{ 	$subs->is_cancel		}}</td>
    		</tr>
    	@endforeach
    	</tbody>
    </table>
</div>
{{ $subscriptions->links() }}
@endsection