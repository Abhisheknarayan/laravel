@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Users
@endsection
@section('contentheader_title')
Users
@endsection
<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} users</span>
	<span class="label label-warning">Not Verified: {{ $not_verified }} users</span>
	<span class="label label-danger">Not Active: {{ $not_active }} users</span>
</div>

<div>
	<form class="search form-inline" method="post">
		<input name="_token" value="{{ csrf_token() }}" type="hidden">
		<div class="form-group">
			<label>Verified</label>
			{{ Form::select('verified', array(''=>'Select','1' => 'Verified', '0' => 'Unverified'),'') }}
      	</div>
      	<div class="form-group">
			<label>Active:</label>
			{{ Form::select('is_active', array(''=>'Select','1' => 'Active', '0' => 'InActive'),'') }}
      	</div>
      	<div class="form-group">
			<label>Email:</label>
			<input type="text" name="email" class="input-medium search-query" value="{{ $email }}">
      	</div>
      	<button type="submit" class="btn btn-warning" >Search</button>
      	<a href="{{ url('/') }}/tmh/users"  class = "btn btn-primary">Reset</a>
    </form>
    
    <?php if($verified != '' || $active_account !='' || $email!=''): ?>
    	<div style="margin:10px 0">Search Result : {{$search_count}} users </div>
	<?php endif;?>	   
	<div class="msg"></div>
	<div class="table-responsive" style="margin-top:30px">
        <table class="table table-striped">
    		<thead>
    			<tr>
    				<th>Id</th>
    				<th>Email</th>
    				<th>Last Login</th>
    				<th>Added</th>
    				<th>Verified</th>
    				<th>Status</th>
    				<th></th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($users as $usr)
    				<tr>
    					<td><small>{{ $usr->id }}</small></td>
        				<td><small>{{ $usr->email }}<br />({{ $usr->ip_address }})</small></td>
        				<td><small>{{ date('d-M-y H:i:s',strtotime($usr->last_login_time)) }}</small></td>
        				<td><small>{{ date('d-M-y H:i:s',strtotime($usr->created_at)) }}</small></td>
        				<td><small>{{ Form::select('verified', array('1' => 'Verified', '0' => 'Not Verified'), $usr->verified, array('id'=>'verified'.$usr->id)) }}</small></td>
        				<td><small>{{ Form::select('is_active', array('1' => 'Enable', '0' => 'Disable'), $usr->is_active, array('id'=>'is_active'.$usr->id)) }}</small></td>
        				<td>
            				<div class="btn-group" role="group">
            					<a href="{{ url('/tmh/trackers/') }}?user_id={{ $usr->id }}" class="btn btn-primary btn-sm">Trackers</a>
            					<a href="{{ url('/tmh/transactions/') }}?user_id={{ $usr->id }}" class="btn btn-warning btn-sm">Transactions</a>
            					<a href="{{ url('/tmh/subscriptions/') }}?user_id={{ $usr->id }}" class="btn btn-danger btn-sm">Subscriptions</a>
        				 		{{ Form::submit('Save',array('id'=>$usr->id,'class' => 'savebtn btn btn-success btn-sm')) }}
            				</div>

        				</td>
        			</tr>
    			@endforeach	
    		</tbody>
    	</table>
	</div>
</div>
<?php 
	$query_arr = array();
	if($verified != ''){
		$query_arr['verified'] 	= $verified;
	}
	if($active_account != ''){
	    $query_arr['is_active']    =   $active_account;
	}
	if($email!=''){
	 	$query_arr['email'] = $email;
	}
?>
{{ $users->appends($query_arr)->links() }}
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	$(".savebtn").on('click',function(){
   		var id 			=	$(this).attr('id');   	
    	var token		= 	$("meta[name='csrf-token']").attr("content");    			
    	var verified	=   $("#verified"+id).val();
    	var is_active	=	$("#is_active"+id).val();	
    	$(".msg").hide();	
  	    $.ajax({
	       	type:"POST", 
        	headers: {'X-CSRF-TOKEN': token},
        	url:"/tmh/ajax/userssave",
        	data: {id:id,verified:verified,is_active:is_active},
        	success:function(data,textstatus,xhr)
    	   	{ 
    	       	if(xhr.status == 200){
    		    	$(".msg").html("Saved Successfully");
    	  		    $(".msg").addClass('alert alert-success')
    	  		    $(".msg").show();
    		    }
    	       	else if(xhr.status == 201){
    	 		  	$(".msg").html("Error Occured While Saving");
    		        $(".msg").addClass('alert alert-error')
    	    		$(".msg").show();
    	  		}     		                
    	    }    		     
  		});    	    			   	
	    return false;
      });
});	    
</script>
@endsection