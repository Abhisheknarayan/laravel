@extends('adminlte::layouts.app')
@section('main-content')

    @section('htmlheader_title')
    Historical Data
    @endsection
    
    @section('contentheader_title')
    Add Historical Data
    @endsection


    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

	<div style="margin:10px 0;">
    	<span class="label label-success">Total: {{ $total_count }} reports</span>
    </div>

	<div class="user_filter mb-3 mt-3">
		
		<form class="search form-inline"  action="{{ url('/tmh/historical') }}" method="post">

			<input name="_token" value="{{ csrf_token() }}" type="hidden">
			<input name="_filter" value="_filter" type="hidden">
			
			<div class="form-group">
				<label>User Id</label>
		      	<input type="text" name="user_id" class="input-medium search-query" value="{{ $user_id }}">	
			</div>

	        <div class="form-group">
	            <label>Type</label>
	            {{ Form::select('tracker_type',array(''=>'Select type','1'=>'7 days report','0' => 'Full Archive'),$tracker_type) }}
	        </div>

	        <div class="form-group">
	            <label>Status</label>
	        	{{ Form::select('tracker_status',array(''=>'Select status','1'=>'In progress','2' => 'Completed','3' => 'Failed', '4' => 'Last Month Report'),$tracker_status) }}
	    	</div>

	      	<button type="submit" class="btn btn-warning" >Search</button>

	      	<a href="{{ url('/tmh/historical') }}"  class = "btn btn-primary">Reset</a>

	      	<button type="button" id="add-new-keyword" class="btn btn-success"><i class="fa fa-plus"></i> Add New Keyword</button>

	    </form>

    	
    </div>

    <div class="msg1">
    	@if(Session::has('s_message'))
    		<p class="alert alert-success">{{ Session::get('s_message') }}</p>
    	@endif
    </div>

	<div class="msg"></div>

	<div>
		
		<div class="table-responsive">
			<table class="table table-hover table-striped">
				<thead class="thead-dark">
        			<tr>
        				<th>Id</th>
        				<th>User_id</th>
        				<th>Keyword</th>
        				<th>Type</th>
        				<th>View Type</th>
        				<th>Tweets</th>
        				<th>First Tweet</th>
        				<th>Last Tweet</th>
        				<th>Created Date</th>
        				<th>Status</th>
        				<th></th>
        			</tr>
				</thead>	
				<tbody>
					@foreach($tmhhistorical as $tmh)
					<form name="" class="edit-historical-data" action="{{ url('/tmh/ajax/historical/update') }}" method="POST">

						<tr id="rowid_{{ $tmh->id }}" rowid="{{ $tmh->id }}">

							<td>{{ $tmh->id }} <input type="hidden" name="id" value="{{ $tmh->id }}"></td>
							<td>{{ $tmh->user_id }}</td>
							<td>{{ $tmh->keyword }}</td>
							<td>{{ ($tmh->tracker_type) ? '7 days report':'Full Archive' }}</td>
							<td>{{ $tmh->view_type }}</td>

							<td><input style="width:70px;" name="total_tweets" type="number" id="{{ $tmh->id.'_tweets' }}" value="{{ $tmh->total_tweets }}" required /></td>

							<td><input type="text" name="first_tweet_time" class="datetime" value='{{ $tmh->first_tweet_time }}' id="{{ $tmh->id.'_ftt' }}" /></td>

							<td><input type="text" name="last_tweet_time" class="datetime" value='{{ $tmh->last_tweet_time }}' id="{{ $tmh->id.'_ltt' }}" /></td>

							<td>{{ date('d M y H:i:s',strtotime($tmh->created_at)) }}</td>

							<td>
								
								{{ Form::select('tracker_status',array('1'=>'In progress','2' => 'Completed','3' => 'Failed', '4' => 'Last Month Report'),$tmh->tracker_status) }}

							</td>		

							<td><button type="submit" class="save_btn btn btn-primary btn-sm">Save</button><button style="margin-left: 10px;" class="delete_btn btn btn-danger btn-sm"> Delete</button></td>
						</tr>
					</form>
					@endforeach
				</tbody>	
			</table>
		</div>		

		<?php 

			$query_arr = array();

		    if($user_id != ''){
		        $query_arr['user_id'] = $user_id;
		    }

		    if($tracker_type!= ''){
				$query_arr['tracker_type']= $tracker_type;
			}

			if($tracker_status!= ''){
				$query_arr['tracker_status']= $tracker_status;
			}

		?>

		{{ $tmhhistorical->appends($query_arr)->links() }}

	</div>

	<div id="addModal" class="modal" tabindex="-1" role="dialog">
      
      <div class="modal-dialog" role="document">

        <div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title">Add New Keyword</h5>
			</div>

			{{ Form::open(['method' => 'post']) }}

			<div class="modal-body">
					
				<div class="form-group row">
		    		{{ Form::label('User Id', null,['class'=>'col-sm-3 col-form-label']) }}
		    		<div class="col-sm-9">
		    		{{ Form::text('user_id','', array('class'=>'form-control','id'=>'user_id')) }}
		    		</div>
				</div>
				<div class="form-group row">
		    		{{ Form::label('Keyword', null,['class'=>'col-sm-3 col-form-label']) }}
		    		<div class="col-sm-9">
		    		{{ Form::text('keyword','', array('placeholder'=>'Enter Keyword','class'=>'form-control','id'=>'Keyword')) }}
		    		</div>
				</div>
				<div class="form-group row">
		    		{{ Form::label('Total Tweets', null,['class'=>'col-sm-3 col-form-label']) }}
		    		<div class="col-sm-9">
		    		{{ Form::text('total_tweets','', array('placeholder'=>'','class'=>'form-control','id'=>'total-tweets')) }}
		    		</div>
				</div>
				<div class="form-group row">
		    		{{ Form::label('First Tweet Date', null,['class'=>'col-sm-3 col-form-label']) }}
		    		<div class="col-sm-9">
		    		{{ Form::text('first_tweet_date','', array('placeholder'=>'','class'=>'form-control datetime','id'=>'first-tweet-date ')) }}
		    		</div>
				</div>
				<div class="form-group row">
		    		{{ Form::label('Last Tweet Date', null,['class'=>'col-sm-3 col-form-label ']) }}
		    		<div class="col-sm-9">
		    		{{ Form::text('last_tweet_date','', array('placeholder'=>'','class'=>'form-control datetime','id'=>'last-tweet-date')) }}
		    		</div>
				</div>
					
			</div>

			<div class="modal-footer">
			    <button type="submit" class="btn btn-primary">Submit</button>
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

			{{ Form::close() }}

        </div>

      </div>

    </div>

	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/moment.js') }}"></script>
	<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

	<script type="text/javascript">

		$('#add-new-keyword').on('click',function(){
            $('#addModal').modal('show');
        });

	   	$(document).ready(function(){

	   		var $_format = 'YYYY-MM-DD hh:ss A';

		    $('.datetime').datetimepicker({
		        format:$_format
		    });

		   	$('.delete_btn').on('click',function(){
				var rowid = $(this).parent().parent().attr('rowid');
		    	var token		= 	$("meta[name='csrf-token']").attr("content");    			
		    	$(".msg").hide();
		  	    $.ajax({
	    	    	type:"POST", 
	    	    	headers: {'X-CSRF-TOKEN': token},
	    	    	url:"/tmh/ajax/historical/delete",
	    	    	data: {id:rowid},
	    	    	success:function(data,textstatus,xhr)
	    	    	{ 
	    	        	if(xhr.status == 200){
	    	   		    	$(".msg").html("Delete Successfully");
	    	     		    $(".msg").addClass('alert alert-success')
	    	     		    $(".msg").show();
	    	     		    $('#rowid_'+rowid).hide();
	    	     		}
	    	        	else if(xhr.status == 201){
	    	  		    	$(".msg").html("Error Occured While Saving");
	    	  		        $(".msg").addClass('alert alert-danger')
	    	  		        $(".msg").show();
	    	  		   }     		                
	    	        }    		     
	  		   	});    	    			   	
	  	    	return false;
			});	

		   	$(document).on('submit','.edit-historical-data',function(e){
		   		
		   		e.preventDefault();
        		var form = $(this);
        		var url = form.attr('action');

		   		$.ajax({

			   			type:"POST",
			   			url:url,
			   			data:form.serialize(),
			   			headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
			   			success:function(data,textstatus,xhr){

			                if(xhr.status == 200){
		    	   		    	$(".msg").html("Saved Successfully");
		    	     		    $(".msg").addClass('alert alert-success')
		    	     		    $(".msg").show();
		    	     		}
		    	        	else if(xhr.status == 201){
		    	  		    	$(".msg").html("Error Occured While Saving");
		    	  		        $(".msg").addClass('alert alert-danger')
		    	  		        $(".msg").show();
		    	  		   } 

			   			},
			   			error:function(){

			   				$(".msg").html("Something went wrong.");
			                $(".msg").addClass('alert alert-danger')
			                $(".msg").show(); 
			                $(".msg").html("Error Occured While Saving");
	    	  		        $(".msg").addClass('alert alert-danger')
	    	  		        $(".msg").show();
			   			}

		   		});
		   	});

	   	});

	</script>

 @endsection
