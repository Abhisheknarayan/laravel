@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Users
@endsection
@section('contentheader_title')
Trackers
@endsection

<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_trackers }} trackers</span>
	<span class="label label-warning">Active Trackers: {{ $active_trackers }} trackers</span>
	<span class="label label-warning">Finish Trackers: {{ $finish_trackers }} trackers</span>
</div>

<div>
	<form class="search" method="post" action="{{ url('/tmh/trackers') }}">

		<input name="_token" value="{{ csrf_token() }}" type="hidden">

        <strong>User Id:</strong>

        <input type="text" name="user_id" value="{{ $user_id }}" placeholder="User Id">

        <strong>Status:</strong>

        {{ Form::select('tracker_status', array(''=>'Select','0' => 'InActive', '1' => 'Active'),$tracker_status) }}

		<strong>State:</strong>

        {{ Form::select('is_active', array(''=>'Select','1' => 'Active', '0' => 'InActive'),$active_accounts) }}

      	<button type="submit" class="btn btn-warning" >Search</button>
      	<a href="{{ url('/tmh/trackers') }}"  class = "btn btn-primary">Reset</a>
    </form>
    
    <?php if( $active_accounts !=''): ?> 
    	<div style="margin:10px 0">Search Result : {{$search_count}} trackers </div>
	<?php endif;?>	   
    
	<div class="msg"></div>
	<div class="table-responsive" style="margin-top:30px">
        <table class="table table-hover table-striped text-center">
    		<thead class="thead-dark">
    			<tr>
    				<th>Id</th>
    				<th>User_id</th>
    				<th>Keyword</th>
    				<th>Tweets</th>
    				<th>First Tweet</th>
    				<th>Last Tweet</th>
    				<th>Created Date</th>
    				<th>Last Update</th>
    				<th>Status</th>
    				<th>State</th>
                    <th>Action</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($trackers as $tracker)
                    <form name="" class="edit-tracker-form" method="POST" action="{{ url('/tmh/ajax-edit-tracker') }}">
        				<tr>
            				<td>
                                <small>{{ $tracker->id }}</small>
                                <input type="hidden" value="{{ $tracker->id }}" name="id">
                            </td>
            				<td><small>{{ $tracker->user_id }}</small></td>
        					<td><small title="{{ $tracker->keyword }}">{{ substr($tracker->keyword,0,10) }}</small></td>
        					
                            <td><small title="{{ $tracker->total_tweets }}">{{ nice_number($tracker->total_tweets) }}</small></td>

        					<td><input type="text" name="first_tweet_time" id="first_tweet_time_{{ $tracker->id }}" class="datetime" value="{{ $tracker->first_tweet_time }}"> </td>

            				<td>
                                <input type="text" name="last_tweet_time" id="last_tweet_time_{{ $tracker->id }}" class="datetime" value="{{ $tracker->last_tweet_time }}">
                            </td>

            				<td><small>{{ date('d-M-y h:i:s A',strtotime($tracker->created_at)) }}</small></td>

            				<td><small>{{ date('d-M-y h:i:s A',strtotime($tracker->updated_at)) }}</small></td>

                           
            				<td>
                        
                                {{ Form::select('tracker_status',array('InActive','Active'),$tracker->tracker_status) }}

                            </td>

            				<td>
                        
                                {{ Form::select('is_active',array('InActive','Active'),$tracker->is_active) }}

                            </td>

                            <td><button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Save</button></td>

            			</tr>
                    </form>
    			@endforeach	
    		</tbody>
    	</table>
	</div>
</div>

<?php 

	$query_arr = array();


	if(isset($_GET['user_id'])){
	    $query_arr['user_id']          =   $_GET['user_id'];
	}

    if(!is_null($tracker_status)){
        $query_arr['tracker_status']   =   $tracker_status;
    }

    if(!is_null($active_accounts)){
        $query_arr['is_active']        =   $active_accounts;
    }

?>

{{ $trackers->appends($query_arr)->links() }}

@endsection

@section('scripts')

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

    $('#add-subscription-button').on('click',function(){
        $('#addModal').modal('show');
    });

    $(document).on('submit','.edit-tracker-form',function(e){

        e.preventDefault();
        
        var form = $(this);

        var url = form.attr('action');

        $.ajax({

            type:"POST",
            url:url,
            data:form.serialize(),
            headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
            success:function(data,textstatus,xhr){

                $('.text-danger').empty();

                if(xhr.status==200 && data.status==200){
                    
                    $(".msg").html(data.message);
                    $(".msg").addClass('alert alert-success')
                    $(".msg").show();

                    setTimeout(function(){location.reload()},1000);
                }
                else if(xhr.status==200 && data.status==400){
                    $(".msg").html(data.message);
                    $(".msg").addClass('alert alert-danger')
                    $(".msg").show();   
                }

            },
            error:function(){
                $(".msg").html("Something went wrong.");
                $(".msg").addClass('alert alert-danger')
                $(".msg").show(); 
            }

        });
    });

    var $_format = 'YYYY-MM-DD hh:ss A';

    $('.datetime').datetimepicker({
        format:$_format
    });

    $('#ends_at_datepicker').datetimepicker({
        format:$_format
    });
    $('#created_at_datepicker').datetimepicker({
        format:$_format
    });
    $('#updated_at_datepicker').datetimepicker({
        format:$_format
    });


</script>

@endsection