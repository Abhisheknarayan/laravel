@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Transactions
@endsection
@section('contentheader_title')
Transactions
@endsection
<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} Transactions</span>
</div>

{{ Form::open(['method' => 'post','class'=>'form-inline']) }}

	<div class="form-group">
		{{ Form::label('Email', null, ['class' => 'add-user']) }}
		{{ Form::text('email','', array('placeholder'=>'Enter Email Address')) }}

        <strong>User Id:</strong>

        <input type="text" name="user_id" value="{{ $user_id }}" placeholder="User Id">

	</div>

	<div class="btn-group" role="group">
		{{ Form::submit('Search',['class' => 'btn btn-warning']) }}
		<a href="{{ url('/') }}/tmh/transactions"  class = "btn btn-primary">Reset</a>
	</div>

{{ Form::close() }}

<?php if($email != ''): ?>
	<div style="margin:10px 0">Search Result : {{$user_trans_count}} users </div>
<?php endif ?>	   
<div class="table-responsive" style="margin-top:30px">
    <table class="table table-striped text-center">
    	<thead>
        	<tr>
                <th>User Id</th>
        		<th>Email</th>
        		<th>Tweets</th>
        		<th>Amount</th>
        		<th>Transaction Date</th>
        	</tr>
    	</thead>
    	<tbody>
            @foreach($user_trans as $usr)
            <tr>
                <td>{{ $usr->user_id }}</td>
            	<td>{{ $usr->email }}</td>
            	<td>{{ $usr->tweets_count }}</td>
            	<td>{{ '$'.$usr->amount }}</td>
            	<td>{{ date('d M Y H:i:s',strtotime($usr->created_at))}}</td>
            </tr>
            @endforeach
	    </tbody>
    </table>
</div>

<?php 

	$query_arr = array();
	
    if(!empty($email)){
		$query_arr['email'] 			= 	$email;
    }
	   
    if(!empty($user_id)){
        $query_arr['user_id']           =   $user_id;
    }
		
?>

{{ $user_trans->appends($query_arr)->links() }}

@endsection