@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Subscriptions
@endsection
@section('contentheader_title')
Subscriptions
@endsection

<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} Transactions</span>
</div>

{{ Form::open(['method' => 'post','class'=>'form-inline']) }}

    <div class="form-group">
        {{ Form::label('User Id ', null, ['class' => 'add-user']) }}
        {{ Form::text('user_id','', array('placeholder'=>'Enter User Id')) }}
    </div>

	<div class="form-group">
		{{ Form::label('Email ', null, ['class' => 'add-user']) }}
		{{ Form::text('email','', array('placeholder'=>'Enter Email Address')) }}
	</div>

	<div class="btn-group" role="group">
		{{ Form::submit('Search',['class' => 'btn btn-warning']) }}
		<a href="{{ url('/') }}/tmh/subscriptions"  class = "btn btn-primary">Reset</a>
        <button type="button" id="add-subscription-button" class="btn btn-success"><i class="fa fa-plus"></i> Add Subscription</button>
	</div>

{{ Form::close() }}

<p>&nbsp;</p>    

<br/>

<?php
	  if($email != '' ) 
		echo  "<p>Search Result : ".$user_subs_count." users </p>";
?>	

<div class="msg"></div> 

<div class="table-responsive">
    <table class="table table-striped text-center">
    	<thead>
        	<tr>
        		<th>User Id</th>
                <th>Email</th>
                <th>Stripe Id</th>
        		<th>Stripe Status</th>
        		<th>Stripe Plan</th>
                <th>Ends At</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
        	</tr>
    	</thead>
    	<tbody>
    		@foreach($user_subs as $usr)
            <form name="" class="edit-subscription-form" method="POST" action="{{ url('/tmh/ajax-edit-subscription') }}">
        		<tr>

        			<td>
                        {{ $usr->user_id }}
                        <input type="hidden" name="id" value="{{ $usr->id }}">
                    </td>

                    <td>{{ $usr->email }}</td>

                    <td><input type="text" name="stripe_id" class="stripe_id" value="{{ $usr->stripe_id }}" ></td>

        			<td><input type="text" name="stripe_status" class="stripe_status" value="{{ $usr->stripe_status }}" ></td>

                    <td><input type="text" name="stripe_plan" class="stripe_plan" value="{{ $usr->stripe_plan }}" ></td>

                    <td><input type="text" id="ends_at_{{ $usr->id }}" name="ends_at" class="ends_at datetime" value="{{ $usr->ends_at }}" ></td>

                    <td><input type="text" id="created_at_{{ $usr->id }}" name="created_at" class="created_at datetime" value="{{ $usr->created_at }}" ></td>

                    <td><input type="text" id="updated_at_{{ $usr->id }}" name="updated_at" class="updated_at datetime" value="{{ $usr->updated_at }}" ></td>

                    <td>
                        <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Save</button>
                    </td>

        		</tr>
            </form>
    		@endforeach
    	</tbody>
    </table>
</div>

<?php 

	$query_arr = array();

    if($user_id != ''){
        $query_arr['user_id'] =   $user_id; 
    }

	if($email != ''){
		$query_arr['email'] = 	$email;		
	}

?>

{{ $user_subs->appends($query_arr)->links() }}

<div id="addModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h5 class="modal-title">Add Module</h5>

      </div>

      <form name="add-subscription-form" id="add-subscription-form" action="{{ url('/tmh/ajax-add-subscription') }}" method="POST">

        <div class="modal-body">

            <div class="add-flash-message"></div>

            {{ csrf_field() }}

            <div class="form-group">
                <label>User Id</label>
                <input type="text" name="tmh_user" value="" class="form-control" placeholder="User Id">
                <p class="text-danger error_user"></p>
            </div>

            <div class="form-group">
                <label>Stripe Id</label>
                <input type="text" name="stripe_id" value="" class="form-control" placeholder="Stripe Id">
                <p class="text-danger error_stripe_id"></p>
            </div>

            <div class="form-group">
                <label>Stripe Status</label>
                <input type="text" name="stripe_status" value="" class="form-control" placeholder="Stripe status">
                <p class="text-danger error_stripe_status"></p>
            </div>

            <div class="form-group">
                <label>Stripe Plan</label>
                <input type="text" name="stripe_plan" value="" class="form-control" placeholder="Stripe plan">
                <p class="text-danger error_stripe_plan"></p>
            </div>

            <div class="form-group">
                <label>Ends At</label>

                <div class="form-group">
                    <div class='input-group date' id='ends_at_datepicker'>
                        <input type="text" name="ends_at" value="" class="form-control ends_at" placeholder="yyyy-mm-dd hh:ss A" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>    
                </div>

                <p class="text-danger error_ends_at"></p>
            </div>

            <div class="form-group">
                <label>Created At</label>
                
                <div class="form-group">
                    <div class='input-group date' id='created_at_datepicker'>
                        <input type="text" name="created_at" value="" class="form-control created_at" placeholder="yyyy-mm-dd hh:ss A">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>    
                </div>

                <p class="text-danger error_created_at"></p>
            </div>

            <div class="form-group">
                <label>Updated At</label>
                
                <div class="form-group">
                    <div class='input-group date' id='updated_at_datepicker'>
                        <input type="text" name="updated_at" value="" class="form-control updated_at" placeholder="yyyy-mm-dd hh:ss A">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>    
                </div>

                <p class="text-danger error_updated_at"></p>
            </div>

        </div>

        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>

      </form>

    </div>
  </div>
</div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

    </div>
  </div>
</div>

@endsection

@section('scripts')

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

    $('#add-subscription-button').on('click',function(){
        $('#addModal').modal('show');
    });

    $(document).on('submit','#add-subscription-form',function(e){

        e.preventDefault();
        
        var form = $(this);

        var url = form.attr('action');

        $.ajax({

            type:"POST",
            url:url,
            data:form.serialize(),
            headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
            success:function(data,textstatus,xhr){

                $('.text-danger').empty();

                if(xhr.status==200 && data.status==200){
                    
                    $('.add-flash-message').html('<div class="alert alert-success"><strong>Success!  </strong>'+data.message+'.</div>');

                    setTimeout(function(){location.reload()},1000);
                }
                else if(xhr.status==200 && data.status==400){
                    $('.error_stripe_id').text(data.data.stripe_id);
                    $('.error_stripe_status').text(data.data.stripe_status);
                    $('.error_stripe_status').text(data.data.stripe_status);
                    $('.error_stripe_plan').text(data.data.stripe_plan);
                    $('.error_ends_at').text(data.data.ends_at);
                    $('.error_created_at').text(data.data.created_at);
                    $('.error_updated_at').text(data.data.updated_at);
                }

            },
            error:function(){
                $('.add-flash-message').html('<div class="alert alert-danger"><strong>Error! </strong> Something went wrong.</div>');
            }

        });
    });

    $(document).on('submit','.edit-subscription-form',function(e){

        e.preventDefault();
        
        var form = $(this);

        var url = form.attr('action');

        $.ajax({

            type:"POST",
            url:url,
            data:form.serialize(),
            headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
            success:function(data,textstatus,xhr){

                $('.text-danger').empty();

                if(xhr.status==200 && data.status==200){
                    
                    $(".msg").html(data.message);
                    $(".msg").addClass('alert alert-success')
                    $(".msg").show();

                    setTimeout(function(){location.reload()},1000);
                }
                else if(xhr.status==200 && data.status==400){
                    $(".msg").html(data.message);
                    $(".msg").addClass('alert alert-danger')
                    $(".msg").show();   
                }

            },
            error:function(){
                $(".msg").html("Something went wrong.");
                $(".msg").addClass('alert alert-danger')
                $(".msg").show(); 
            }

        });
    });

    var $_format = 'YYYY-MM-DD hh:ss A';

    $('.datetime').datetimepicker({
        format:$_format
    });

    $('#ends_at_datepicker').datetimepicker({
        format:$_format
    });
    $('#created_at_datepicker').datetimepicker({
        format:$_format
    });
    $('#updated_at_datepicker').datetimepicker({
        format:$_format
    });


</script>

@endsection