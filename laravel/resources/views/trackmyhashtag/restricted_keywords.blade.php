@extends('adminlte::layouts.app')
@section('main-content')
    @section('htmlheader_title')
	    Restricted Keywords
    @endsection
    @section('contentheader_title')
    	Restricted Keywords
    @endsection
	
	<div>
		@if(Session::has('k_error'))
    		<div class="alert alert-danger" role="alert">
              {{ Session::get('k_error') }}
            </div>
		@elseif(Session::has('k_success'))
    		<div class="alert alert-success" role="alert">
              {{ Session::get('k_success') }}
            </div>
		@endif		
		<div class="msg"></div>
	
		<form class="form-inline " method="post" >
			<input name="_token" value="{{ csrf_token() }}" type="hidden">
			<input type="text" name="keyword" value="" placeholder="Add Keyword" class="form-control" required="required">
			<input type="submit" name="keyword-submit" value="Add" class="btn btn-success" />
		</form>
	</div>
	
	<div style="margin-top:30px;">
		<table class="table table-hover">
			<thead>
				<tr>
        			<th>Keyword</th>
        			<th>Added</th>
        			<th>Is Active</th>
        			<th></th>
				</tr>
			</thead>
			<tbody>
    			@foreach($tmhrestrictedkeyword as $tmh)
    				<tr id="{{ $tmh->id.'_key' }}">
        				<td>{{ $tmh->keyword }}</td>
        				<td>{{ date('d M Y',strtotime($tmh->created_at)) }}</td>
        				<td><input id="{{ $tmh->id.'_key_check' }}" type="checkbox" onClick="update_keyword({{ $tmh->id}} ,{{ ($tmh->is_active) ? 0:1 }})"  {{ ($tmh->is_active) ? 'checked' : '' }} /></td>
        				<td><button class="btn btn-danger" onclick="return delete_keyword({{ $tmh->id }});">Delete</button></td>
    				</tr>
    			@endforeach
			</tbody>
		</table>
	</div>
	{{ $tmhrestrictedkeyword->links() }}
	
	<script type="text/javascript">
		
		$(".msg").hide();
		function delete_keyword(id){
	    	var token		= 	$("meta[name='csrf-token']").attr("content");    			
	    	$(".msg").hide();	
	  	    $.ajax({
		       	type:"POST", 
	        	headers: {'X-CSRF-TOKEN': token},
	        	url:"/tmh/ajax/restricted-keywords/delete",
	        	data: {id:id},
	        	success:function(data,textstatus,xhr)
	    	   	{ 
	    	       	if(xhr.status == 200){
	    		    	$(".msg").html("Delete Successfully");
	    	  		    $(".msg").addClass('alert alert-success')
	    	  		    $(".msg").show();
						$('#'+id+"_key").hide();
	    	       	}
	    	    }    		     
	  		});    	    			   	
		    return false;
		}

		function update_keyword(id,action){
	    	var token		= 	$("meta[name='csrf-token']").attr("content");    			
	    	$(".msg").hide();	
	  	    $.ajax({
		       	type:"POST", 
	        	headers: {'X-CSRF-TOKEN': token},
	        	url:"/tmh/ajax/restricted-keywords/update",
	        	data: {id:id,action:action},
	            context: this,
	        	success:function(data,textstatus,xhr)
	    	   	{ 
	    	       	if(xhr.status == 200){
	    		   		$(".msg").html("Update Successfully");
	    	  		    $(".msg").addClass('alert alert-success')
	    	  		    $(".msg").show();
	    	       	}
	    	    }    		     
	  		});    	    
	  		
	  	  	if(action){
	  			$('#'+id+"_key_check").attr("onClick","update_keyword("+id+",0)");
		    }else{
	  		  	$('#'+id+"_key_check").attr("onClick","update_keyword("+id+",1)");
		    }			   	
		}
		
		
	</script>
	
@endsection