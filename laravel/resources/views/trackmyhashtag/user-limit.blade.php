@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Trackmyhashtag Users LImit
@endsection
@section('contentheader_title')
Trackmyhashtag Users Limit
@endsection
<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">

    <div style="margin:10px 0;">
    	<span class="label label-success">Total: {{ $total_count }} users</span>
    </div>
	
	<form class="search form-inline"  action="{{ url('/tmh/users-limit') }}" method="post">

		<input name="_token" value="{{ csrf_token() }}" type="hidden">
		
		<div class="form-group">
			<label>Email Id</label>
	      	<input type="text" name="email-id" class="input-medium search-query" value="{{ $email_id }}">	
		</div>

        <div class="form-group">
            <label>User Id</label>
            <input type="text" name="user-id" class="input-medium search-query" value="{{ $user_id }}">   
        </div>
		
      	<button type="submit" class="btn btn-warning" >Search</button>

      	<a href="{{ url('/tmh/users-limit') }}"  class = "btn btn-primary">Reset</a>

    </form>
    
    <?php if($email_id !='' || $user_id != ''):  ?>
   		<div style="margin:10px 0">Search Result : {{ $search_count }} user(s) </div>
	<?php endif; ?>	

	<div class="msg"></div>

    <div class="table-responsive tmh-user-limit" style="margin-top:30px">

    	<table class="table table-striped">

    		<thead>

        		<tr>
                    <th>User Id</th>
                    <th>Email</th>
            		<th>Number of Trackers</th>
            		<th>Number of tweets</th>
                    <th>Extra tweets</th>
                    <th>Consumed tweets</th>
            		<th>Historical report</th>
            		<th>Current Plan</th>
                    <th>Action</th>
            	</tr>

    		</thead>

    		<tbody>
                
    			@foreach($userLimitObj as $usr)		
                    
    				<tr>

                        <form class="user-limit-form" method="POST" action="{{ url('tmh/ajax-save-user-limit') }}">
                        <td>
                            {{ $usr->id }}
                        </td>
                        <td>
                            {{ $usr->email }}
                        </td>
        				<td>
                            <input type="hidden" name="user_id" value="{{ $usr->id }}" class="">
                            <input type="text" name="tracker" value="{{ $usr->trackers }}" class="" >
                        </td>
        				<td>
                            <input type="text" name="tweets" value="{{ $usr->tweets }}" class="">
                        </td>
        				<td>
                            <input type="text" name="extra_tweets" value="{{ $usr->extra_tweets }}" class="">            
                        </td>
                        <td>
                            <input type="text" name="consumed_tweets" value="{{ $usr->consumed_tweets }}">
                        </td>
                        <td>
                            <input type="text" name="historical_report" value="{{ $usr->historical_report }}">
                        </td>
        				<td>
                            {{  Form::select('current_product_id', config('trackmyhashtag.pricing'), $usr->current_product_id ) }}
                        </td>

                        <td>
                            <button type="submit" class="btn btn-success" class="save-button" > <i class="fa fa-save"></i> Save</button>
                        </td>

                        </form>

        			</tr>

    			@endforeach		

    		</tbody>

    	</table>

    </div>
    
<?php 

	$query_arr = array();
	
	if($email_id != ''){
		$query_arr['email-id']= $email_id;
	}

    if($user_id != ''){
        $query_arr['user-id'] = $user_id;
    }

?>

{{ $userLimitObj->appends($query_arr)->links() }}

@endsection

@section('scripts')
<script type="text/javascript">

    jQuery(document).ready(function($){

        $(document).on('submit','.user-limit-form',function(e){

            e.preventDefault();

            var token       =   $("meta[name='csrf-token']").attr("content");

            $_data          =   $(this).serialize();

            $_url           =   $(this).attr('action');

            $(".msg").hide();   

            $.ajax({

                    type:"POST", 
                    headers: {'X-CSRF-TOKEN': token},
                    url: $_url,
                    data: $_data,
                    success:function(data,textstatus,xhr)
                    {

                        if(xhr.status == 200){
                            $(".msg").html("Saved Successfully");
                            $(".msg").addClass('alert alert-success')
                            $(".msg").show();
                            }
                        else if(xhr.status == 201){
                            $(".msg").html("Error Occured While Saving");
                            $(".msg").addClass('alert alert-error')
                            $(".msg").show();
                        } 

                    }    

               });                          

             return false;

        });

        $('.created_at').datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $('.updated_at').datepicker({
            dateFormat: 'yy-mm-dd'
        });

    });	  

</script>

@endsection