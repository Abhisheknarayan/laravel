@extends('adminlte::layouts.app')
@section('main-content')

    @section('htmlheader_title')
    Historical Data
    @endsection
    
    @section('contentheader_title')
    Historical Data
    @endsection
    
	<style>

        form{
            display:contents;
        }
        .report-header{
            margin-bottom:20px;
        }

    </style>
    <div class="container-fluid">
        @if(Session::has('message'))
            {!! Session::get('message') !!}
        @endif

        <div class="report-header shadow-sm">

            <div class="row">

                <div class="col-12">

                    <div class="row report-desc">

                        <form name="found" action="{{ url('historical/search-tweet-historical') }}" method="POST" >

                            {{ csrf_field() }}

                            <div class="col-3 col-md-3">
                                <input type="text" name="keyword" required class="form-control" value="" placeholder="keyword" />
                            </div>
                            <div class="col-3 col-md-3">
                                <input type="number" min="100" required name="limit" class="form-control" value="" placeholder="limit" />
                            </div>
                            <div class="col-2 col-md-2">
                                  
                                <button type="submit" class="btn btn-success" name="submit" value="submit">Submit</button>
                               
                            </div>

                        </form>

                    </div>

                    <div class="row" style="display:none;">
                        <div class="col-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <span id="download-file-msg"></span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>	
                        </div>
                    </div>

                    <!-- table -->

                    <div class="table-responsive" style="margin-top:30px">

                        <table class="table table-striped">

                            <thead>

                                <tr>
                                    
                                    <th>Keyword</th>
                                    <th>Rows</th>
                                    <th>Status</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Created at</th>
                                    <th class="action">Action</th>
                                </tr>

                            </thead>

                            <tbody> 
                                @foreach($historicalData as $key => $row)


                                    <tr>
                                       
                                        <td>{{ $row->keyword }}</td>
                                        <td>{{ $row->tweet_count }}</td>
                                        <td>

                                            {!! ($row->status=='0') ? '<p class="text-warning">Downloading</p>' : ''  !!}

                                            {!! ($row->status=='1') ? '<p class="text-success">Completed</p>' : ''  !!}

                                            {!! ($row->status=='2') ? '<p class="text-danger">Failed</p>' : ''  !!}

                                        </td>
                                        <td>{{ (!empty($row->started_date)) ? date('Y M d H:i:s A',strtotime($row->started_date)) : '--' }}</td>
                                        <td>{{ (!empty($row->end_date)) ? date('Y M d H:i:s A',strtotime($row->end_date)) : '--' }}</td>
                                        <td>{{ date('Y M d H:i:s A',strtotime($row->created_at)) }}</td>
                                        <td>

                                            <form action="{{ url('historical/downloadFile/'.strtotime($row->created_at).'_'.$row->id.'.csv') }}" method="POST">
                                                {{ csrf_field() }}

                                                @if($row->status=='1')

                                                <button type="submit" class="btn btn-success">Download</button>

                                                @else

                                                <button type="reset" class="btn btn-default">Download</button>

                                                @endif

                                            </form>

                                            <form action="{{ url('historical/deleteFile/'.strtotime($row->created_at).'_'.$row->id.'.csv'.'/'.$row->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>

                    </div>

                    <!-- end -->

                </div>

            </div>

        </div>

    </div>
    
    
@endsection