@extends('adminlte::layouts.app')

@section('contentheader_title')
	
@endsection

@section('main-content')
 	<div class="container-fluid spark-screen">
		<div class="row">


			@if(in_array(21,$permission))

			<div class="col-md-12">
				@if($inactive_accounts_tracker > 0) 
					<a href="javascript:" id="reset_accounts_btn" class="btn btn-danger btn-sm">Reset Accounts ({{ $inactive_accounts_tracker }}) -  ({{ $jobs_in_queue }})</a>
				@else
					<a href="javascript:" class="btn btn-danger disabled btn-sm">Reset Accounts</a>
				@endif
				<a href="javascript:" id="flush_failed_jobs_btn" class="btn btn-danger btn-sm">Reset Failed Jobs (<span class="loadfj"></span>)</a>
			</div>

			<div class="col-md-12 center-block" style="margin-top:2em">
				<p class="bg-info" style="padding:10px">Primary</p>
				<div class="text-center pull-left" style="margin:15px">
    				<p class="label label-primary center">{{ $access_token }}</p>
    				<p>Active Access Tokens</p>
				</div>
				<div class="text-center pull-left" style="margin:15px;">
    				<p class="label label-primary load center" id="loadjinq"></p>
    				<p>Jobs in Queue</p>
				</div>
					
				<div class="text-center pull-left" style="margin:15px;">
    				<p class="label label-primary load center loadfj"></p>
    				<p>Failed Jobs</p>
				</div>
				<div class="text-center pull-left" style="margin:15px">
    				<p class="label label-primary load center" id="loadutnd"></p>
    				<p>Users Tweets In DB</p>
				</div>
				
				<div class="text-center pull-left" style="margin:15px">
    				<p class="label label-primary load center" id="loadund"></p>
    				<p>Users In DB</p>
				</div>
				<div class="text-center pull-left" style="margin:15px">
    				<p class="label label-primary center">{{ $user_tweets_not_index }}</p>
    				<p>Tweets Not Indexed</p>
				</div>
				<div class="clearfix"></div>
			</div>

			@endif

			@if(in_array(26,$permission))
				<div class="col-md-12" style="margin-top:2em">
					<p class="bg-info" style="padding:10px">FollowersAnalysis</p>
					<div class="text-center pull-left" style="margin:15px">
	    				<p class="label label-primary load center" id="fausers"></p>
	    				<p>Followers Analysis Users</p>
					</div>
					
					<div class="text-center pull-left" style="margin:15px">
	    				<p class="label label-primary load center" id="faorders"></p>
	    				<p>Followers Analysis Orders</p>
					</div>
					<div class="clearfix"></div>
				</div>
			@endif

			@if(in_array(55,$permission))

				<div class="col-md-12" style="margin-top:2em">
					<p class="bg-info" style="padding:10px">TrackMyHashtag</p>
					<div class="text-center pull-left" style="margin:15px">
	    				<p class="label label-primary load center" id="tmhusers"></p>
	    				<p>TrackMyHashtag User</p>
					</div>
					
					<div class="text-center pull-left" style="margin:15px">
	    				<p class="label label-primary load center" id="tmhsubscriptions"></p>
	    				<p>TrackMyHashtag Subscriptions</p>
					</div>
						
					<div class="text-center pull-left" style="margin:15px;">
	    				<p class="label label-primary load center" id="tmhtransactions"></p>
	    				<p>TrackMyHashtag Transactions</p>
					</div>
					<div class="clearfix"></div>
				</div>

			@endif

			@if(in_array(72,$permission))

			<div class="col-md-12" style="margin-top:2em">
				<p class="bg-info" style="padding:10px">MostPopularTweet</p>
				<div class="text-center pull-left" style="margin:15px">
    				<p class="label label-primary load center" id="mptusers"></p>
    				<p>MostPopularTweet User</p>
				</div>
				
				<div class="text-center pull-left" style="margin:15px;">
    				<p class="label label-primary load center" id="mptreports"></p>
    				<p>MostPopularTweets Reports</p>
				</div>
				
				<div class="text-center pull-left" style="margin:15px">
    				<p class="label label-primary load center" id="mptsubscriptions"></p>
    				<p>MostPopularTweet Subscriptions</p>
				</div>
				<div class="clearfix"></div>
			</div>	

			@endif

		</div>
	</div>

<script type="text/javascript">

$(document).ready(function() {
	
	$(".load").html('<img src="{{ asset("img/loading.gif") }}" />');

	@if(in_array(21,$permission))

	$.ajax({
		type: "POST",
		headers: {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
		url: "/core/ajax/countfields",
		data:{},
		success: function(arr){
			$(".loadfj").html(arr.countfj);
			$("#loadjinq").html(arr.countjinq);
		},
		error: function(){
			$(".loadfj").html(0);
    		$("#loadjinq").html(0);
    	}	 
	});

		
	$.ajax({
    	type: "POST",
    	headers: {'X-CSRF-TOKEN':  $("meta[name='csrf-token']").attr("content")},
    	url: "/core/ajax/countsavedtwitterusers",
    	data:{},
    	success: function(arr){
    		$("#loadutnd").html(arr.total_users);
    	},
    	error: function(){
    		$("#loadutnd").html(0);
    	}
    });


	$.ajax({
    	type: "POST",
    	headers: {'X-CSRF-TOKEN':  $("meta[name='csrf-token']").attr("content")},
    	url: "/core/ajax/countsavedusers",
    	data:{},
    	success: function(arr){
    		$("#loadund").html(arr.total_users);
    	},
    	error: function(){
    		$("#loadutnd").html(0);
    	}
    });

    $('#reset_accounts_btn').on('click',function(){
		$.ajax({
			type: "GET",
			headers: {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
			url: "/core/ajax/reset/accounts",
			data:{},
			success: function(arr){
				window.location.reload();
			}	 
		 });
	});

	$('#flush_failed_jobs_btn').on('click',function(){
		$.ajax({
			type: "GET",
			headers: {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
			url: "/core/ajax/reset/failedjobs",
			data:{},
			success: function(arr){
				window.location.reload();
			}	 
		 });
	});

	@endif

	@if(in_array(26, $permission))
	
		$.ajax({
			type: "POST",
			url: "/core/ajax/fa",
			data:{},
			headers: {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
			success: function(arr){
				$("#fausers").html(arr.users);
				$("#faorders").html(arr.orders);
			},	
			error:function(){
				$("#fausers").html(0);
				$("#faorders").html(0);
			}
		});

	@endif

	@if(in_array(55, $permission))
	
		$.ajax({
			type: "POST",
			headers: {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
			url: "/core/ajax/tmh",
			data:{},
			success: function(arr){
				$("#tmhusers").html(arr.users);
				$("#tmhtransactions").html(arr.transactions);
				$("#tmhsubscriptions").html(arr.subscriptions);
			},
			error:function(){
				$("#tmhusers").html(0);
				$("#tmhtransactions").html(0);
				$("#tmhsubscriptions").html(0);
			}	 
		});

	@endif


	@if(in_array(72,$permission))

	$.ajax({
		type: "POST",
		headers: {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
		url: "/core/ajax/mpt",
		data:{},
		success: function(arr){
			$("#mptusers").html(arr.users);
			$("#mptreports").html(arr.reports);
			$("#mptsubscriptions").html(arr.subscriptions);
		},
		error:function(){
			$("#mptusers").html(0);
			$("#mptreports").html(0);
			$("#mptsubscriptions").html(0);
		}	 
	});

	@endif
	
	

});	
</script>
@endsection 