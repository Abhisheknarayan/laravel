@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Accounts
@endsection
@section('contentheader_title')
Accounts
@endsection
<div class="label label-success">Total Accounts: {{ $total_accounts }}</div>
<div class="label label-success">Active Accounts Tracking: {{ $total_count }}</div>
<div class="label label-danger">Not Updated Accounts: {{ $inactive_accounts_tracker }}</div>
<p>&nbsp;</p>
@if (count($errors) > 0)
	<div class = "alert alert-danger">
	    <ul>
	    	@foreach ($errors->all() as $error)
	        	<li>{{ $error }}</li>
	        @endforeach
	    </ul>    
  	</div>
@endif
@if(Session::has('add_account_failed'))
<div class = "alert alert-danger">
   <ul>
        <li>
			<div class="add-user-error"><p>{!! session('add_account_failed') !!}</p></div>
	     </li>
   </ul>    
</div>
@endif
@if(Session::has('add_user_failed'))
<div class = "alert alert-danger">
   <ul>
        <li>
			<div class="add-user-error"><p>{!! session('add_user_failed') !!}</p></div>
	     </li>
   </ul>    
</div>
@endif
@if(Session::has('add_user_success'))
<div class = "alert alert-success">
   <ul>
        <li>
			<div class="add-user-success"><p>{!! session('add_user_success') !!}</p></div>
	     </li>
   </ul>    
</div>
@endif
	<div class= "search">
		{{ Form::open(['method' => 'post','class'=>'searchform']) }}
			{{ Form::text('q',$search, array('placeholder'=>'Search Twitter Id/Username')) }}
			{{ Form::submit('search',['class'=>'btn btn-warning']) }}
			<a href="{{ url('/') }}/core/accounts"  class = "btn btn-primary">Reset</a>
		{{ Form::close() }}
	</div>

	<div class="addnewtagform">
	{{ Form::open(['method' => 'post','action' => 'Core\MobileController@add_account']) }}
	   	<div id="keywordtext" class="form-heading">
	   		<b>Add New Account:</b>
	   	</div>
		{{ Form::text('account','', array('placeholder'=>'Enter Username')) }}

	    {{ Form::submit('Add',array('id'=>'save','class' => 'btn btn-warning')) }}   
	{{ Form::close() }}
	</div>

	<div class="msg"></div>
  	<div class="table-responsive" style="margin-top:50px">
    	<table class="table text-center table-hover table-striped">
    		<tr>
    			<th>Twitter Id</th>
    			<th>Username</th>
    			<th>Tweets Count</th>
    			<th>Last Update</th>
    			<th>Status</th>
    			<th>Actions</th>
    		</tr>
    		@foreach($accounts as $account)
    			<tr id="listaccount{{ $account->id }}" keyword-id="{{ $account->id }}" twitter-id="{{ $account->twitter_id }}" class="data-row">
    				<td>{{ $account->twitter_id }}</td>
    				<td>{{ $account->username }}</td>
    				<td id="tc_{{ $account->twitter_id }}" class="load tweets_count"></td>
    				<td>{{ date('d-M-Y H:i:s',strtotime($account->updated_at)) }}</td>
    
    				<td>{{ Form::select('status', ['1' => 'Active', '0' => 'Inactive'],$account->is_active, array('class'=>'hashstatus')) }}</td>
    				<td>{{ Form::submit('Save',array('class' => 'btn btn-info savebtn')) }}</td>
    			</tr>
    		@endforeach
    	</table>
	</div>
<?php 
		$query_arr = array();
		
		if($search != ''){
			$query_arr['q'] = $search;
		}
?>
{{ $accounts->appends($query_arr)->links() }}
@endsection

@section('scripts')


<!-- 
<link rel="stylesheet" href="{{ asset('css/sumoselect.min.css') }}" />
<script src="{{ asset('js/jquery.sumoselect.min.js') }}"></script>
-->
<link href="{{ asset('css/fselect.css')  }}" rel="stylesheet" />
<script src="{{ asset('js/fselect.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

	var loading_img	='<img src="{{ asset("img/loading.gif") }}" />';
	$(".load").html(loading_img);
	var cids	=	[];
	$('.data-row').each(function(){
		cids.push($(this).attr('twitter-id'));	
	});

	if(cids.length > 0){
		var token		= 	$("meta[name='csrf-token']").attr("content");    			
		$.ajax({
		   	type:"POST", 
		   	headers: {'X-CSRF-TOKEN': token},
		  	url:"/core/ajaxlistusers-count",
		    data: {cids:cids},
		   	success:function(data,textstatus,xhr){ 
		     	if(xhr.status == 200){
					$(data).each(function(k,v){
						$('#tc_'+v.id).html(v.tweets);
						first_tweet	=	v.first_tweet;
						if(first_tweet != null){
							$('#ft_'+v.id).html(v.first_tweet.split(" ")[0]);
						}else{
							$('#ft_'+v.id).html('');
						}
					});
				}
		    }    		     
		});    	    			   	
	}

	$('.multiselect').fSelect();
    
	$(".savebtn").on('click',function(){
		let parent_r	=	$(this).parent().parent();
		let id		 	=	parent_r.attr('keyword-id');
		let status		=	parent_r.find('select[name="status"]').val();
		$(".msg").hide();
		$(".msg").removeClass('alert');
		$(".msg").removeClass('alert-success');
		$(".msg").removeClass('alert-danger');
		
		$.ajax({
   			type:"POST", 
   			headers: {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
   			url:"/core/ajax/saveaccount",
   			data: {id:id,status:status},
 	        success:function(data,textstatus,xhr){ 
       			if(xhr.status == 200){
  		       		$(".msg").html("Saved Successfully");
  		       	    $(".msg").addClass('alert alert-success')
    		        $(".msg").show();
    			}
       		    else if(xhr.status == 201){
 		            $(".msg").html("Error Occured While Saving");
 		            $(".msg").addClass('alert alert-danger')
 		            $(".msg").show();
 		        }     		                
 	        }    		     
   		});
       	return false;   	
   });
	
	$(".deletebtn").on("click",function(){
    	var id 			=	$(this).parent().prev().prev().prev().prev().prev().prev().children().val();  
    	var token		= 	$("meta[name='csrf-token']").attr("content");
    	$.ajax({
			type:"POST",
			headers: {'X-CSRF-TOKEN': token},
		    url:"/core/ajax/mptkeyworddelete",
	        data: {id:id},
	        success:function(data,textstatus,xhr)
	         { 
		         if(xhr.status == 200){
		        	$("#listkeywords"+id).hide();
	                    $(".msg").html("Deleted Successfully");
		                $(".msg").addClass('alert alert-success')
		                $(".msg").show();
		                }
		         else if(xhr.status == 201){
	                    $(".msg").html("Error Occured While Deleting");
	                    $(".msg").addClass('alert alert-danger')
	                    $(".msg").show();
	                }     		                
	         }    		     
	    });
	    return false;
	});
});
</script>
@endsection