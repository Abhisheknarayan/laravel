@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Mongodb Databases
@endsection
@section('contentheader_title')
Mongodb Databases
@endsection

<p><strong>Server {{ config('tweetkitapp.mongo_ip') }}</strong></p>
<table class="table-condensed text-center table table-hover table-striped category-table">
	<thead>
    	<tr>
    		<th>Database</th>
    		<th>Size</th>
    	</tr>
	</thead>
	<tbody>
		@foreach($databases as $database)
         	<tr>
        		<td>{{ $database->getName() }}</td>
        		<td>{{ formatBytes($database->getSizeOnDisk()) }}</td>
        	</tr>
		@endforeach
	</tbody>
</table>

<p><strong>Server {{ config('tweetkitapp.mongo_ut_ip') }}</strong></p>
<table class="table-condensed text-center table table-hover table-striped category-table">
	<thead>
    	<tr>
    		<th>Database</th>
    		<th>Size</th>
    	</tr>
	</thead>
	<tbody>
		@foreach($databases_ as $database)
         	<tr>
        		<td>{{ $database->getName() }}</td>
        		<td>{{ formatBytes($database->getSizeOnDisk()) }}</td>
        	</tr>
		@endforeach
	</tbody>
</table>



@endsection