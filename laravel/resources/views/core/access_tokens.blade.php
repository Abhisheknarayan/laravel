@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Access Tokens
@endsection
@section('contentheader_title')
Access Tokens
@endsection
<span class="label label-success">Total: {{ $total_access_token }} tokens</span>  
<span class="label label-danger">Not Working: {{ $total_not_working_access_token }} tokens</span>
<span class="label label-danger">Disabled: {{ $total_disabled_access_token }} tokens</span>
@if (count($errors) > 0)
	<div class = "alert alert-danger">
@endsection 
    	<ul>
        	@foreach ($errors->all() as $error)
           		<li>{{ $error }}</li>
            @endforeach
        </ul>    
	</div>
@endif
@if(Session::has('add_user_failed'))
<div class = "alert alert-danger">
   	<div class="add-user-error"><p>{!! session('add_user_failed') !!}</p></div>
</div>
@endif
@if(Session::has('add_user_success'))
<div class = "alert alert-success">
	<div class="add-user-success"><p>{!! session('add_user_success') !!}</p></div>
</div>
@endif
<div class="add-access-token">
{{ Form::open(['method' => 'post','class'=>'form-inline']) }}
	<p>&nbsp;</p>
	<table class="table form-group-align">
		<tr>
			<td>
				<div class="form-group">
					{{ Form::label('api_key','API Key') }}
					{{ Form::text('api_key','', array('placeholder'=>'Enter API Key','class'=>'input-small')) }}
				</div>
			</td>
			<td>
				<div class="form-group">
					{{ Form::label('api_key_secret','API Key Secret') }}
					{{ Form::text('api_key_secret','',array('placeholder'=> 'Enter API Key Secret')) }}
				</div>			
			</td>
		</tr>
		<tr>
			<td>
				<div class="form-group">
					{{ Form::label('access_token','Access Token') }}
					{{ Form::text('access_token','',array('placeholder'=>'Enter Access Token')) }}
				</div>				
			</td>
			<td>
				<div class="form-group">
					{{ Form::label('access_token_secret','Access Token Secret') }}
					{{ Form::text('access_token_secret','',array('placeholder'=>'Enter Access Token Secret')) }}
				</div>			
			</td>
		</tr>
		<tr>
			<td>
				<div class="form-group">
					{{ Form::label('email','Email') }}
					{{ Form::text('email','',array('placeholder'=>'Enter Email Address','class'=>'input-field')) }}
				</div>				
			</td>
			<td>
				{{ Form::submit('Add',['class'=>'btn btn-info']) }}				
			</td>
		</tr>
	</table>
{{ Form::close() }}
</div>

<div class="access_token table-responsive" style="margin-top:50px">
	<div class="msg"></div>
	<table class="table table-striped text-center">
		<tr>
			<th>App Status</th>
			<th>API Key</th>
			<th>Email</th>
			<th>Updated At</th>
			<th>Status</th>
			<th colspan="3"></th>
		</tr>
	@foreach($access_tokens as $at)
		<tr id="app_{{ $at->_id }}">
			<td>
			@if($at->app_status)
				<div class="circle-status success circle-green">
					<i class="fa fa-check"></i>
				</div>
			@else
				<div class="circle-status danger circle-red">
					<i class="fa fa-times"></i>
				</div>
			@endif
			</td>
			<td>{{ $at->api_key }}</td>
			<td>{{ $at->email }}</td>
			<td>{{ date('d M Y H:i:s',strtotime($at->updated_at)) }}</td>
			<td>
				{{ Form::select(null, ['1' => 'Enable', '0' => 'Disable'], $at->is_active, ['id' => 'api_status_'.$at->_id]) }}
			</td>	
			<td><button class="api_show_btn btn-action" title="View"><i class="fa fa-eye"></i></button></td>
			<td><button class="api_text_btn btn-action" onclick="api_test('{{ $at->_id }}');" title="Test"><i class="fa fa-handshake"></i></button></td>
			<td><button class="api_save_btn btn-action save" onclick="api_save('{{ $at->_id }}');"><i class="fa fa-save"></i></button></td>
			<!-- 
			<td><button class="ap_delete_btn btn-action danger" onclick="api_delete('{{ $at->id }}');"><i class="fa fa-trash"></i></button></td>
			-->
		</tr>
		<tr style="display:none">
			<td colspan="6">
				<div class="">
					<p><strong>API Secret:</strong>{{ $at->api_secret }}</p>
					<p><strong>Access Token:</strong> {{ $at->access_token }}</p>
					<p><strong>Access Token Secret:</strong> {{ $at->access_token_secret }}</p>
				</div>
			</td>		
		</tr>
	@endforeach
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.api_show_btn').click(function(){
			$tr  = $(this).parent().parent().next();
			if ($tr.css('display') == 'none') {
				$(this).html('<i class="fa fa-eye-slash"></i>');
			}else{
				$(this).html('<i class="fa fa-eye"></i>');
			}			
			$(this).parent().parent().next().toggle();
		});
	});
	
	function api_save(id){
		var token 	= 	$('meta[name="csrf-token"]').attr('content');
		var status	=	$('#api_status_'+id).val();
		$('.msg').hide();
		$('.msg').addClass('alert');	
		$.ajax({
 	    	type:"POST", 
 	    	headers: {'X-CSRF-TOKEN': token},
 	    	url:"/core/ajax/update-access-token-status",
 	    	data: {id:id,status:status},
 	    	success:function(data,textstatus,xhr)
 	    	{ 
 	 	    	if(xhr.status == 200){
					$(".msg").html("Saved Successfully");
 	     		    $(".msg").addClass('alert-success')
 	     		    $(".msg").show();
 	     		    }
 	        	else if(xhr.status == 201){
 	  		    	$(".msg").html("Error Occured While Saving");
 	  		        $(".msg").addClass('alert-error')
 	  		        $(".msg").show();
 	  		   }     		                
 	        }    		     
		});    	    			   	
	}

	function api_delete(id){
		var token 	=	$('meta[name="csrf-token"]').attr('content');
		$('.msg').hide();
		$('.msg').addClass('alert');
		$.ajax({
			type: "POST",
			headers: {'X-CSRF-TOKEN':token},
			url: "/core/ajax/delete-access-token",
			data: {id:id},
			success: function(data,textstatus,xhr)
			{
				if(xhr.status == 200){
					$("#app_"+id).hide();
 	   		    	$(".msg").html("Delete Successfully");
 	     		    $(".msg").addClass('alert-success')
 	     		    $(".msg").show();
 	     		}
 	        	else if(xhr.status == 201){
 	  		    	$(".msg").html("Error Occured While Deleting");
 	  		        $(".msg").addClass('alert-error')
 	  		        $(".msg").show();
 	  			}     		               
			}
		});					
	}

	function api_test(id){
		var token 	=	$('meta[name="csrf-token"]').attr('content');
		$('.msg').hide();
		$('.msg').addClass('alert');
		$.ajax({
			type: "POST",
			headers: {'X-CSRF-TOKEN':token},
			url: "/core/ajax/test-access-token",
			data: {id:id},
			success: function(data,textstatus,xhr)
			{
				if(xhr.status == 200){
					var ss_html =	'<div class="circle-status success"><i class="fa fa-check"></i></div>';
					var ff_html	=	'<div class="circle-status danger"><i class="fa fa-times"></i></div>';
					if(data.app_status){
						$('#app_'+id).find('td:first').html(ss_html);						
					}else{
						$('#app_'+id).find('td:first').html(ff_html);	
					}	
					$(".msg").html("Test Completed");
 	     		    $(".msg").addClass('alert-success')
 	     		    $(".msg").show();
 	     		}
 	        	else if(xhr.status == 201){
	  		    	$(".msg").html("Error Occured");
 	  		        $(".msg").addClass('alert-error')
 	  		        $(".msg").show();
 	  			}     		               
			}
		});
	}
</script>
@endsection