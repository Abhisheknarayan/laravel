@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Search Track User
@endsection
@section('contentheader_title')
Search User
@endsection
<div style="margin:10px 0">
	<span class="label label-success">Total: <span id="total_users_in_db"><img src="{{ asset('img/loading.gif') }}" /></span> Users</span>  
	<span class="label label-warning">{{ $total_accounts }} Accounts</span>
</div>
{{ Form::open(['method' => 'get','class'=>'searchform']) }}
		{{ Form::label('Twitter Id', null, ['class' => 'search-id']) }}
		{{ Form::text('twitter_id','', array('placeholder'=>'Enter Twitter Id')) }}
		{{ Form::submit('search',['class'=>'btn btn-warning']) }}
		<a href="{{ url('/core/trackusers') }}"  class = "btn btn-primary">Reset</a>
{{ Form::close() }}


@if(!is_null($twitter_id))
    @if($tweets_count > 0)
    	<div class="table-responsive" style="margin-top:50px">
            <table class="table">
            	<thead>
            		<tr>
            			<th>Twitter_id</th>
            			<th>Tweets Count</th>
            			<th>Last Tweet Id</th>
            			<th>Created At</th>
            			<th>Last Updated</th>
            			<th></th>
            		</tr>
            	</thead>
            	<tbody>
            		<tr>
            			<td>{{ $twitter_id }}</td>
            			<td>{{ $tweets_count }}</td>
            			<td>{{ $taccount->last_tweet_id }}</td>
            			<td>{{ $taccount->created_at }}</td>
            			<td>{{ $taccount->updated_at }}</td>
            			@if($tweets_count > 0)
            				<td><a class="btn btn-info" href="{{ url('/core/user/tweets/download/') }}/{{$twitter_id}}">Download Tweets</a></td>
            			@endif
            		</tr>		
            	</tbody>
            </table>
        </div>
    @else
    	<div style="margin-top:10px;color:#ff0000">
	    	<p>User Not Found!</p>
    	</div>
    @endif
<hr />
@endif
<div class="table-responsive" style="margin-top:50px;">
    <table class="table" >
    	<thead>
    		<tr>
    			<th>Twitter_id</th>
    			<th>Last Tweet Id</th>
    			<th>Created At</th>
    			<th>Last Updated</th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($last_updated_ids as $lui)
    			<tr>
    				<td>{{ $lui->_id }}</td>
    				<td>{{ $lui->last_tweet_id }}</td>
    				<td>{{ $lui->created_at }}</td>
    				<td>{{ $lui->updated_at }}</td>
    			</tr>
    		@endforeach
    	</tbody>
    </table>
</div>
<script type="text/javascript">
    var token		= 	$("meta[name='csrf-token']").attr("content");    
	$.ajax({
    	type: "POST",
    	headers: {'X-CSRF-TOKEN': token},
    	url: "/core/ajax/countsavedtwitterusers",
    	data:{},
    	success: function(arr){
    		$("#total_users_in_db").html(arr.total_users);
    	}
     });
</script>
@endsection