<!-- Main Header -->
<header class="main-header">
	<div class="logo">
		<a  style="float:left;" data-toggle="offcanvas" role="button" class="sidebar-toggle"><i class="fas fa-bars"></i></a>
        <a href="{{ url('/dashboard') }}">
            <span class="logo-lg"><b>ALGODOM</b></span>
        </a>
    </div>
</header>