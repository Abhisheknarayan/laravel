<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">

            @foreach($sidebar['module'] as $key => $val)

                <li class="treeview {{ ($val->name == $current_module) ? 'active':'' }}">

                    <a href="#"><i class="{{ $val->icon }}" arial-hidden="true"></i><span>{{ $val->name }}</span></a>

                    <ul class="treeview-menu">

                        @foreach($val->menu as $key => $val)

                        <?php

                            $route = ltrim($val->route,'/');
                            
                        ?>

                        <li class={{ (Request::is($route)) ? 'active': '' }}><a href="{{ url($val->route) }}"><i class="{{ $val->icon }}"></i>{{ $val->name }}</a></li>

                        @endforeach

                    </ul>

                </li>

            @endforeach
        	
        	
        	<li>

				<a href="{{ url('/logout') }}" id="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
	               	<i class='fa fa-sign-out-alt'></i> <span>LogOut</span>
	            </a>
                
	            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
		        	{{ csrf_field() }}
	                <input type="submit" value="logout" style="display: none;">
	            </form>

	       	</li>

        </ul>
    </section>
</aside>