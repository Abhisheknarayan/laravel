@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    TweetKitApp API Log in
@endsection

@section('content')
<body class="hold-transition login-page">
    <div id="app" v-cloak>
        <div class="login-box">
            <div class="login-logo">
            	<a href="https://www.tweetkitapp.com"><img src="{{ asset('img/tweetkit.png') }}" alt="TweetKitApp"/></a> 
         	</div><!-- /.login-logo -->

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('message'))
            {!! Session::get('message') !!}
        @endif

        <div class="login-box-body">
        	<p class="login-box-msg"></p>
			<form method="post">
				{{ Form::token() }}
				<div id="result" class="alert alert-success text-center" style="display: none;"> Logged in! <i class="fa fa-refresh fa-spin"></i> Entering...</div>
				<div class="form-group has-feedback">
					<input type="email" placeholder="Email" name="email" value="" autofocus="autofocus" class="form-control"> <span class="glyphicon form-control-feedback glyphicon-envelope"></span> <!---->
				</div> 
				<div class="form-group has-feedback">
					<input type="password" placeholder="Password" name="password" class="form-control">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span> <!---->
				</div> 
				<div class="row">
					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label class="">
								<div class="icheckbox_square-blue" style="position: relative;">
									<input type="checkbox" name="remember" style="display: block; position: absolute; top: -20%; left: -20%; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
								</div> Remember Me
    						</label>
    					</div>
    				</div>
    				<div class="col-xs-4">
    					<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
    				</div>
    			</div>
    		</form>
	    </div>
    </div>
    </div>
</body>

@endsection