@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
FollowerAudit Profiles
@endsection
@section('contentheader_title')
FollowerAudit Profiles
@endsection
<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} profiles</span>
</div>
	
	<form class="search form-inline" action="{{ url('followeraudit/tracker-profiles') }}" method="post">
		<input name="_token" value="{{ csrf_token() }}" type="hidden">
	
        <div class="form-group">
            <label>Keyword</label>
            <input type="text" name="keyword" class="input-medium search-query" value="{{ $keyword }}">
        </div>

        <div class="form-group">
            <label>User Id</label>
            <input type="text" name="user_id" class="input-medium search-query" value="{{ $user_id }}">
        </div>

      	<button type="submit" class="btn btn-warning" >Search</button>

      	<a href="{{ url('/followeraudit/tracker-profiles') }}"  class = "btn btn-primary">Reset</a>

    </form>

	<div class="msg"></div>
    <div class="table-responsive" style="margin-top:30px">

    	<table class="table table-striped">
    		
            <thead>
        		<tr>
        			<th>User id</th>
            		<th>Keyword</th>
                    <th>Last entry</th>
                    <th>Status</th>
                    <th>Created at</th>
            		<th>Updated at</th>
            		<th>Action</th>
            	</tr>
    		</thead>

    		<tbody>
                
    			@foreach($profilesObj as $row)		
                    
    				<tr>
        				<td>{{ $row->user_id }}</td>

        				<td>{{ $row->search_keyword }}</td>

        				<td>{{ (!empty($tracker[$row->search_user_id])) ? date('d-M-y h:i:m A',strtotime($tracker[$row->search_user_id]->created_at)) : '--'  }}</td>

                        <td> {{ ($row->archive) ? 'Archived' : '' }} {{ ($row->delete) ? 'Deleted' : '' }} {{ ($row->status) ? 'Active':''  }} </td>

                        <td>{{ date('d-M-y h:i:m A',strtotime($row->updated_at)) }}</td>

        				<td>{{ date('d-M-y h:i:m A',strtotime($row->created_at)) }}</td>

        				<td>
                            <a target="_blank" href="https://www.followeraudit.com/block-fake-followers/{{ $row->token }}/{{ $row->search_keyword }}">
                                <button type="button" class="btn btn-success"> View </button>
                            </a>
                        </td>
        			</tr>

    			@endforeach		

    		</tbody>

    	</table>

    </div>

    <?php 

    	$query_arr = array();
    

        if($keyword!=''){
            $query_arr['keyword']    = $keyword;
        }

        if($user_id!=''){
            $query_arr['user_id']    = $user_id;
        }

    ?>

    {{ $profilesObj->appends($query_arr)->links() }}

@endsection

@section('scripts')
<script type="text/javascript">
   
</script>

@endsection