@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
FollowerAudit Users LImit
@endsection
@section('contentheader_title')
FollowerAudit Users Limit
@endsection
<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} users</span>
</div>
	
	<form class="search form-inline"  action="{{ url('/followeraudit/user-limit') }}" method="post">
		<input name="_token" value="{{ csrf_token() }}" type="hidden">
		
		<div class="form-group">
			<label>Twitter Id</label>
	      	<input type="text" name="twitter-id" class="input-medium search-query" value="{{ $twitter_id }}">	
		</div>

        <div class="form-group">
            <label>User Id</label>
            <input type="text" name="user-id" class="input-medium search-query" value="{{ $user_id }}">   
        </div>
		
      	<button type="submit" class="btn btn-warning" >Search</button>
      	<a href="{{ url('/followeraudit/user-limit') }}"  class = "btn btn-primary">Reset</a>
    </form>
    
    <?php if($twitter_id !='' || $user_id != ''):  ?>
   		<div style="margin:10px 0">Search Result : {{ $search_count }} user(s) </div>
	<?php endif; ?>	

	<div class="msg"></div>

    <div class="table-responsive" style="margin-top:30px" >
    	<table class="table table-striped">

    		<thead>

        		<tr>
                    <th>User Id</th>
            		<th>Number of audits</th>
            		<th>Followers per audits</th>
                    <th>Tracking Profiles</th>
                    <th>Profiles Followers upto</th>
            		<th>Download rows</th>
            		<th>Plan</th>
                    <th>Action</th>
            	</tr>

    		</thead>

    		<tbody>
                
    			@foreach($userLimitObj as $usr)		
                    
    				<tr>

                        <form class="user-limit-form" method="POST" action="{{ url('followeraudit/ajax-save-user-limit') }}">
                        <td>
                            {{ $usr->user_id }}
                        </td>
        				<td>
                            <input type="hidden" name="user_id" value="{{ $usr->user_id }}" class="">
                            <input type="text" name="audits" value="{{ $usr->audits }}" class="" >
                        </td>
        				<td>
                            <input type="text" name="audits_fo" value="{{ $usr->audits_fo }}" class="">
                        </td>
        				<td>
                            <input type="text" name="profiles" value="{{ $usr->profiles }}" class="">            
                        </td>
                        <td>
                            <input type="text" name="profiles_fo" value="{{ $usr->profiles_fo }}">
                        </td>
                        <td>
                            <input type="text" name="rows" value="{{ $usr->rows }}">
                        </td>
        				<td>
                            {{  Form::select('plan', config('followersaudit.pricing'), $usr->plan_id ) }}
                        </td>
                        <td>
                            <button type="submit" class="btn btn-success" class="save-button" > <i class="fa fa-save"></i> Save</button>
                        </td>

                        </form>

        			</tr>

    			@endforeach		

    		</tbody>
    	</table>
    </div>
    
<?php 

	$query_arr = array();
	
	if($twitter_id != ''){
		$query_arr['twitter-id']= $twitter_id;
	}

    if($user_id != ''){
        $query_arr['user-id'] = $user_id;
    }

?>

{{ $userLimitObj->appends($query_arr)->links() }}

@endsection

@section('scripts')
<script type="text/javascript">

    jQuery(document).ready(function($){

        jQuery(document).on('submit','.user-limit-form',function(e){

            e.preventDefault();

            var token       =   $("meta[name='csrf-token']").attr("content");

            $_data          =   $(this).serialize();

            $_url           =   $(this).attr('action');

            $(".msg").hide();   

            $.ajax({

                    type:"POST", 
                    headers: {'X-CSRF-TOKEN': token},
                    url: $_url,
                    data: $_data,
                    success:function(data,textstatus,xhr)
                    {

                        if(xhr.status == 200){
                            $(".msg").html("Saved Successfully");
                            $(".msg").addClass('alert alert-success')
                            $(".msg").show();
                            }
                        else if(xhr.status == 201){
                            $(".msg").html("Error Occured While Saving");
                            $(".msg").addClass('alert alert-error')
                            $(".msg").show();
                        } 

                    }    

               });                          

             return false;

        });

    });	  

</script>

@endsection