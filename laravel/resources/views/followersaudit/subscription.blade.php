@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
FollowerAudit Subscriptions
@endsection
@section('contentheader_title')
FollowerAudit Subscriptions
@endsection

    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">

    <div style="margin:10px 0;">
    	<span class="label label-success">Total: {{ $total_count }} subscriptions</span>
    </div>
	
	<form class="search form-inline" action="{{ url('followeraudit/subscription') }}" method="post">
		<input name="_token" value="{{ csrf_token() }}" type="hidden">
        <div class="form-group">
            <label>Status</label>
            {{ Form::select('status', array(''=>'Select','active' => 'Active', 'canceled' => 'Cancel','-1'=>'Empty'),'') }}
        </div>
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="input-medium search-query" value="{{ $email }}">
      	</div>
        <div class="form-group">
            <label>User Id</label>
            <input type="text" name="user_id" class="input-medium search-query" value="{{ $user_id }}">
        </div>
      	<button type="submit" class="btn btn-warning" >Search</button>
      	<a href="{{ url('/followeraudit/subscription') }}"  class = "btn btn-primary">Reset</a>
    </form>

	<div class="msg"></div>
    <div class="table-responsive" style="margin-top:30px">

    	<table class="table table-striped">
    		
            <thead>
        		<tr>
                    <th>User Id</th>
            		<th>Email</th>
            		<th>Plan</th>
                    <th>Expire Time</th>
            		<th>Added Date</th>
            		<th>Status</th>
            	</tr>
    		</thead>

    		<tbody>
                
    			@foreach($subscriptions as $row)		
                    
    				<tr>

                        <td>{{ $row->id }}</td>

        				<td>{{ $row->email }}</td>

        				<td>{{ $row->stripe_plan }}</td>

                        <td>{{ ($row->ends_at==null) ? '--' : $row->ends_at }}</td>

        				<td>{{ date('d-M-y h:i:m A',strtotime($row->subscription_date)) }}</td>

        				<td><?= ($row->stripe_status=='active') ? '<p class="text-success">'.ucfirst($row->stripe_status).'</p>' : '<p class="text-danger">'.ucfirst($row->stripe_status).'</p>';  ?> </td>
        			</tr>

    			@endforeach		

    		</tbody>

    	</table>

    </div>
    
<?php 

	$query_arr = array();
	
	if($email!=''){

	 	$query_arr['email'] = $email;

	}

    if($user_id!=''){

        $query_arr['user_id'] = $user_id;

    }

    if($status != ''){

        $query_arr['status']  =   $status;  

    }

?>
    {{ $subscriptions->appends($query_arr)->links() }}

@endsection

@section('scripts')
<script type="text/javascript">
   
</script>

@endsection