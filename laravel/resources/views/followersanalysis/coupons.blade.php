@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Coupons
@endsection
@section('contentheader_title')
Coupons
@endsection
<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_coupons }} coupons</span>
</div>

@if(count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>    
         </div>
@endif
@if(Session::has('add_coupon_failed'))
<div class = "alert alert-danger">
   <ul>
        <li>
			<div class="add-coupon-error"><p>{!! session('add_coupon_failed') !!}</p></div>
	     </li>
   </ul>    
</div>
@endif
@if(Session::has('add_coupon_success'))
<div class = "alert alert-success">
   <ul>
        <li>
			<div class="add-coupon-success"><p>{!! session('add_coupon_success') !!}</p></div>
	     </li>
   </ul>    
</div>
@endif

@if(Session::has('update_coupon_failed'))
<div class = "alert alert-danger">
   <ul>
        <li>
			<div class="add-coupon-error"><p>{!! session('update_coupon_failed') !!}</p></div>
	     </li>
   </ul>    
</div>
@endif
@if(Session::has('update_coupon_success'))
<div class = "alert alert-success">
   <ul>
        <li>
			<div class="add-coupon-success"><p>{!! session('update_coupon_success') !!}</p></div>
	     </li>
   </ul>    
</div>
@endif
<hr />
{{ Form::open(['method' => 'post','class'=>'addcouponform form-horizontal','action' => 'FollowersAnalysis\FAWebsiteAdminController@add_coupons']) }}
	<input type="hidden" name ="hidden_id" value = "0" id="hidden">
   	<div id="coupontext">Add New Coupon</div>
   	
   	<div class="row">
   		<div class="col-sm-12 col-lg-6">
		   	<div class="form-group">
		   		<label class="control-label" for="coupon_code">Coupon Code</label>
				<div class="controls">
					{{ Form::text('coupon_code','', array('placeholder'=>'Enter Coupon','class'=>'addcoupon','id'=>'coupon_code')) }}			
				</div>
		   	</div>   		
   		</div>
   		<div class="col-sm-12 col-lg-6">
		   	<div class="form-group">
		   		<label for="coupon_type" class="control-label">Coupon Type</label>
		   		<div class="controls">
					<select name="coupon_type" id="coupon_type">
						<option value="1">Percentage</option>
						<option value="2">Fixed</option>	
					</select>   		
		   		</div>
		   	</div>   		
   		</div>
   	</div>
   	<div class="row">
   		<div class="col-lg-6">
			<div class="form-group">
				<label for="coupon_amount" class="control-label">Amount</label>
				<div class="controls">
					{{ Form::text('coupon_amount','', array('placeholder'=>'Amount','class'=>'adddesc','id'=>'coupon_amount')) }}			
				</div>
			</div>   			
   		</div>
   		<div class="col-lg-6">
			<div class="form-group">
				<label for="coupon_users" class="control-label">Users</label>
				<div class="controls">
					<textarea name="coupon_users" id="coupon_users"></textarea>
				</div>
			</div>   			
   		</div>
   	</div>
   	<div class="row">
   		<div class="col-lg-6">
   			<div class="form-group">
   				<label for="coupon_products" class="control-label">Products</label>
   				<div class="controls">
   					<select name="coupon_products[]" id="coupon_products" multiple>
   						<option value="1">Download Followers</option>
   						<option value="2">Download Tweets</option>
   					</select>
   				</div>
   			</div>	
   		</div>
   		<div class="col-lg-6">
   			<div class="form-group">
   				<label for="c_max_product" class="control-label">Max Products in Cart</label>
   				<div class="controls">
   					<input type="text" name="c_max_product" id="c_max_product" value="">
   				</div>
   			</div>
   		</div>
   	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="coupon_per_user" class="control-label">Coupons per user</label>
				<div class="controls">
					{{ Form::text('coupon_per_user','', array('placeholder'=>'','class'=>'cperuser','id'=>'coupon_per_user')) }}
				</div>
			</div>			
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<label class="control-label" for="datepicker">Expire At:</label>
				<div class="controls">
					<input type="text" value="" name="date" id="datepicker">		
				</div>
			</div>			
		</div>
	</div>
	<div class="control-group">
		<label class="control-label"></label>
		<div class="controls">
			{{ Form::submit('Add',array('id'=>'save','class' => 'submitbtn btn btn-warning')) }}   		
		</div>
	</div>
{{ Form::close() }}
<hr />
	<div class="msg"></div>
	<div><h2>List of Coupons</h2></div>
	<div>
		{{ Form::open(['method' => 'post','class'=>'searchform']) }}
				{{ Form::label(null, null, ['class' => 'search-coupon']) }}
				{{ Form::text('q','', array('placeholder'=>'Search Coupon or User')) }}
				{{ Form::submit('search',['class'=>'btn btn-warning']) }}
				<a href="{{ url('/fa/coupons') }}"  class = "btn btn-primary">Reset</a>
				
			{{ Form::close() }}
			<p>&nbsp;</p>
	</div>
	<?php
	  if($search !='') 
		echo  "<p>Search Result : ".$search_count."</p>";
	?>	   
	<div class="table-responsive" style="margin-top:50px">
        <table class="table table-striped text-center">
    		<thead>
    			<tr>
    				<th>Coupon Code</th>
    				<th>Coupon Type</th>
    				<th>Amount</th>
    				<th>Users</th>
    				<th>Total Users</th>
    				<th>Products</th>
    				<th>Max Products In Cart</th>
    				<th>Coupons Per User</th>
    				<th>Expire At</th>
    				<th></th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($coupons as $c)
    				<?php 
    					if($c->users != '') {
    						$total_users	=	sizeof(explode(',',$c->users));
    					}else{
    						$total_users	=	'Unlimited';
    					}
    					
    					if($c->max_products_in_cart == 0){
    						$max_products 	=	'Unlimited';
    					}else{
    						$max_products	=	$c->max_products_in_cart;
    					}
    					
    					$products 			=	explode(',',$c->products);
    					$products_str 		=	array();
    					foreach ($products as $p){
    						if($p == 1){
    							$products_str[]	=	'F';
    						}
    						if($p == 2){
    							$products_str[]	=	'T';
    						}
    					}
    					$products_str	=	implode(',', $products_str);
    					
    				?>			
    				<tr id="listcouptbl{{ $c->id }}">
    					<td id ="coupon_code_{{ $c->id }}" >{{ $c->coupon_code }}</td>
    					<td id ="coupon_type_{{ $c->id }}" title="{{ $c->discount_type }}" >{{ ($c->discount_type == 1) ? 'Percentage': 'Fixed' }}</td>
    					<td id ="coupon_amount_{{ $c->id }}">{{ $c->amount }}</td>
    					<td id="coupon_users_{{ $c->id }}" title="{{ $c->users }}">{{ substr($c->users,0,10) }}..</td>
    					<td>{{ $total_users }}</td>
    					<td id="c_products_{{ $c->id }}"  title="{{ $c->products }}">{{ $products_str }}</td>
    					<td id="c_max_product_{{ $c->id }}" title="{{ $c->max_products_in_cart }}" >{{ $max_products }}</td>
    					<td id ="coupons_per_user_{{ $c->id }}" >{{ $c->per_user_coupon }}</td>
    					<td id ="exp{{ $c->id }}" >{{ $c->expired }}</td>
    					<td><button id='edit_<?php echo $c->id?>' type="button" class="btn-action editbtn"><i class="fa fa-edit"></i></button></td>
    					<td><button id="delete_<?php echo $c->id?>" type="button" class="btn-action danger deletebtn"><i class="fa fa-trash"></i></button></td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
	</div>
	<?php 
		$query_arr = array();
		if($search != ''){
			$query_arr['q'] = $search;
		}
	?>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{ asset('css/sumoselect.min.css') }}" />
<script src="{{ asset('js/jquery.sumoselect.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#coupon_products').SumoSelect();
	$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });

	$(".editbtn").on("click",function() {
		 var id		 	=	this.id.split('_');
		 id 			=	id[1];
		 var coupon_code		=	$("#coupon_code_"+id).html();
		 var coupon_type		=	$("#coupon_type_"+id).attr('title');
		 var coupon_amount		=	$("#coupon_amount_"+id).html();
		 var coupon_users		=	$('#coupon_users_'+id).attr('title');
		 var coupons_per_user	=	$('#coupons_per_user_'+id).html();
		 var coupons_products	=	$('#c_products_'+id).attr('title');
		 var max_products		=	$('#c_max_product_'+id).attr('title');			
		 var exp				=   $("#exp"+id).html();	
		 cp_arr					=	coupons_products.split(','); 
		
		 $("#coupon_code").val(coupon_code);
		 $("#coupon_type").val(coupon_type);
		 $("#coupon_amount").val(coupon_amount);
		 $('#coupon_users').val(coupon_users)
		 $("#coupon_per_user").val(coupons_per_user);
		 $("#c_max_product").val(max_products);
		 $('#coupon_products').val(cp_arr);
		 $('#coupon_products')[0].sumo.reload();		 
		 $("#datepicker").val(exp);
		 
		 $("#save").val("Update");
		 $("#hidden").val(id);
		 $("#coupontext").html("<b>Update Coupon</b>");
		 $("html, body").animate({ scrollTop: 0 }, "slow");	   	
		  return false;
	});
	
	$(".deletebtn").on("click",function(){
		
    	var id 			=	this.id.split('_');
    	id				=	id[1]
    	var token		= 	$("meta[name='csrf-token']").attr("content");
    	
    	$.ajax({
			type:"POST",
			headers: {'X-CSRF-TOKEN': token},
		    url:"/fa/ajax/delcoupon",
	        data: {id:id},
	        success:function(data,textstatus,xhr)
	         { 
		         if(xhr.status == 200){
		        	    $("#listcouptbl"+id).remove();
	                    $(".msg").html("Deleted Successfully");
		                $(".msg").addClass('alert alert-success')
		                $(".msg").show();
		                }
		         else if(xhr.status == 201){
	                    $(".msg").html("Error Occured While Deleting");
	                    $(".msg").addClass('alert alert-danger')
	                    $(".msg").show();
	                }     		                
	         }    		     
	    });
	    return false;

	});
});
</script>
@endsection