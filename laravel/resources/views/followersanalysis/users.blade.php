@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Users
@endsection
@section('contentheader_title')
Users
@endsection
<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} users</span>
	<span class="label label-danger">Not Active: {{ $not_active_account }} users</span>
</div>

<div class="fa-user-filter">	
	<form class="search form-inline" method="post">
		<input name="_token" value="{{ csrf_token() }}" type="hidden">
		<div class="form-group">
			<label>Status</label>
			{{ Form::select('status', array(''=>'Select','1' => 'Enable', '0' => 'Disable'),'') }}
		</div>
        <div class="form-group">
            <label>User Id</label>
            <input type="text" name="user-id" class="input-medium search-query" value="{{ $user_id }}">   
        </div>
		<div class="form-group">
			<label>Twitter Id</label>
	      	<input type="text" name="twitter-id" class="input-medium search-query" value="{{ $twitter_id }}">	
		</div>
		<div class="form-group">
			<label>Username</label>
			<input type="text" name="username" class="input-medium " value="{{ $username }}">
      	</div>
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="input-medium" value="{{ $email }}">
      	</div>
        <br></br>
      	<button type="submit" class="btn btn-warning" >Search</button>
      	<a href="{{ url('/fa/users') }}"  class = "btn btn-primary">Reset</a>
    </form>
</div>

    <?php if($user_id != '' || $status != '' || $twitter_id !='' || $username!='' || $email!=''):  ?>
   		<div style="margin:10px 0">Search Result : {{ $search_count }} user(s) </div>
	<?php endif; ?>	
	<div class="msg"></div>
    <div class="table-responsive" style="margin-top:30px" >
    	<table class="table table-striped">
    		<thead>
        		<tr>
        			<th>User Id</th>
                    <th>Twitter Id</th>
            		<th>Username</th>
            		<th>Twitter email</th>
            		<th>Added Date</th>
            		<th>Status</th>
                    <th>Action</th>
            	</tr>
    		</thead>
    		<tbody>
    			@foreach($users as $usr)			
    				<tr>
        				<td>{{ $usr->id }}</td>
                        <td>{{ $usr->twitter_id }}</td>
        				<td>{{ $usr->username }}</td>
        				<td>{{ $usr->twitter_email }}</td>
        				<td>{{ date('d-M-y h:i:m A',strtotime($usr->added_date)) }}</td>
        				<td>{{ Form::select('status', array('1' => 'Enable', '0' => 'Disable'), $usr->is_active, array('id'=>'status_'.$usr->id,'class'=>'user-status')) }}</td>

                        <td>
                            <a target="_blank" class="btn btn-primary" href="https://www.followersanalysis.com/mlogin?twitter_id={{ $usr->twitter_id }}">
                                Login
                            </a>
                        </td>

        			</tr>	
    			@endforeach			
    		</tbody>
    	</table>
    </div>
<?php 
	
    $query_arr = array();

    if($user_id != ''){
        $query_arr['user-id']    = $user_id;
    }

	if($status != ''){
		$query_arr['status'] 	= $status;
	}

	if($twitter_id != ''){
		$query_arr['twitter-id']= $twitter_id;
	}

	if($username!=''){
	 	$query_arr['username'] 	= $username;
	}

	if($email!=''){
	 	$query_arr['email'] = $email;
	}
?>
{{ $users->appends($query_arr)->links() }}
@endsection

@section('scripts')
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".user-status").on('change',function(){
    	var token		= 	$("meta[name='csrf-token']").attr("content");    			
    	var status		=   this.value;	
    	var id_str		=	this.id.split('_');
		var id 			=	id_str[1];
    	$(".msg").hide();	
  	    $.ajax({
    	    	type:"POST", 
    	    	headers: {'X-CSRF-TOKEN': token},
    	    	url:"/fa/ajax/userssave",
    	    	data: {id:id,status:status},
    	    	success:function(data,textstatus,xhr)
    	    	{ 
    	        	if(xhr.status == 200){
    	   		    	$(".msg").html("Saved Successfully");
    	     		    $(".msg").addClass('alert alert-success')
    	     		    $(".msg").show();
    	     		    }
    	        	else if(xhr.status == 201){
    	  		    	$(".msg").html("Error Occured While Saving");
    	  		        $(".msg").addClass('alert alert-error')
    	  		        $(".msg").show();
    	  		   }     		                
    	        }    		     
  		   });    	    			   	

  	     return false;
      });
});	    
</script>

@endsection