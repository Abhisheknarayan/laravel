@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Transactions
@endsection
@section('contentheader_title')
Transactions
@endsection
<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} Transactions</span>
</div>

{{ Form::open(['method' => 'post','class'=>'form-inline']) }}
	<div class="form-group">
		{{ Form::label('Username', null, ['class' => 'add-user']) }}
		{{ Form::text('username','', array('placeholder'=>'Enter username')) }}
	</div>
	<div class="form-group">
		{{ Form::label('Twitter Id', null, ['class' => 'search-twitter_id']) }}
		{{ Form::text('twitter_id','', array('placeholder'=>'Enter Twitter Id')) }}
	</div>
	{{ Form::submit('Search',['class' => 'btn btn-warning']) }}
	<a href="{{ url('/') }}/fa/transactions"  class = "btn btn-primary">Reset</a>
{{ Form::close() }}
<?php if($username != '' || $twitter_id !=''):  ?>
		<div style="margin:10px 0">Search Result : {{$user_trans_count}} user(s) </div>
<?php endif;?>	   
<div class="table-responsive" style="margin-top:30px" >
    <table class="table table-striped text-center">
    	<tr>
    		<th>Twitter Id</th>
    		<th>First Name</th>
    		<th>Last Name</th>
    		<th>Payer Email</th>
    		<th>Transaction Date</th>
    		<th>Username</th>
    		<th>Download Report</th>
    	</tr>
    @foreach($user_trans as $usr)
    	<?php 
    		$url          = config('tweetkitapp.fa_base_url').'/download/premiumreport/'.md5($usr->twitter_id).'/'.$usr->id;
    		$invoice_url  = config('tweetkitapp.fa_base_url').'/invoice/'.$usr->twitter_id.'/'.($usr->id+1105);
    		
    	?>
    	<tr>
    		<td>{{ $usr->twitter_id }}</td>
    		<td>{{ $usr->first_name }}</td>
    		<td>{{ $usr->last_name }}</td>
    		<td>{{ $usr->payer_email }}</td>
    		<td>{{ $usr->transaction_date }}</td>
    		<td><a href="https://www.twitter.com/{{ $usr->username }}" target="blank">{{ $usr->username }}</a></td>
    		<td>
    			<a title="Download Report" href="{{ $url }}"  class = "btn-action" target="_blank"><i class="fa fa-download"></i></a>
    			<a title="View Invoice" href="{{ $invoice_url }}"  class = "btn-action" target="_blank"><i class="fa fa-file"></i></a>
    		</td>
    	</tr>
    @endforeach
    </table>
</div>
<?php 
		$query_arr = array();
		if($username != '')
			$query_arr['username'] 			= 	$username;
		if($twitter_id != '')	
			$query_arr['twitter_id']		=	$twitter_id;
		
?>
{{ $user_trans->appends($query_arr)->links() }}
@endsection