<div class="modal-header">     
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>

    <h5 class="modal-title">Edit Module</h5>
</div>

<form name="edit-module-form" id="edit-module-form" action="{{ url('/useraccess/ajax-edit-module') }}" method="POST">

    <div class="modal-body">

        <div class="edit-flash-message"></div>

        {{ csrf_field() }}

        <div class="form-group">

            <label>Name</label>

            <input type="hidden" value="{{ $data->id }}" name="id">

            <input type="text" name="name" value="{{ $data->name }}" class="form-control" placeholder="Name">
            <p class="text-danger edit_error_name"></p>

        </div>

        <div class="form-group">

            <label>Icon</label>

            <input type="text" name="icon" value="{{ $data->icon }}" class="form-control" placeholder="fa fa-icon">

            <p class="text-danger edit_error_icon"></p>

        </div>

        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control">
                <option value="1">Enable</option>
                <option {{ ($data->status==0) ? 'selected':'' }} value="0">Disable</option>
            </select>
        </div>

    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</form>