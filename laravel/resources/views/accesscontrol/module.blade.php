@extends('adminlte::layouts.app')

@section('main-content')

    @section('htmlheader_title')
    Tweetkit api module
    @endsection
    @section('contentheader_title')
    Module
    @endsection

    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">

    <div class="user_filter">
    	<button id="add-module-button" class="btn btn-warning"><i class="fa fa-plus"></i> Add Module</button>
    </div>

	<div class="msg"></div>

    <div class="table-responsive" style="margin-top:30px" >
    	
        <table class="table table-striped">
    		<thead>
        		<tr>
                    <th>Id</th>
                    <th>Icon</th>
                    <th>Name</th>
                    <th>Status</th>
        			<th>Created At</th>
            		<th>Updated At</th>
                    <th>Action</th>
            	</tr>
    		</thead>
    		<tbody>

                @foreach($module as $row)
                    
                    <tr>
                        <td>{{ $row->id }}</td>
                        <td><i class="{{ $row->icon }}"></i></td>
                        <td>{{ $row->name }}</td>
                        <td>{!! ($row->status==1)? '<p class="text-success">Enable</p>' : '<p class="text-danger">Disable</p>' !!}</td>
                        <td>{{ $row->created_at }}</td>
                        <td>{{ $row->updated_at }}</td>
                        <td>
                            <button type="button" data-value="{{ $row->id }}" class="btn btn-success edit-module">Edit</button>
                            <button type="button" data-value="{{ $row->id }}" class="btn btn-danger delete-module">Delete</button>
                        </td>
                    </tr>

                @endforeach

    		</tbody>
    	</table>

    </div>

    <?php  $query_arr = array(); ?>

    {{ $module->appends($query_arr)->links() }}

    <div id="addModal" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>

            <h5 class="modal-title">Add Module</h5>

          </div>

          <form name="add-module-form" id="add-module-form" action="{{ url('/useraccess/ajax-add-module') }}" method="POST">

            <div class="modal-body">

                <div class="add-flash-message"></div>

                {{ csrf_field() }}

                <div class="form-group">

                    <label>Name</label>

                    <input type="text" name="name" value="" class="form-control" placeholder="Name">
                    <p class="text-danger error_name"></p>

                </div>

                <div class="form-group">

                    <label>Icon</label>

                    <input type="text" name="icon" value="" class="form-control" placeholder="fa fa-icon">

                    <p class="text-danger error_icon"></p>

                </div>

            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

          </form>

        </div>
      </div>
    </div>

    <div id="editModal" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
      </div>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">

        $('#add-module-button').on('click',function(){

            $('#addModal').modal('show');

        });

        $(document).on('submit','#add-module-form',function(e){

            e.preventDefault();
            
            var form = $(this);

            var url = form.attr('action');

            $.ajax({

                type:"POST",
                url:url,
                data:form.serialize(),
                headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                success:function(data,textstatus,xhr){

                    $('.text-danger').empty();
                    
                    if(xhr.status==200 && data.status==200){
                        
                        $('.add-flash-message').html('<div class="alert alert-success"><strong>Success!  </strong>'+data.message+'.</div>');

                        setTimeout(function(){location.reload()},1000);
                    }
                    else if(xhr.status==200 && data.status==400){
                        $('.error_name').text(data.data.name);
                        $('.error_icon').text(data.data.icon);
                    }

                },
                error:function(){
                    $('.add-flash-message').html('<div class="alert alert-danger"><strong>Error! </strong> Something went wrong.</div>');
                }

            });

        })

        $(document).on('click','.edit-module',function(){

           var Id       = $(this).data('value');

           $.ajax({
                
                type:"POST",
                url:"{{ url('/useraccess/edit-module') }}",
                data:{id:Id},
                headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},
                success:function(data,textstatus,xhr){

                    if(xhr.status==200){

                        $('#editModal .modal-content').html(data);

                        $('#editModal').modal('show');

                    }

                },
                error:function(){

                }

            });

        });

        $(document).on('submit','#edit-module-form',function(e){

            e.preventDefault();
            
            var form = $(this);

            var url = form.attr('action');

            $.ajax({

                type:"POST",
                url:url,
                data:form.serialize(),
                headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                success:function(data,textstatus,xhr){

                    if(xhr.status==200 && data.status==200){

                        $('.edit-flash-message').html('<div class="alert alert-success"><strong>Success!  </strong>'+data.message+'.</div>');

                        setTimeout(function(){location.reload()},1000);

                    }
                    else if(xhr.status==200 && data.status==400){
                        $('.edit_error_name').text(data.data.name);
                        $('.edit_error_icon').text(data.data.icon);
                    }

                },
                error:function(){
                    $('.edit-flash-message').html('<div class="alert alert-danger"><strong>Error! </strong> Something went wrong.</div>');
                }

            });

        })

        $(document).on('click','.delete-module',function(){

            if(confirm('Are you sure to delete this?')){
                
                var id = $(this).data('value');

                $.ajax({

                    type:'POST',
                    url:'{{ url("/useraccess/ajax-delete-module") }}',
                    data:{id:id},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    success:function(data,textstatus,xhr){
                        
                        if(xhr.status==200){
                            $(".msg").html(data.message);
                            $(".msg").addClass('alert alert-success');
                            $(".msg").show();
                        }

                        setTimeout(function(){location.reload()},1000);

                    },
                    error:function(){
                        $(".msg").html("Something went wrong.");
                        $(".msg").addClass('alert alert-danger');
                        $(".msg").show();
                    }
                });

            }

        });

    </script>

@endsection