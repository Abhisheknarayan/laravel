@extends('adminlte::layouts.app')

@section('main-content')

    @section('htmlheader_title')
    Routes
    @endsection
    @section('contentheader_title')
    Routes
    @endsection

    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">

    <div class="mb-2">
    	<button id="add-route-button" class="btn btn-warning"><i class="fa fa-plus"></i> Add Routes</button>
    </div>

	<div class="msg"></div>

    <div class="row filter mt-2">

        <div class="col-md-4 col-12">
        
            <select id="module_search" name="module_search"  class="form-control">
                    <option value="">Select Module</option>
                @foreach($module as $row)
                    <option {{ ($row->name==$search_module)? 'selected':'' }} value="{{ $row->name }}"> {{ $row->name }} </option>
                @endforeach
            </select>

        </div>

        <div class="col-md-4 col-12">
            <input type="text" class="form-control" value="{{ (!empty($search_route)) ? $search_route : '' }}" id="route_search" name="route_search" placeholder="Route search">
        </div>

        <div class="col-md-2">
            <button type="button" id="search-buttton" class="btn btn-success">Search</button>
            <a href="{{ url('/useraccess/add-routes/') }}">
                <button type="button" id="reset-buttton" class="btn btn-danger">Reset</button>
            </a>
        </div>

        
    </div>

    <div class="table-responsive" style="margin-top:30px" >
    	
        <table class="table table-striped">
    		<thead>
        		<tr>
                    <th>Route Id</th>
                    <th>Module</th>
                    <th>Type</th>
        			<th>Name</th>
            		<th>Route</th>
                    <th>Controller</th>
            		<th>Method</th>
            		<th>Status</th>
                    <th>Action</th>
            	</tr>
    		</thead>
    		<tbody>

                @foreach($routes as $row)

                    <tr>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->module->name }}</td>
                        <td>{{ ($row->menu_subgroup==0) ? 'Menu':'Related routes'  }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->route }}</td>
                        <td>{{ $row->controller }}</td>
                        <td>{{ $row->method }}</td>
                        <td>{!! ($row->status==1)? '<p class="text-success">Active</p>' : '<p class="text-danger">Deactive</p>' !!}</td>
                        <td>
                            <button type="button" data-value="{{ $row->id }}" data-subgroup="{{ $row->menu_subgroup }}" class="btn btn-success edit-routes">Edit</button>
                            <button type="button" data-value="{{ $row->id }}" class="btn btn-danger delete-routes">Delete</button>
                        </td>
                    </tr>

                @endforeach

    		</tbody>
    	</table>

    </div>

    <?php  

        $query_arr                  = array(); 
        
        if(!empty($search_module)){
            $query_arr['module']    = $search_module;
        }

        if(!empty($search_route)){
            $query_arr['route']     = $search_route;
        } 

    ?>

    {{ $routes->appends($query_arr)->links() }}

    <div id="addModal" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title">Add Routes</h5>

          </div>

          <form name="route-form" id="route-form" action="{{ url('/useraccess/ajax-add-routes') }}" method="POST">

              <div class="modal-body">

                    <div class="add-flash-message"></div>

                    {{ csrf_field() }}

                    <div class="form-group">

                        <label for="menu_group">Select Module</label>

                        <select id="menu_group" name="menu_group" class="form-control" required>
                            @foreach($module as $key => $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach 
                        </select>
                    </div>

                    <div class="form-group">

                        <label for="menu_type">Select Type</label>

                        <select id="menu_type" name="menu_type" class="form-control">
                            <option value="0">Menu</option> 
                            <option value="1">Sub Menu</option>
                        </select>

                    </div>

                    <div class="form-group" id="menu-from-group" style="display: none;">

                        <label for="menu">Select Menu</label>

                        <select id="menu" name="menu" class="form-control">
                            <option value="0">Menu</option> 
                        </select>

                        <p class="text-danger error_menu"></p>

                    </div>

                    <div class="form-group">

                        <label>Name</label>

                        <input type="text" name="name" value="" class="form-control" placeholder="Dashboard">
                        <p class="text-danger error_name"></p>

                    </div>

                    <div class="form-group">

                        <label>Icon</label>

                        <input type="text" name="icon" value="" class="form-control" placeholder="fa fa-dashboard">
                        <p class="text-danger error_icon"></p>

                    </div>

                    <div class="form-group">

                        <label>Route</label>

                        <input type="text" name="route" value="" class="form-control" placeholder="/dashboard">

                        <p class="text-danger error_route"></p>

                    </div>

                    <div class="form-group">

                        <label>Controller</label>

                        <input type="text" name="controller" value="" class="form-control" placeholder="HomeController@dashboard">

                        <p class="text-danger error_controller"></p>

                    </div>

                    <div class="form-group">

                        <label>Method</label>

                        <select id="method" name="method" class="form-control">
                            <option value="get">Get</option> 
                            <option value="post">Post</option>
                        </select>

                    </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>

          </form>

        </div>
      </div>
    </div>

    <div id="editModal" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
      </div>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">

        $(document).on('change','#menu_type',function(){
            var menuType = $(this).val();
            
            if(menuType==1){
                $('#menu-from-group').css({display:'block'});
            }
            else{
                $('#menu-from-group').css({display:'none'});
            }

        });

        $(document).on('change','#edit_menu_type',function(){
            var menuType = $(this).val();
            
            if(menuType==1){
                $('#edit-menu-from-group').css({display:'block'});
            }
            else{
                $('#edit-menu-from-group').css({display:'none'});
            }

        });

        $('#add-route-button').on('click',function(){

            $('#addModal').modal('show');
        });

        $(document).on('change','#menu_group',function(){
            var moduleId = $(this).val();
            ajaxMenuList(moduleId,'menu');
        });

        $(document).on('change','#edit_menu_group',function(){
            var moduleId = $(this).val();
            ajaxMenuList(moduleId,'edit-menu');
        });

        function ajaxMenuList(moduleId,div){
            $.ajax({

                type:'POST',
                url:'{{ url("/useraccess/ajax-menu-list") }}',
                data:{'moduleId':moduleId},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend:function(){
                    console.log('processing..');
                },
                success:function(data,textstatus,xhr){
                    if(xhr.status == 200){
                        
                        $('#'+div).empty();

                        $.each(data.data,function(i,v){

                            $('#'+div).append('<option value="'+v.id+'">'+v.name+'</option>');

                        });

                    }
                },
                error:function(){
                    $(".msg").html("Something went wrong.");
                    $(".msg").addClass('alert alert-danger')
                    $(".msg").show();
                }
            });
        }

        $(document).on('submit','#route-form',function(e){

            e.preventDefault();
            
            var form = $(this);

            var url = form.attr('action');

            $.ajax({

                type:"POST",
                url:url,
                data:form.serialize(),
                headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                success:function(data,textstatus,xhr){
                    
                    $('.text-danger').empty();

                    if(xhr.status==200 && data.status==200){
                        
                        $('.add-flash-message').html('<div class="alert alert-success"><strong>Success!  </strong>'+data.message+'.</div>');

                        setTimeout(function(){location.reload()},1000);
                    }
                    else if(xhr.status==200 && data.status==400){
                        $('.error_name').text(data.data.name);
                        $('.error_route').text(data.data.route);
                        $('.error_controller').text(data.data.controller);
                    }

                },
                error:function(){
                    $('.add-flash-message').html('<div class="alert alert-danger"><strong>Error! </strong> Something went wrong.</div>');
                }

            });

        })

        $(document).on('click','.edit-routes',function(){

           var Id       = $(this).data('value');
           var subgroup = $(this).data('subgroup');

           $.ajax({
                
                type:"POST",
                url:"{{ url('/useraccess/edit-routes') }}",
                data:{id:Id,'subgroup':subgroup},
                headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},
                success:function(data,textstatus,xhr){

                    if(xhr.status==200){

                        $('#editModal .modal-content').html(data);

                        $('#editModal').modal('show');

                    }

                },
                error:function(){

                }

            });

        });

        $(document).on('submit','#edit-route-form',function(e){

            e.preventDefault();
            
            var form = $(this);

            var url = form.attr('action');

            $.ajax({

                type:"POST",
                url:url,
                data:form.serialize(),
                headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                success:function(data,textstatus,xhr){

                    $('.text-danger').empty();

                    if(xhr.status==200 && data.status==200){

                        $('.edit-flash-message').html('<div class="alert alert-success"><strong>Success!  </strong>'+data.message+'.</div>');

                        setTimeout(function(){location.reload()},1000);

                    }
                    else if(xhr.status==200 && data.status==400){
                        $('.edit_error_name').text(data.data.name);
                        $('.edit_error_route').text(data.data.route);
                        $('.edit_error_controller').text(data.data.controller);
                    }

                },
                error:function(){
                    $('.edit-flash-message').html('<div class="alert alert-danger"><strong>Error! </strong> Something went wrong.</div>');
                }

            });

        })

        $(document).on('click','.delete-routes',function(){

            if(confirm('Are you sure to delete this?')){

                var id = $(this).data('value');

                $.ajax({

                    type:'POST',
                    url:'{{ url("/useraccess/ajax-delete-routes") }}',
                    data:{id:id},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    beforeSend:function(){
                        console.log('processing..');
                    },
                    success:function(data,textstatus,xhr){
                        
                        if(xhr.status==200){
                            $(".msg").html(data.message);
                            $(".msg").addClass('alert alert-success');
                            $(".msg").show();
                        }

                        setTimeout(function(){location.reload()},1000);
                    },
                    error:function(){
                        $(".msg").html("Something went wrong.");
                        $(".msg").addClass('alert alert-danger');
                        $(".msg").show();
                    }
                });

            }

        });

        $(document).on('click','#search-buttton',function(){

            var $_module = $('#module_search').val();

            var $_route  = $('#route_search').val();

            var $_url   =  "{{ url('/useraccess/add-routes') }}?module="+$_module+"&route="+$_route;

            window.location.href = $_url;

        });

    </script>

@endsection