<div class="modal-header">
            
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
<h5 class="modal-title">Edit Route</h5>

</div>

<form name="route-form" id="edit-route-form" action="{{ url('/useraccess/ajax-edit-routes') }}" method="POST">

  <div class="modal-body">

        <div class="edit-flash-message"></div>

        {{ csrf_field() }}

        <div class="form-group">

            <label for="menu_group">Select Module</label>
            
            <input type="hidden" value="{{ $routesData->id }}" name="routedId">

            <select id="edit_menu_group" name="menu_group" class="form-control" required>
                @foreach($module as $key => $value)
                    <option {{ ( $routesData->menu_group == $value->id ) ? 'selected':'' }} value="{{ $value->id }}">{{ $value->name }}</option>
                @endforeach 
            </select>
        </div>

        <div class="form-group">

            <label for="menu_type">Select Type</label>

            <select id="edit_menu_type" name="menu_type" class="form-control">
                <option value="0">Menu</option> 
                <option {{ ($routesData->menu_subgroup != 0) ? 'selected':'' }} value="1">Sub Menu</option>
            </select>

        </div>

        <div class="form-group" id="edit-menu-from-group" {{ ($routesData->menu_subgroup == 0) ? 'style=display:none':'' }}>

            <label for="menu">Select Menu</label>

            <select id="edit-menu" name="menu" class="form-control">

                @foreach($modulemenu as $key => $val)

                    <option {{ ( $selectedMenu == $val->id ) ? 'selected':'' }} value="{{ $val->id }}">{{ $val->name }}</option>

                @endforeach 

            </select>

        </div>

        <div class="form-group">

            <label>Name</label>

            <input type="text" name="name" value="{{ $routesData->name }}" class="form-control" placeholder="Dashboard">

            <p class="text-danger edit_error_name"></p>

        </div>

        <div class="form-group">

            <label>Icon</label>

            <input type="text" name="icon" value="{{ $routesData->icon }}" class="form-control" placeholder="fa fa-dashboard">
            <p class="text-danger error_icon"></p>

        </div>

        <div class="form-group">

            <label>Route</label>

            <input type="text" name="route" value="{{ $routesData->route }}" class="form-control" placeholder="/dashboard">

            <p class="text-danger edit_error_route"></p>

        </div>

        <div class="form-group">

            <label>Controller</label>

            <input type="text" name="controller" value="{{ $routesData->controller }}" class="form-control" placeholder="HomeController@dashboard">

            <p class="text-danger edit_error_controller"></p>

        </div>

        <div class="form-group">

            <label>Method</label>

            <select id="edit-method" name="method" class="form-control">
                <option value="get">Get</option> 
                <option {{ ( $routesData->method == 'post') ? 'selected' : '' }} value="post">Post</option>
            </select>

        </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary">Save changes</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  </div>

</form>