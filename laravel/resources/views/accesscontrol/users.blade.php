@extends('adminlte::layouts.app')

@section('main-content')

    @section('htmlheader_title')
    Users
    @endsection
    @section('contentheader_title')
    Users
    @endsection

    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">

    <div class="user_filter">
    	<button id="add-user-button" class="btn btn-warning"><i class="fa fa-plus"></i> Add User</button>
    </div>

	<div class="msg"></div>

    <div class="table-responsive" style="margin-top:30px" >
    	
        <table class="table table-striped">
    		<thead>
        		<tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
        			<th>Created At</th>
            		<th>Updated At</th>
                    <th>Action</th>
            	</tr>
    		</thead>
    		<tbody>

                @foreach($users as $row)
                    
                    <tr>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email  }}</td>
                        <td>{!! ($row->status==1)? '<p class="text-success">Enable</p>' : '<p class="text-danger">Disable</p>' !!}</td>
                        <td>{{ $row->created_at }}</td>
                        <td>{{ $row->updated_at }}</td>
                        <td>
                            <button type="button" data-value="{{ $row->id }}" class="btn btn-success edit-routes">Edit</button>
                            <button type="button" data-value="{{ $row->id }}" class="btn btn-danger delete-users">Delete</button>
                        </td>
                    </tr>

                @endforeach

    		</tbody>
    	</table>

    </div>

    <?php  $query_arr = array(); ?>

    {{ $users->appends($query_arr)->links() }}

    <div id="addModal" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title">Add User</h5>

          </div>
          <form name="add-user-form" id="add-user-form" action="{{ url('/useraccess/ajax-add-users') }}" method="POST">

            <div class="modal-body">

                <div class="add-flash-message"></div>

                {{ csrf_field() }}

                <div class="form-group">

                    <label>Name</label>

                    <input type="text" name="name" value="" class="form-control" placeholder="Name">
                    <p class="text-danger error_name"></p>

                </div>

                <div class="form-group">

                    <label>Email</label>

                    <input type="email" name="email" value="" class="form-control" placeholder="Email">

                    <p class="text-danger error_email"></p>

                </div>

                <div class="form-group">

                    <label>Password</label>

                    <input type="password" name="password" value="" class="form-control" placeholder="********">

                    <p class="text-danger error_password"></p>

                </div>

                <div class="form-group">

                    <label>Confirm Password</label>

                    <input type="text" name="confirm_password" value="" class="form-control" placeholder="********">

                    <p class="text-danger error_confirm_password"></p>

                </div>

            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

          </form>

        </div>
      </div>
    </div>

    <div id="editModal" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
      </div>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">

        $('#add-user-button').on('click',function(){

            $('#addModal').modal('show');

        });

        $(document).on('submit','#add-user-form',function(e){

            e.preventDefault();
            
            var form = $(this);

            var url = form.attr('action');

            $.ajax({

                type:"POST",
                url:url,
                data:form.serialize(),
                headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                success:function(data,textstatus,xhr){

                    $('.text-danger').empty();
                    
                    if(xhr.status==200 && data.status==200){
                        
                        $('.add-flash-message').html('<div class="alert alert-success"><strong>Success!  </strong>'+data.message+'.</div>');

                        setTimeout(function(){location.reload()},1000);
                    }
                    else if(xhr.status==200 && data.status==400){
                        $('.error_name').text(data.data.name);
                        $('.error_email').text(data.data.email);
                        $('.error_password').text(data.data.password);
                        $('.error_confirm_password').text(data.data.confirm_password);
                    }

                },
                error:function(){
                    $('.add-flash-message').html('<div class="alert alert-danger"><strong>Error! </strong> Something went wrong.</div>');
                }

            });

        })

        $(document).on('click','.edit-routes',function(){

           var Id       = $(this).data('value');
           var subgroup = $(this).data('subgroup');

           $.ajax({
                
                type:"POST",
                url:"{{ url('/useraccess/edit-user') }}",
                data:{id:Id,'subgroup':subgroup},
                headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},
                success:function(data,textstatus,xhr){

                    if(xhr.status==200){

                        $('#editModal .modal-content').html(data);

                        $('#editModal').modal('show');

                    }

                },
                error:function(){

                }

            });

        });

        $(document).on('submit','#edit-user-form',function(e){

            e.preventDefault();
            
            var form = $(this);

            var url = form.attr('action');

            $.ajax({

                type:"POST",
                url:url,
                data:form.serialize(),
                headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                success:function(data,textstatus,xhr){

                    if(xhr.status==200 && data.status==200){

                        $('.edit-flash-message').html('<div class="alert alert-success"><strong>Success!  </strong>'+data.message+'.</div>');

                        setTimeout(function(){location.reload()},1000);

                    }
                    else if(xhr.status==200 && data.status==400){
                        $('.edit_error_name').text(data.data.name);
                        $('.edit_error_email').text(data.data.email);
                        $('.edit_error_password').text(data.data.password);
                        $('.edit_error_confirm_password').text(data.data.confirm_password);
                    }

                },
                error:function(){
                    $('.edit-flash-message').html('<div class="alert alert-danger"><strong>Error! </strong> Something went wrong.</div>');
                }

            });

        })

        $(document).on('click','.delete-users',function(){

            if(confirm('Are you sure to delete this?')){

                var id = $(this).data('value');

                $.ajax({

                    type:'POST',
                    url:'{{ url("/useraccess/ajax-delete-users") }}',
                    data:{id:id},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    success:function(data,textstatus,xhr){
                        
                        if(xhr.status==200){
                            $(".msg").html(data.message);
                            $(".msg").addClass('alert alert-success');
                            $(".msg").show();
                        }

                        setTimeout(function(){location.reload()},1000);

                    },
                    error:function(){
                        $(".msg").html("Something went wrong.");
                        $(".msg").addClass('alert alert-danger');
                        $(".msg").show();
                    }
                });

            }

        });

    </script>

@endsection