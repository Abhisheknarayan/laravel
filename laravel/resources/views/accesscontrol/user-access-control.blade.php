@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
User Access Control
@endsection
@section('contentheader_title')
User Access Control
@endsection

	<link rel="stylesheet" href="{{ asset('/css/jquery-ui.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/tree.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/select2.css') }}">

    <script src="{{ asset('/js/tree.js') }}"></script>
    <script src="{{ asset('/js/select2.js') }}"></script>


    <div class="msg"></div>

    <div class="user_list mb-5">

        <form class="search form-inline" action="{{ url('useraccess/user-access') }}" method="post">

            <input name="_token" value="{{ csrf_token() }}" type="hidden">

            <div class="form-group">
                <label>Users</label>
                <select name="user_list" id="user_list" class="form-control select2">
                    <option value="0">Select Users</option> 
                    @foreach($users  as $key => $value)
                        <option {{ ($selectedUser == $value->id) ? 'selected':'' }} value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach

                </select>
            </div>
            
            <button type="submit" class="btn btn-warning">Select</button>

            <a href="{{ url('useraccess/user-access') }}" class="btn btn-primary">Reset</a>

        </form>
    </div>

	<div>

	    <div class="demo-section k-content">
	        <div>
	            <div id="treeview"></div>
	        </div>
	    </div>

        <div id="result"></div>

        <button id="submit" class="btn btn-success">Submit</button>

	</div>

@endsection

@section('scripts')

<script type="text/javascript">

    $('.select2').select2({
        placeholder: 'Choose account',
        allowClear: true
    });

	$("#treeview").kendoTreeView({
        checkboxes: {
            checkChildren: true
        },

        check: onCheck,

        dataSource: [{!! json_encode($dataSource) !!}]
    });


    // function that gathers IDs of checked nodes
    function checkedNodeIds(nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].id);
            }

            if (nodes[i].hasChildren) {
                checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    }

    // show checked node IDs on datasource change

    function onCheck(){

        var checkedNodes = [],
            treeView = $("#treeview").data("kendoTreeView"),
            message;

        checkedNodeIds(treeView.dataSource.view(), checkedNodes);

        if (checkedNodes.length > 0) {
            message = checkedNodes.join(",");
        } else {
            message = [];
        }

        return message;

    }

    $(document).on('click','#submit',function(){

        var ids         = onCheck();

        var user_id     = $('#user_list').val();

        if(user_id == '' ){
            $(".msg").html("Please select user");
            $(".msg").addClass('alert alert-danger');
            $(".msg").show();

            return false;
        }

        $.ajax({

                type:"POST",
                url:"{{ url('/useraccess/add-user-access') }}",
                data:{'ids':ids,'user_id':user_id},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend:function(){
                    console.log('processing..');
                },
                success:function(data,textstatus,xhr){
                    if(xhr.status == 200){
                        $(".msg").html("Saved Successfully");
                        $(".msg").addClass('alert alert-success')
                        $(".msg").show();
                        setTimeout(function(){ location.reload() },1000);
                    }
                },
                error:function(){
                    $(".msg").html("Something went wrong.");
                    $(".msg").addClass('alert alert-danger')
                    $(".msg").show();
                }
        });

    });

</script>

@endsection