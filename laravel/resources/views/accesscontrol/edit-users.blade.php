<div class="modal-header">     
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>

    <h5 class="modal-title">Edit User</h5>
</div>

<form name="edit-user-form" id="edit-user-form" action="{{ url('/useraccess/ajax-edit-users') }}" method="POST">

    <div class="modal-body">

        <div class="edit-flash-message"></div>

        {{ csrf_field() }}

        <div class="form-group">

            <label>Name</label>

            <input type="hidden" value="{{ $data->id }}" name="id" >

            <input type="text" name="name" value="{{ $data->name }}" class="form-control" placeholder="Name">
            <p class="text-danger edit_error_name"></p>

        </div>

        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control">
                <option value="1">Enable</option>
                <option {{ ($data->status==0) ? 'selected':'' }} value="0">Disable</option>
            </select>
        </div>

        <div class="form-group">

            <label>Email</label>

            <input type="email" name="email" value="{{ $data->email }}" class="form-control" placeholder="Email" readonly>

            <p class="text-danger edit_error_email"></p>

        </div>

        <div class="form-group">

            <label>Password</label>

            <input type="password" name="password" value="" class="form-control" placeholder="********">

            <p class="text-danger edit_error_password"></p>

        </div>

        <div class="form-group">

            <label>Confirm Password</label>

            <input type="text" name="confirm_password" value="" class="form-control" placeholder="********">

            <p class="text-danger edit_error_confirm_password"></p>

        </div>

    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</form>