@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Users
@endsection
@section('contentheader_title')
Users Limit
@endsection

    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">

    <div style="margin:10px 0;">
    	<span class="label label-success">Total: {{ $total_count }} users</span>
    </div>
	
	<form class="search form-inline"  action="{{ url('/followersearch/user-limit') }}" method="post">
		<input name="_token" value="{{ csrf_token() }}" type="hidden">
		
		<div class="form-group">
			<label>Email Id</label>
	      	<input type="text" name="email-id" class="input-medium search-query" value="{{ $email_id }}">	
		</div>
		
      	<button type="submit" class="btn btn-warning" >Search</button>
      	<a href="{{ url('/followersearch/user-limit') }}"  class = "btn btn-primary">Reset</a>
    </form>
    
    <?php if($email_id !=''):  ?>
   		<div style="margin:10px 0">Search Result : {{ $search_count }} user(s) </div>
	<?php endif; ?>	

	<div class="msg"></div>

    <div class="table-responsive" style="margin-top:30px" >
    	<table class="table table-striped">

    		<thead>

        		<tr>
        			<th>Email Id</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th class="action">Action</th>
            	</tr>

    		</thead>

    		<tbody>
                
    			@foreach($userLimitObj as $usr)		
                    
    				<tr>

                        <form class="user-limit-form" method="POST" action="{{ url('followersearch/ajax-save-user-limit') }}">

        				<td rowspan="3" class="email">{{ $usr->email }}</td>

        				<td>
                            <label>Bios Search per day</label>
                            <input type="hidden" name="user_id" value="{{ $usr->user_id }}" class="">
                            <input type="text" name="b_srch_p_d" value="{{ $usr->b_srch_p_d }}" class="" >
                        </td>
        				<td>
                            <label>Result per Search</label>
                            <input type="text" name="b_rs_p_srch" value="{{ $usr->b_rs_p_srch }}" class="">
                        </td>
        				<td>
                            <label>Profile Track&Sort</label>
                            <input type="text" name="pr_trk_srt" value="{{ $usr->pr_trk_srt }}" class="">            
                        </td>
                        <td>
                            <label>Competitor's Profiles Track&Sort</label>
                            <input type="text" name="cmt_trk_srt" value="{{ $usr->cmt_trk_srt }}">
                        </td>
                        <td>
                            <label>Track&Sort Upto</label>
                            <input type="text" name="trk_srt_upto" value="{{ $usr->trk_srt_upto }}">
                        </td>
                        <td>
                            <label>Accounts analysis per month</label>
                            <input id="analyze_prf" type="text" name="analyze_prf" value="{{ $usr->analyze_prf }}">
                        </td>
                        <td>
                            <label>Add twitter pofile</label>
                            <input id="add_tw_prf" type="text" name="add_tw_prf" value="{{ $usr->add_tw_prf }}">
                        </td>

                        <td rowspan="3" class="action">
                            <button type="submit" class="btn btn-success" class="save-button" > <i class="fa fa-save"></i> Save</button>
                        </td>

                    <tr/>
                    <tr>

                        <td>
                            <label>Analyze Upto</label>
                            <input type="text" name="analyze_upto" value="{{ $usr->analyze_upto }}">
                        </td>
                    
                        <td>
                            <label>Comparisons Per Month</label>
                            <input type="text" name="cmpr_pr_d" value="{{ $usr->cmpr_pr_d }}">
                        </td>

                        <td>
                            <label>Comparisons Upto</label>
                            <input type="text" name="cmpr_upto" value="{{ $usr->cmpr_upto }}">
                        </td>

                        <td>
                            <label>Downloads per day</label>
                            <input type="text" name="dwnld_pr_d" value="{{ $usr->dwnld_pr_d }}">
                        </td>

                        <td>
                            <label>Download rows</label>
                            <input type="text" name="dwnld_row" value="{{ $usr->dwnld_row }}">
                        </td>
                        
        				<td>
                            <label>Plan</label>
                            {{  Form::select('plan_id', config('followersearch.pricing'), $usr->plan_id ) }}
                        </td>

                        <td></td>

                        </form>
        			</tr>

    			@endforeach		

    		</tbody>
    	</table>
    </div>
    
<?php 

	$query_arr = array();
	
	if($email_id != ''){
		$query_arr['email-id']= $email_id;
	}
	
?>

{{ $userLimitObj->appends($query_arr)->links() }}

@endsection

@section('scripts')

<script type="text/javascript">

    jQuery(document).ready(function($){

        jQuery(document).on('submit','.user-limit-form',function(e){

            e.preventDefault();

            var token       =   $("meta[name='csrf-token']").attr("content");

            $_data          =   $(this).serialize();

            $_url           =   $(this).attr('action');

            $(".msg").hide();   

            $.ajax({

                    type:"POST", 
                    headers: {'X-CSRF-TOKEN': token},
                    url: $_url,
                    data: $_data,
                    success:function(data,textstatus,xhr)
                    {

                        if(xhr.status == 200){
                            $(".msg").html("Saved Successfully");
                            $(".msg").addClass('alert alert-success')
                            $(".msg").show();
                            }
                        else if(xhr.status == 201){
                            $(".msg").html("Error Occured While Saving");
                            $(".msg").addClass('alert alert-error')
                            $(".msg").show();
                        } 

                    }    

               });                          

             return false;

        });

    });	  

</script>

@endsection