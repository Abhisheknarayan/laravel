@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Profiles
@endsection
@section('contentheader_title')
Compare Profiles Report
@endsection

<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">

<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} reports</span>
</div>
	
	<form class="search form-inline" action="{{ url('followersearch/compare-report') }}" method="post">
		<input name="_token" value="{{ csrf_token() }}" type="hidden">
	
        <div class="form-group">
            <label>Profile</label>
            <input type="text" name="profile" class="input-medium search-query" value="{{ $keyword }}">
        </div>
      	<button type="submit" class="btn btn-warning" >Search</button>
      	<a href="{{ url('/followersearch/compare-report') }}"  class = "btn btn-primary">Reset</a>
    </form>

	<div class="msg"></div>
    <div class="table-responsive" style="margin-top:30px">

    	<table class="table table-striped">
    		
            <thead>
        		<tr>
        			<th>Email Id</th>
            		<th>Profile</th>
                    <th>Type</th>
                    <th>Created at</th>
            		<th>Updated at</th>
            		<th>Action</th>
            	</tr>
    		</thead>

    		<tbody>
                
    			@foreach($profilesObj as $row)		
                    
    				<tr>
        				<td>{{ $users[$row->user]->email }}</td>

        				<td>{{ '@'.implode(', @',$row->t_users) }}</td>

        				<td>{{ ucfirst($row->type)  }}</td>

                        <td>{{ date('d-M-y h:i:m A',strtotime($row->updated_at)) }}</td>

        				<td>{{ date('d-M-y h:i:m A',strtotime($row->created_at)) }}</td>

        				<td>
                            <a target="_blank" href="{{ config('followersearch.link') }}/dashboard/compare/report/{{ $row->job_id }}">
                                <button type="button" class="btn btn-success"> View </button>
                            </a>
                        </td>
        			</tr>

    			@endforeach		

    		</tbody>

    	</table>

    </div>

    <?php 

    	$query_arr = array();
    

        if($keyword!=''){
            $query_arr['t_users'] = $keyword;
        }

    ?>

    {{ $profilesObj->appends($query_arr)->links() }}

@endsection

@section('scripts')
<script type="text/javascript">
   
</script>

@endsection