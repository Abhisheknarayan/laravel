@extends('adminlte::layouts.app')
@section('main-content')
@section('htmlheader_title')
Users
@endsection
@section('contentheader_title')
Users
@endsection
<link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
<div style="margin:10px 0;">
	<span class="label label-success">Total: {{ $total_count }} users</span>
	<span class="label label-danger">Not Active: {{ $not_active_account }} users</span>
</div>

<div class="fs-user-filter">

	<form class="search form-inline" action="{{ url('/followersearch/users') }}" method="post">
		<input name="_token" value="{{ csrf_token() }}" type="hidden">


        <div class="form-group">
            <label>Blocked</label>
            {{ Form::select('is_block', array(''=>'Select','0' => 'Unblocked', '1' => 'Blocked'),$is_block) }}
        </div>

        <div class="form-group">
            <label>Deleted</label>
            {{ Form::select('is_deleted', array(''=>'Select','0' => 'Active', '1' => 'Deleted'),$is_deleted) }}
        </div>

        <div class="form-group">
            <label>User Id</label>
            <input type="text" id="user_filter" name="user_id" class="input-medium search-query" value="{{ $user_id }}">
        </div>

		<div class="form-group">
			<label>Stripe Id</label>
			<input type="text" id="stripe_filter" name="stripe_id" class="input-medium search-query" value="{{ $stripe_id }}">
      	</div>

		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="input-medium search-query" value="{{ $email }}">
      	</div>

      	<button type="submit" class="btn btn-warning" >Search</button>

      	<a href="{{ url('/followersearch/users') }}"  class = "btn btn-primary">Reset</a>
    </form>

</div>
    
    <?php if($user_id != '' || $is_deleted != '' || $is_block != '' || $stripe_id !=''|| $email!=''):  ?>
   		<div style="margin:10px 0">Search Result : {{ $search_count }} user(s) </div>

	<?php endif; ?>	

	<div class="msg"></div>
    <div class="table-responsive" style="margin-top:30px" >
    	<table class="table table-striped">
    		<thead>
        		<tr>
                    <th>Id              </th>
            		<th>Email Id        </th>
                    <th>Stripe Id       </th>
                    <th>Last login      </th>
                    <th>Login Count     </th>
                    <th>IP address      </th>
            		<th>Added Date      </th>
            		<th>Verify email    </th>
                    <th>Is Delete       </th>
                    <th>Is Block        </th>
            	</tr>
    		</thead>
    		<tbody>
                
    			@foreach($users as $usr)		
                    
    				<tr>
        				<td>{{ $usr->id }}</td>
        				<td>{{ $usr->email }}</td>
                        <td>{{ $usr->stripe_id }}</td>
                        <td>{{ $usr->last_login }}</td>
                        <td>{{ $usr->login_counter }}</td>
                        <td>{{ $usr->ip_address }}</td>
        				<td>{{ date('d-M-y h:i:m A',strtotime($usr->created_at)) }}</td>
                        <td>

                            <select name="verify_email" id="verifyEmail_{{ $usr->id }}" class="user-status">
                                <option value="1">Verified</option>
                                <option {{ (is_null($usr->email_verified_at)) ? 'selected':''  }} value="0">Unverified</option>
                            </select>
                            
                        </td>
                        <td>{{ Form::select('status', array('0' => 'Active', '1' => 'Deleted'), $usr->is_deleted, array('id'=>'isdeleted_'.$usr->id,'class'=>'user-status')) }}</td>
        				<td>{{ Form::select('status', array('0' => 'Unblocked', '1' => 'Blocked'), $usr->is_block, array('id'=>'isblock_'.$usr->id,'class'=>'user-status')) }}</td>
                        
        			</tr>

    			@endforeach		

    		</tbody>
    	</table>
    </div>
<?php 

	$query_arr = array();

    if($user_id != ''){
        $query_arr['user_id']   = $user_id;
    }

	if($is_block != ''){
		$query_arr['is_block'] 	= $is_block;
	}

    if($is_deleted != ''){
        $query_arr['is_deleted']  = $is_deleted;
    }

	if($stripe_id != ''){
		$query_arr['stripe_id']= $stripe_id;
	}
	
	if($email!=''){
	 	$query_arr['email'] = $email;
	}

?>

{{ $users->appends($query_arr)->links() }}
@endsection

@section('scripts')

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".user-status").on('change',function(){
    	var token		= 	$("meta[name='csrf-token']").attr("content");    			
    	var id_str		=	this.id.split('_');
		var id 			=	id_str[1];

        var is_verified  =   $('#verifyEmail_'+id).val(); 
        var is_deleted   =   $('#isdeleted_'+id).val(); 
        var is_block     =   $('#isblock_'+id).val(); 


    	$(".msg").hide();	
  	    $.ajax({
    	    	type:"POST", 
    	    	headers: {'X-CSRF-TOKEN': token},
    	    	url:"/followersearch/ajax/userssave",
    	    	data: {id:id,is_verified:is_verified,is_deleted:is_deleted,is_block:is_block},
    	    	success:function(data,textstatus,xhr)
    	    	{ 
    	        	if(xhr.status == 200){
    	   		    	$(".msg").html("Saved Successfully");
    	     		    $(".msg").addClass('alert alert-success')
    	     		    $(".msg").show();
    	     		    }
    	        	else if(xhr.status == 201){
    	  		    	$(".msg").html("Error Occured While Saving");
    	  		        $(".msg").addClass('alert alert-error')
    	  		        $(".msg").show();
    	  		   }     		                
    	        }    		     
  		   });    	    			   	

  	     return false;
      });
});	    
</script>

@endsection