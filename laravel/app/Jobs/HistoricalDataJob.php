<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Log,Config;

use App\Helpers\Twitter;
use League\Csv\CannotInsertRecord;
use League\Csv\Writer;

use App\Modal\Historical\HistoricalData;
use App\Modal\Historical\Historical_collections;

use Carbon\Carbon;

class HistoricalDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $keyword;
    private $tweet_count;
    private $uniqueId;

    public function __construct($keyword,$tweet_count,$uniqueId)
    {
        $this->keyword      = $keyword;
        $this->tweet_count  = $tweet_count;
        $this->uniqueId     = $uniqueId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $requestData    =   HistoricalData::find($this->uniqueId);

        $requestTime    =   strtotime($requestData->created_at);

        $collection     =   $requestTime.'_'.$this->uniqueId;

        $twitterObj     =   new Twitter();

        try{
            $connection = $twitterObj->_create_connection();
        }
        catch (\Exception $e){
            return array();
        }
        
        
        $LANG_CODE  =   Config::get('tweetkitapp.lang');
       
        $filename   =   $collection.'.csv';

        $filepath   =   config('tweetkitapp.historical_data').$filename;
        
        $oldmask    =   umask(0);
        $fp = fopen($filepath, 'w');
        fclose($fp);
        chmod($filepath,0777);
        umask($oldmask);

        self::_fetch_search_tweets($connection,$this->keyword,$LANG_CODE,$this->tweet_count,'',$filepath);

        self::create_csv($collection,$filepath);

        $this->complete($collection);

    }

    private function _fetch_search_tweets($connection,$search_keyword,$LANG_CODE,$limit,$max_id,$filepath){
        
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', -1);
        
        $limitExceed =0;

        $prm = '/search/tweets';

        $requestData    =   HistoricalData::find($this->uniqueId);

        $requestTime    =   strtotime($requestData->created_at);

        $collection     =   $requestTime.'_'.$this->uniqueId;

        $historicalDb   = new Historical_collections($collection);
        
        while(1){
            
            try{

                $connection->setTimeouts(120,120);

                $r              =   $connection->get('application/rate_limit_status');

                if(empty($r->errors) || empty($r->error)){

                    if(isset($r->resources->search->{$prm}->remaining) && $r->resources->search->{$prm}->remaining > 0 ){

                        $remaining            = $r->resources->search->{$prm}->remaining;

                        $params['q']          = urlencode($search_keyword);

                        $params['count']      = 100;

                        $params['tweet_mode'] = 'extended';

                        for($i=1;$i<=$remaining;$i++){

                            if($max_id != ''){
                                $params['max_id'] = $max_id;
                            }

                            $search_tweets  =   $connection->get($prm,$params);

                            if(!isset($search_tweets->errors)){
                                
                                $search_feeds=$search_tweets->statuses;
                                
                                if(sizeof($search_feeds)>0 && bcsub($search_feeds[0]->id_str, $max_id)==0){
                                    array_shift($search_feeds);
                                }
                                
                                $feed_size  = sizeof($search_feeds);
                                
                                $limitExceed += $feed_size;

                                if($limitExceed > $limit){
                                    break 2;
                                }

                                if($feed_size > 0){

                                    foreach($search_feeds as $tweet){

                                        if(isset($tweet->id_str) && $tweet->id_str  != '' && isset($tweet->user->id_str) && isset($tweet->user->screen_name)){
                        
                                            $insertData         =   (array) $tweet;

                                            $insertData['_id']  =   $tweet->id;

                                            $max_id             =   $tweet->id_str; 

                                            try{

                                                unset($insertData['id']);
                                                
                                                $historicalDb->insert($insertData);

                                            }
                                            catch(\Exception $e){   

                                                Log::info($e->getMessage());

                                            }

                                        }

                                    }
                                    
                                }
                                else{

                                    break 2;

                                }

                            }
                            else{

                                break;

                            }

                        }

                    }
                    else{

                        $connection->setTimeouts(120,120);

                        $r              =   $connection->get('application/rate_limit_status');

                        Log::error(serialize($r));

                        if(isset($r->resources->search->{$prm}->reset)){

                            $curr           =   strtotime(date('Y-m-d H:i:s'));

                            $remain         =   strtotime(date('Y-m-d H:i:s',$r->resources->search->{$prm}->reset));

                            $sleep          = ($remain-$curr)+10;

                            Log::info(__LINE__.' Server on sleep '.$sleep);

                            sleep($sleep);

                        }
                        else{

                            Log::info(__LINE__.' server on sleep');

                            sleep(900);

                        }

                    }

                }
                else{

                    Log::error(serialize($r));

                    Log::info(__LINE__.' server on sleep');

                    sleep(900);

                }

            }
            catch(\Exception $e){

                Log::info(__LINE__.$e->getTraceAsString());

                throw new Exception;

            } 

        }
         
    }

    private function create_csv($collection,$filepath){

        try{

            $historicalDb = new Historical_collections($collection);

            $start  = 0;

            $csv    = Writer::createFromPath($filepath);

            $x      = 0;

            do{

                $data = $historicalDb->skip($x*5000)->take(5000)->get();

                if(sizeof($data) <= 0){
                    break;
                }

                foreach($data as $key => $tweet){

                    /* create csv */

                    $csv_array = array();

                    // tweet id

                    $csv_array['tweet_id']  =   '"'.$tweet->id_str.'"';

                    // tweet url
                    
                    $csv_array['url']       =   'https://twitter.com/'.$tweet->user['screen_name'].'/status/'.$tweet->id_str;
                    
                    // tweet posted time


                    if(isset($tweet->created_at))
                        $csv_array['tcreated_at']   =   date('d M Y H:i:s',strtotime($tweet->created_at));
                    else
                        $csv_array['tcreated_at']   =   '';


                    // tweet content
                    
                    if(isset($tweet->full_text))
                        $csv_array['text']  =   '" '.preg_replace('/\\\\/', '', htmlspecialchars(stripslashes($tweet->full_text))).' "';
                    else
                        $csv_array['text']  =   '';

                    // tweet type
                                    
                    if(isset($tweet->retweeted_status))
                        $csv_array['is_retweet']    =   "ReTweet";
                    else
                        $csv_array['is_retweet']    =   "Tweet";

                    // client
                            
                    if(isset($tweet->source)){
                        $tweet->source = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $tweet->source);
                        $csv_array['source']    =   '"'.$tweet->source.'"';
                    }else{
                        $csv_array['source']    =   '';
                    }

                    // retweets received

                    if(isset($tweet->retweet_count))
                        $csv_array['retweet_count'] =   $tweet->retweet_count;
                    else
                        $csv_array['retweet_count'] =   0;  

                    // likes received   

                    if(isset($tweet->favorite_count))
                        $csv_array['favorite_count']    =   $tweet->favorite_count;
                    else
                        $csv_array['favorite_count']    =   0;

                    // tweet location   

                    if(isset($tweet->user['location']) && $tweet->user['location'] != ''){
                        $csv_array['location']  =   '"'.$tweet->user['location'].'"';
                        //$csv_array['location_lat_lng']    =   '';
                    }
                    else{
                        $csv_array['location']          =   '';
                        //$csv_array['location_lat_lng']    =   '';
                    }   

                    // tweet language

                    if(isset($tweet->lang) && $tweet->lang != 'und'){
                        if(isset($LANG_CODE[$tweet->lang])){
                            $tweet->lang = $LANG_CODE[$tweet->lang];
                            $csv_array['lang']  =   $tweet->lang;
                        }else{
                            $csv_array['lang']  =   '';
                        }
                    }else if(isset($tweet->metadata->iso_language_code) && $tweet->metadata->iso_language_code != 'und'){
                        if(isset($LANG_CODE[$tweet->metadata->iso_language_code])){
                            $tweet->lang = $LANG_CODE[$tweet->metadata->iso_language_code];
                            $csv_array['lang']  =   $tweet->lang;
                        }else{
                            $csv_array['lang']  =   '';
                        }
                    }else{
                        $csv_array['lang']      =   '';
                    }

                    // user id

                    $csv_array['user_id']   =   '"'.$tweet->user['id_str'].'"';   

                    // name 

                    $csv_array['name']      =   preg_replace('/\\\\/', '', htmlspecialchars(stripslashes($tweet->user['name'])));

                    // username

                    $csv_array['username']  =   $tweet->user['screen_name'];

                    // user bio @todo

                    if(isset($tweet->user['description']))
                        $csv_array['user_bio']      =   '"'.preg_replace('/\\\\/', '', htmlspecialchars(stripslashes($tweet->user['description']))).'"';
                    else
                        $csv_array['user_bio']      =   '';

                    // verified or non-verified

                    if(isset($tweet->user['verified'])){
                        if($tweet->user['verified'])
                            $csv_array['verified']      =   "Verified";
                        else
                            $csv_array['verified']      =   "Non-Verified";
                    }else{
                        $csv_array['verified']          =   'Non-Verified';
                    }

                    // profile url 

                    $csv_array['profile_url']           = 'https://twitter.com/'.$tweet->user['screen_name'];

                    // user followers

                    if(isset($tweet->user['followers_count']))
                        $csv_array['followers_count']   =   $tweet->user['followers_count'];
                    else
                        $csv_array['followers_count']   =   0;

                    // user following

                    if(isset($tweet->user['friends_count']))
                        $csv_array['friends_count']     =   $tweet->user['friends_count'];
                    else
                        $csv_array['friends_count']     =   0;

                    // user account creation date

                    if(isset($tweet->user['created_at']))
                        $csv_array['created_at']        =   date('d M Y H:i:s',strtotime($tweet->user['created_at']));
                    else
                        $csv_array['created_at']        =   '';

                    // insert csv

                    if($start == 0){

                        $csv->insertOne(['Tweet Id','Tweet URL','Tweet Posted Time','Tweet Content','Tweet Type','Client','Retweets Received','Likes Received','Tweet Location','Tweet Language','User Id','Name','Username','User Bio','Verified or Non-Verified','Profile URL','User Followers','User Following','User Account Creation Date']);
                    }
                    else{

                            try {

                                $csv->insertOne($csv_array); // create csv

                            } 
                            catch(CannotInsertRecord $e){

                                Log::info($e->getRecords());

                            }

                    }

                    /* end create csv */

                    $start++;
                }

                $x++;

            }
            while(true);

        }
        catch(\Exception $e){

            Log::info($e->getTraceAsString());
            
        }

    }

    public function complete($collection){

        Log::info('complete');

        $historicalDb   = new Historical_collections($collection);

        $started_date   = $historicalDb->orderBy('created_at','asc')->first();
        $end_date       = $historicalDb->orderBy('created_at','desc')->first();

        $historicalData                 = HistoricalData::find($this->uniqueId);
        $historicalData->status         = 1;
        $historicalData->started_date   = date('Y-m-d H:i:s',strtotime($started_date->created_at));
        $historicalData->end_date       = date('Y-m-d H:i:s',strtotime($end_date->created_at));
        $historicalData->save();

    }

    public function failed(){

        Log::info('failed');

        $historicalData                 = HistoricalData::find($this->uniqueId);
        $historicalData->status         = 2;
        $historicalData->save();

    }

}
