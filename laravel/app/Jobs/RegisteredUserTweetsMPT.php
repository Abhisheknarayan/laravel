<?php

namespace App\Jobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\Twitter;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Modal\Core\UserTweetsQueue;

class RegisteredUserTweetsMPT extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels, DispatchesJobs;

	protected $user;
	protected $target_twitter_id;
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	
	public function __construct($user,$target_twitter_id){
		$this->user					=	$user;
		$this->target_twitter_id	=	$target_twitter_id;
	}
	
	public function handle(){
		try {
			$twitterObj  	=	new Twitter();
			$twitterObj->track_registered_user_tweets($this->user,$this->target_twitter_id,1);
			$usertweetsqueue = UserTweetsQueue::find($this->target_twitter_id);
			if(!is_null($usertweetsqueue)){
				$usertweetsqueue->delete();
			}
		}catch (\Exception $e){
			if($this->attempts() < 3 ){
				$this->release(30*60);  //10 min
			}else{
//				throw new \Exception();
			}
		}
	}
}