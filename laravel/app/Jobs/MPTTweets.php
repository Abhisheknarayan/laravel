<?php

namespace App\Jobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Modal\MPT\MobileUsers;
use League\Csv\Writer;
use App\Modal\Core\TwitterUsers;
use App\Modal\MPT\Reports;

class MPTTweets extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels, DispatchesJobs;

	protected $twitter_id;
	protected $target_twitter_id;
	protected $report_id;
	
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	
	public function __construct($twitter_id,$target_twitter_id,$report_id){
        $this->twitter_id           =   $twitter_id;
        $this->target_twitter_id    =   $target_twitter_id;
        $this->report_id            =   $report_id;
	  
	}
	
	private function _create_user_file($user_tweets,$file_path){
	    $tweets_csv 		   =   Writer::createFromPath($file_path,'w');
	    $tweets_csv->insertOne(array('Tweet Id','Tweet URL','Tweet Posted Time','Tweet Content','Tweet Type','Client','Retweets received','Likes received','User Id','Name','Username','Verified or Non-Verified','Profile URL','Protected or Not Protected'));
	    
	    foreach ($user_tweets as $tweet){
	        $user		      =	  TwitterUsers::find($tweet['user_id']);
	        
	        if($user == null) continue;
	        
	        $tweet_type         =   'Tweet';
	        if($tweet['is_retweet'] == 'Yes'){
	            $tweet_type     =   "Retweet";
	        }
	        else if($tweet['is_reply']   == 'Yes'){
	            $tweet_type     =   'Reply';
	        }
	        
	        $tweets_csv->insertOne(array(
	            '"'.$tweet['_id'].'"',
	            'https://twitter.com/'.$user->screen_name.'/status/'.$tweet['_id'],
	            $tweet['tweet_time'],
	            '"'.$tweet['tweet_text'].'"',
	            $tweet_type,
	            $tweet['tweet_source'],
	            $tweet['tweet_retweet_count'],
	            $tweet['tweet_favorite_count'],
	            '"'.$tweet['user_id'].'"',
	            '"'.$user->name.'"',
	            $user->screen_name,
	            ($user->verified) ? "Verified":"Non-Verified",
	            "https://twitter.com/".$user->screen_name,
	            ($user->protected)?"Protected":"Not Protected"
	        ));
	    }
	}
	
	public function handle(){
		try {
            $tweet_found            =   MobileUsers::where('twitter_id',$this->twitter_id)->count();
            if($tweet_found > 0){
                $reportObj          =   Reports::find($this->report_id);
                $files              =   unserialize(tweetkit_file_download_decrypt($reportObj->file_path));
                
                $reportObj->status  =   1;
                $reportObj->save();
                
                
                $user_tweets	    =   \DB::connection('mongodbut')->collection($this->target_twitter_id)->orderBy('tweet_time','DESC')->limit(100)->get();
                $this->_create_user_file($user_tweets, $files['tweets']);
                
                $user_tweets		=	\DB::connection('mongodbut')->collection($this->target_twitter_id)->where('is_retweet',"No")->orderBy('tweet_retweet_count','DESC')->limit(100)->get();
                $this->_create_user_file($user_tweets, $files['retweets']);
                
                $user_tweets		=	\DB::connection('mongodbut')->collection($this->target_twitter_id)->where('is_retweet',"No")->orderBy('tweet_favorite_count','DESC')->limit(100)->get();
                $this->_create_user_file($user_tweets, $files['likes']);
                
                $reportObj              =   Reports::find($this->report_id);
                $reportObj->status      =   2;
                $reportObj->progress    =   100;
                $reportObj->save();
            }
		}catch (\Exception $e){
			$reportObj               =   Reports::find($this->report_id);
			$reportObj->status       =   3;
			$reportObj->save();
		
		    if($this->attempts() < 3 ){
				$this->release(30*60);  //10 min
			}else{
				throw new \Exception();
			}
		}
	}
}