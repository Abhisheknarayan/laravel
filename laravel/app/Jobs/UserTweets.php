<?php
namespace App\Jobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\Twitter;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Modal\Core\UserTweetsQueue;

class UserTweets extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels, DispatchesJobs;

	protected $user;
	protected $connection_count;
	
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	
	public function __construct($user,$nbr){
		$this->user					=	$user;
		$this->connection_count		=	$nbr;
	}
	
	public function handle(){
		try {
			$twitterObj  	=	new Twitter();
			$twitterObj->track_user_tweets($this->user,$this->connection_count);
			
			$usertweetsqueue = UserTweetsQueue::find($this->user->twitter_id);
			if(!is_null($usertweetsqueue)){
				$usertweetsqueue->delete();
			}
		}catch (\Exception $e){
			if($this->attempts() < 3 ){
				$this->release(30*60);  //10 min
			}else{
				throw new \Exception();
			}
		}
	}
}