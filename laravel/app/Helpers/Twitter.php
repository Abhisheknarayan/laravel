<?php
namespace App\Helpers;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Modal\Core\TargetAccounts;
use App\Modal\Core\AccessToken;
use App\Modal\Core\TwitterUsers;
use DB;
class Twitter {
	
	function __construct(){}
	
	public function validate_token($connection){
		try{
			$rate_limit = $connection->get('search/tweets',array('q'=>urlencode('#modi'),'count'=>1));
			$valid = 1;
			if(isset($rate_limit->errors) || isset($rate_limit->error)){
			    if(isset($rate_limit->errors) && is_array($rate_limit->errors)){
			        foreach ($rate_limit->errors as $err){
			            if($err->code == 215 || $err->code = 220){
			                $valid  = 0;
			            }
			        }
			    }
			}
			return $valid;
		}
		catch (\Exception $e){
			return 0;
		}
	}
	
	
	public function geo_location($connection,$location){
	    try{
	        $rate_limit = $connection->get('geo/search',array('query'=>$location));
	        print_r($rate_limit);
	    }
	    catch (\Exception $e){
	        return 0;
	    }
	}
	
	public function _create_connection($user=null,$mpt=0){
		
		if(isset($user->oauth_token) && isset($user->oauth_token_secret)){
		    if($mpt){
		        $connection 		= 	new TwitterOAuth(config('services.twitter_mpt.client_id'),config('services.twitter_mpt.client_secret'),$user->oauth_token,$user->oauth_token_secret);
		    }else{
		        $connection 		= 	new TwitterOAuth(config('services.twitter.client_id'),config('services.twitter.client_secret'),$user->oauth_token,$user->oauth_token_secret);
		    }
		}
		else{
			
			$total_access_token  	=	AccessToken::where('is_active',1)->where('app_status',1)->count() - 1;

			$random_value 			= 	rand(0,$total_access_token);

			$token 					= 	AccessToken::where('is_active',1)->where('app_status',1)->skip($random_value)->take(1)->first();
			
			$connection 			= 	new TwitterOAuth($token->api_key, $token->api_secret,$token->access_token,$token->access_token_secret);
		}

		return $connection;
	}
	
	public function get_user_details($twitter_username,$twitter_id=	'',$user='',$mpt=0){
	    $user_arr = array();
		if($twitter_id != '' ){
			$user_arr['user_id']	    =	$twitter_id;
		}else{
			$user_arr['screen_name']	=	$twitter_username;
		}

		$connection					=	$this->_create_connection($user,$mpt);
		$connection->setTimeouts(120,120);
		try{
			$userinfo	=	$connection->get('users/show',$user_arr);
			if(isset($userinfo->errors)){
				if(isset($userinfo->errors[0]->code) && $userinfo->errors[0]->code == 17){
					return array('userinfo'=>array(),'protected'=>false,'message'=>'User not found.');
				}else{
					return array('userinfo'=>array(),'protected'=>false,'message'=>'An error occurred. Please try again.');
				}
			}
			else{
				if(!isset($userinfo->status))
					$protected = true;
				else
					$protected = false;
				$result = array('userinfo'=>$userinfo,'protected'=>$protected,'message'=>'');
				return $result;
			}
		}catch (\Exception $e){
			return array('userinfo'=>array(),'protected'=>'','message'=>'An error occurred. Please try again.');
		}
	}
	
	public function user_search($query,$user,$mpt=0){
	    $connection				=	$this->_create_connection($user,$mpt);
	    $connection->setTimeouts(120,120);
	    
	    $search_result	=	$connection->get('users/search',array('q'=>$query,'count'=>20,'include_entities'=>false));
	    
	    if(isset($search_result->errors) || isset($search_result->error))
	    {
	        return array();
	    }
	    else{
	        return $search_result;
	    }
	}

	/** CHECK RATE LIMIT **/
	public function checkrate_limit($connection,$context=''){
		try{
			$resources=implode(',',$context);
			$rate_limit = $connection->get('application/rate_limit_status',array('resources'=>$resources));
			if(isset($rate_limit->errors)){
				return array();
			}else{
				return $rate_limit;
			}
		}
		catch (\Exception $e){
			return array();
		}
	}
	/** END RATE LIMIT **/
	
	public function track_registered_user_tweets($user,$target_twitter_id,$mpt=0){
		$connection				=	$this->_create_connection($user,$mpt);
		$targetObj				=	TargetAccounts::find($target_twitter_id);
		if(is_null($targetObj)){
		    $last_tweet_id		=	'';
			$targetObj			=	new TargetAccounts();
			$targetObj->_id		=	$target_twitter_id;
		}else{
			$last_tweet_id		=	$targetObj->last_tweet_id;	
		}
		$last_id 				= 	$this->download_3200_tweets_is_exists($connection,$target_twitter_id,'','',$last_tweet_id,true);
		
		if($last_id != ''){
			$targetObj->last_tweet_id	=	$last_id;
			$targetObj->save();
		}
	}
	
	public function download_200_user_tweets($user,$target_twitter_id,$count=30,$mpt=0){
		$connection		=	$this->_create_connection($user,$mpt);
		$tweets_data	=	$this->download_200_tweets($connection, $target_twitter_id,$count);
		return $tweets_data;
	}
	
	private function download_200_tweets($connection,$user_id,$count){
	    $params                         =   array();
		$params['count'] 				= 	$count;
		$params['exclude_replies']		=	'false';
		$params['include_rts']			=	'true';
		$params['user_id']				=	$user_id;
		$params['contributor_details']	=	'false';
		$params['tweet_mode']         	=   'extended';
			
		try{
			$connection->setTimeouts(120,120);
			$userfeeds	=	$connection->get('statuses/user_timeline',$params);
			
			if(!isset($userfeeds->errors) && !isset($userfeeds->error)){
				if(sizeof($userfeeds) > 0){
					$user_tweets	=	array();
					foreach ($userfeeds as $tweet){
						if($tweet->id_str != '' && $tweet->user->screen_name != '' &&  $tweet->full_text != ''){
							if(isset($tweet->retweeted_status)){
								$temparray	=	$this->save_tweets($tweet->retweeted_status,"Yes");
							}else{
								$temparray	=	$this->save_tweets($tweet, "No");
							}
							$user_tweets[]	=	$temparray;
						}
					}
					return $user_tweets;
				}else{
					return array();
				}
			}
			else{
				return array();
			}
		}catch(\Exception $e){
			return	array();
		}
	}
	
	//track user tweets command
	public function track_user_tweets($user,$connection_count){
	    $token 					= 	AccessToken::where('is_active',1)->where('app_status',1)->skip($connection_count)->take(1)->first();
	    $connection 			= 	new TwitterOAuth($token->api_key, $token->api_secret,$token->access_token,$token->access_token_secret);
        $this->download_3200_tweets_is_exists($connection,$user->twitter_id,'','','',true);
	}
	
	// TrackUserTweets
	private function download_3200_tweets_is_exists($connection,$user_id,$last_id,$first_id,$since_id,$loop){
	    $params                         =   array();   
		$params['count'] 				= 	200;
		$params['exclude_replies']		=	'false';
		$params['include_rts']			=	'true';
		$params['user_id']				=	$user_id;
		$params['contributor_details']	=	'false';
		$params['tweet_mode']         	=   'extended';
		if(!empty($last_id) || $last_id != 0)
			$params['max_id']	=	$last_id;
		try{
			$connection->setTimeouts(120,120);
			$userfeeds	=	$connection->get('statuses/user_timeline',$params);

			if(!isset($userfeeds->errors) && !isset($userfeeds->error)){
				if(sizeof($userfeeds)>0 && bcsub($userfeeds[0]->id_str, $last_id)==0){
					array_shift($userfeeds);
				}
				if(sizeof($userfeeds) > 0){
					foreach ($userfeeds as $tweet){
						if($tweet->id_str != '' && $tweet->user->screen_name != '' &&  $tweet->full_text != ''){
							
							if(isset($tweet->retweeted_status)){
								$tweet_id	=	(string)$tweet->retweeted_status->id_str;
								
								if($tweet_id == $since_id){
									$loop = false;
									break;
								}
								$temparray	=	$this->save_tweets($tweet->retweeted_status,"Yes");	
							}else{
								$tweet_id = (string)$tweet->id_str;
								if($tweet_id == $since_id){
									$loop = false;
									break;
								}
								$temparray	=	$this->save_tweets($tweet, "No");
							}
							try {
								\DB::connection('mongodbut')->collection($user_id)->insert($temparray);
							}catch(\Exception $e){}
													
							if($last_id == '')
								$first_id  								=	$tweet->id_str;
							$last_id									=	$tweet->id_str;
						}
					}
					if($loop){
						return self::download_3200_tweets_is_exists($connection, $user_id, $last_id,$first_id,$since_id,$loop);
					}else{
						return $first_id;
					}
				}else{
					return $first_id;
				}
			}
			else{
				if(isset($userfeeds->errors[0]->code) && $userfeeds->errors[0]->code == 88){
					if($loop){
						sleep(1321);
						return self::download_3200_tweets_is_exists($connection, $user_id,$last_id,$first_id,$since_id,$last_id);
					}else{
						return $first_id;
					}
				}else{
					return $first_id;
				}
			}
		}catch(\Exception $e){
			return $first_id;
		}
	}
	
	private function save_tweets($tweet,$is_retweet){
		$temparray									=	array();
		
		$temparray['_id']							=	(string)$tweet->id_str;
		
		$temparray['is_retweet']					=	$is_retweet;

		if(isset($tweet->in_reply_to_status_id_str) && !empty($tweet->in_reply_to_status_id_str)){
			$temparray['is_reply']					=	"Yes";
		}
		else{
			$temparray['is_reply']					=	"No";
		}
		
		$timestamp									=	strtotime($tweet->created_at);
		
		$temparray['tweet_time']					=	date('Y-m-d H:i:s',$timestamp);
		$temparray['tweet_text']					=	$tweet->full_text;
		
		if($tweet->source != '')
			$temparray['tweet_source'] 				= 	preg_replace('#<a.*?>(.*?)</a>#i', '\1', $tweet->source);
		else
			$temparray['tweet_source']				=	' ';
				
		if(isset($tweet->retweet_count))
			$temparray['tweet_retweet_count']		=	$tweet->retweet_count;
		else
			$temparray['tweet_retweet_count']		=	0;
					
		if(isset($tweet->favorite_count))
			$temparray['tweet_favorite_count']		=	$tweet->favorite_count;
		else
			$temparray['tweet_favorite_count']		=	0;
		
		if(isset($tweet->extended_entities->media) && sizeof($tweet->extended_entities->media) > 0){
			$temparray['media']						=	$tweet->extended_entities->media;
		}else if(isset($tweet->entities->media) && sizeof($tweet->entities->media) > 0){
			$temparray['media']						=	$tweet->extended->media;
		}
								
		if(isset($tweet->entities->hashtags) && sizeof($tweet->entities->hashtags) > 0){
			$temparray['hashtags']					=	$tweet->entities->hashtags;
		}
								
		if(isset($tweet->entities->urls) && sizeof($tweet->entities->urls)){
			$temparray['urls']						=	$tweet->entities->urls;
		}
		$temparray['user_id']						=	(string)$tweet->user->id_str;
		try {
			
			$user_details								=	TwitterUsers::find($tweet->user->id_str);
			
			if($user_details == null){
				$user_details							=	new TwitterUsers();
				$user_details->_id						=	$temparray['user_id'];	
			}
			
			if(isset($tweet->user->screen_name))
				$user_details->screen_name 				=	strtolower($tweet->user->screen_name);
			else 
				$user_details->screen_name				=	'';
			
			if(isset($tweet->user->name))
				$user_details->name 					=	$tweet->user->name;
			else
				$user_details->name						=	$tweet->user->screen_name;
			
			if(isset($tweet->user->description))
				$user_details->description 				= 	$tweet->user->description;
			else
				$user_details->description 				= 	'';
			
			if(isset($tweet->user->location))
				$user_details->location					=	$tweet->user->location;
			else
				$user_details->location					=	'';
			
			if(isset($tweet->user->time_zone))
				$user_details->timezone					=	$tweet->user->time_zone;
			else
				$user_details->timezone					=	'';
				
			if(isset($tweet->user->created_at))
				$user_details->created_at				=	date('Y-m-d H:i:s',strtotime($tweet->user->created_at));
			else
				$user_details->created_at				=	'';
			
			if(isset($tweet->user->followers_count))
				$user_details->followers_count			=	$tweet->user->followers_count;
			else
				$user_details->followers_count			=	0;
			
			if(isset($tweet->user->friends_count))
				$user_details->friends_count			=	$tweet->user->friends_count;
			else
				$user_details->friends_count			=	0;
		
			if(isset($tweet->user->statuses_count))
				$user_details->statuses_count			=	$tweet->user->statuses_count;
			else
				$user_details->statuses_count			=	0;
			if(isset($tweet->user->verified))
				$user_details->verified					=	$tweet->user->verified;
			else
				$user_details->verified					=	0;
				
			if(isset($tweet->user->profile_image_url_https))
				$user_details->profile_image_url_https 	=	str_replace('_normal','_bigger', $tweet->user->profile_image_url_https);
			else
				$user_details->profile_image_url_https 	= 	'';
		
			if(isset($tweet->user->profile_banner_url))
				$user_details->profile_banner_url 		=	$tweet->user->profile_banner_url;
			else
				$user_details->profile_banner_url 		= 	'';
																						
			if(isset($tweet->user->listed_count))
				$user_details->listed_count				=	$tweet->user->listed_count;
			else
				$user_details->listed_count				=	0;
			
			if(isset($tweet->user->favourites_count))
				$user_details->favorites_count			=	$tweet->user->favourites_count;
			else
				$user_details->favorites_count			=	0;
			
			if(isset($tweet->user->lang))
				$user_details->lang						=	$tweet->user->lang;
			else
				$user_details->lang						=	'';

			if(isset($tweet->user->protected))
				$user_details->protected				=	$tweet->user->protected;
			else
				$user_details->protected				=	false;
			
			if(isset($tweet->user->url))
				$user_details->url						=	$tweet->user->url;
			else
				$user_details->url						=	'';
						
			$user_details->save();
		}catch (\Exception $e){}
		return $temparray;
	}
	
	private function update_tweet($tweet,$temparray){
		if(isset($tweet->retweet_count))
			$temparray['tweet_retweet_count']	=	$tweet->retweet_count;
		else
			$temparray['tweet_retweet_count']	=	0;
						
		if(isset($tweet->favorite_count))
			$temparray['tweet_favorite_count']	=	$tweet->favorite_count;
		else
			$temparray['tweet_favorite_count']	=	0;
								
		return $temparray;
	}
	
}	
?>