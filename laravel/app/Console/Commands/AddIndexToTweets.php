<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

use MongoDB\Client;
use MongoDB\Database;
use MongoDB\Collection;
use App\Modal\Core\TargetAccounts;
/**
 * @todo
 *
 */
class AddIndexToTweets extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:index';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Index to User Tweets Collections';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    public function handle(){
        $mongodclient        =   new Client('mongodb://tweetkiturapi_ut:L77s%40K%4011312%40SL4%25%5E3S%40%3Fdsd@'.config('tweetkitapp.mongo_ip').'/',array('authSource'=>'tweetkit_ut'));
        $manager             =   $mongodclient->getManager();
    //  $databaseObj         =   new Database($manager, 'tweetkit_ut');
    //  $collections         =   $databaseObj->listCollections();
        
        $accounts            =   TargetAccounts::where('is_index',0)->take(100)->get();
//      foreach ($collections as $col){
        foreach ($accounts as $account){
//          $collection      =  new Collection($manager, 'tweetkit_ut', $col->getName());
  
            $collection      =  new Collection($manager, 'tweetkit_ut', $account->_id);
            
            try {
                $collection->dropIndex('tweet_retweet_count');
                $collection->dropIndex('tweet_favorite_count');
                $collection->dropIndex('tweet_time');
            }catch (\Exception $e){
                
            }
            $collection->createIndex(array('tweet_retweet_count'=>-1),array('background'=>true));
            $collection->createIndex(array('tweet_favorite_count'=>-1),array('background'=>true));
            $collection->createIndex(array('tweet_time'=>-1),array('background'=>true));
            
            $taObj              =   TargetAccounts::find($account->_id);
            $taObj->is_index    =   1;
            $taObj->save();
        }
    }
}