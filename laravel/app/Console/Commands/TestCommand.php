<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;

class TestCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'test';
	
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'testing....';
	
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
		
	public function handle()
	{
		
	}

}