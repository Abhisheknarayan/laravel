<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modal\Core\AccessToken;
use App\Helpers\Twitter;
use Abraham\TwitterOAuth\TwitterOAuth;

class TestAccessToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:accesstoken';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Access Token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     
     * @return mixed
     */
    public function handle()
    {
        
        $all_tokens             = 	AccessToken::where('is_active',1)->get();
    	$twitterObj	            =	new Twitter;
    	foreach ($all_tokens as $token){
    		$id 				= 	$token->id;
    		$api_key			=	$token->api_key;
    		$api_secret			=	$token->api_secret;
    		$access_token		=	$token->access_token;
    		$access_token_secret=	$token->access_token_secret;
    		
    		if($access_token == '' && $access_token_secret == ''){
    			$connection		=	new TwitterOAuth($api_key,$api_secret);
    		}else{
    			$connection		=	new TwitterOAuth($api_key,$api_secret,$access_token,$access_token_secret);
    		}
    		$app_status			=	$twitterObj->geo_location($connection,'pittsburgh');
            die;
    	}
    }
}