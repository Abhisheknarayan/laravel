<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\UserTweets;
use App\Modal\Core\UserTweetsQueue;
use App\Modal\Core\Accounts;
use App\Modal\Core\AccessToken;

class TrackUserTweets extends Command {
	
	use DispatchesJobs;
	
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'track:usertweets {--twitter_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Track Twitter Accounts of News and Business Categories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
    
    	$twitter_id			    =	$this->option('twitter_id');
    	if(is_null($twitter_id)){
    		$trackusers		    = 	Accounts::where('is_active',1)->get();
    	}else{
    		$trackusers		    = 	Accounts::where('twitter_id',$twitter_id)->where('is_active',1)->get();
    	}
    	$i=0;
    	$total_access_token  	=	AccessToken::where('is_active',1)->where('app_status',1)->count() - 1;
    	
    	foreach ($trackusers as $user){
    		//check whether download tweets job already in queue or not
    		$job_exists         =   UserTweetsQueue::find($user->twitter_id);
    		if(is_null($job_exists)){
    			$trackusertweets 		= 	new UserTweetsQueue;
    			$trackusertweets->_id	=	$user->twitter_id;
    			$trackusertweets->save();
    			if($i > $total_access_token){
    				$i=0;
    			}	
    			$job =	(new UserTweets($user,$i))->onQueue('tweetkit_track_user_tweets');
    			dispatch($job);
    			$i++;
    		}
    	}
    }
}