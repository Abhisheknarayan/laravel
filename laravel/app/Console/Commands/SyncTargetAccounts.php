<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;


use MongoDB\Client;
use MongoDB\Database;
use App\Modal\Core\TargetAccounts;

class SyncTargetAccounts extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'synctargetaccounts';
	
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sync target accounts';
	
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	public function handle()
	{
	    $mongodclient   =   new Client('mongodb://tweetkiturapi_ut:L77s%40K%4011312%40SL4%25%5E3S%40%3Fdsd@'.config('tweetkitapp.mongo_ip').'/',array('authSource'=>'tweetkit_ut'));
	    $manager        =   $mongodclient->getManager();
	    $databaseObj    =   new Database($manager, 'tweetkit_ut');
	    $collections    =   $databaseObj->listCollections();

	    foreach ($collections as $col){
	        $collection_name                   =   $col->getName();
	        $target_account                    =   TargetAccounts::find($collection_name);
	        if(is_null($target_account)){
	            $target_account                 =   new TargetAccounts();
	            $target_account->_id            =   $collection_name;
	            $target_account->last_tweet_id  =   '';
	            $target_account->is_index       =   0;
	            $target_account->save();
	        }
	    }
	}
}