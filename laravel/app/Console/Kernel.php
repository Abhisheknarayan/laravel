<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
	
	protected $commands = [
			
    	
    ];
	
	protected function commands()
	{
		$this->load(__DIR__.'/Commands');
			
		require base_path('routes/console.php');
	}

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule){
    	
    	$schedule->command('test:accesstoken')->cron('35 */6  * * *');	//	every 6 hours
    	
    	//account tracking News And Business Categories
    	$schedule->command('track:usertweets')->daily(); 			//run at 0  		every hour
    	
    	$schedule->command('add:index')->daily();
    	
    }
}