<?php

function nice_number($n){
    // first strip any formatting;
    $n = (0+str_replace(",", "",$n));
    // is this a number?
    if (!is_numeric($n)) return 0;
    
    // now filter it;
    if ($n > 1000000) return round(($n/1000000), 2).'M';
    elseif ($n > 1000) return round(($n/1000), 2).'K';
    
    return number_format($n);
}

function search_hashtags($allmpthashtag,$keyword){
	foreach ($allmpthashtag as $ht){
		if(array_key_exists($keyword, $ht)){
			return $ht[$keyword];
		}
	}
	return 0;
}


function getHashtags($string) {
	$hashtags= FALSE;
	$matches = FALSE;
	preg_match_all("/(#\w+)/u", $string, $matches);
	if ($matches) {
		$hashtagsArray = array_count_values($matches[0]);
		$hashtags = array_keys($hashtagsArray);
	}
	return $hashtags;
}

function tweetkit_file_download_encrypt($value){
	$encrypted = bin2hex(base64_encode($value));
	return $encrypted;
}

function tweetkit_file_download_decrypt($enc_text){
	$decrypted=base64_decode(hex2bin($enc_text));
	return $decrypted;
}

function add_quotes($str) {
	return sprintf("'%s'", $str);
}

function get_saved_tweets_csv_header(){
	$arr	=	array(
					'Twitter Id','Tweet Create Time (UTC)','Tweet','Is Retweet','ReTweet Count','Favorite Count',
					'User Id','Real Name','User Name','Location','Profile Image URL','Profile Banner URL','Bio',
					'Followers','Following','No. of Tweets','Verified Account'
				);
	return $arr;
}

function create_user_Obj($tu,$user){
	$tu->name				=	$user->name;
	$tu->username			=	strtolower($user->screen_name);
	if(isset($user->description)){
		$tu->user_bio		=	$user->description;
	}else{
		$tu->user_bio		=	'';
	}
	if(isset($user->time_zone)){
		$tu->timezone		=	$user->time_zone;
	}else{
		$tu->timezone		=	'';
	}
	if(isset($user->profile_image_url_https)){
		$tu->image_url		=	str_replace('_normal','_bigger',$user->profile_image_url_https);
	}else{
		$tu->image_url		=	'';
	}
	if(isset($user->profile_banner_url)){
		$tu->banner_url		=	$user->profile_banner_url;
	}else{
		$tu->banner_url		=	'';
	}
	if(isset($user->created_at)){
		$tu->account_create_date	=	date('Y-m-d H:i:s',strtotime($user->created_at));
	}else{
		$tu->account_create_date	=	'';
	}
	if(isset($user->verified)){
		$tu->verified_account	=	$user->verified;
	}else{
		$tu->verified_account	=	0;
	}
	if(isset($user->lang)){
		$tu->account_lang	=	$user->lang;
	}else{
		$tu->account_lang	=	'';
	}
	if(isset($user->location)){
		$tu->location		=	$user->location;
	}else{
		$tu->location		=	'';
	}
	$tu->is_active			=	1;
	$tu->is_deleted			=	0;
	if(isset($user->friends_count)){
		$tu->friends_count	=	$user->friends_count;
	}else{
		$tu->friends_count	=	0;
	}
	if(isset($user->followers_count)){
		$tu->followers_count	=	$user->followers_count;
	}else{
		$tu->followers_count	=	0;
	}
	if(isset($user->favourites_count)){
		$tu->favorites_count	=	$user->favourites_count;
	}else{
		$tu->favorites_count	=	0;
	}
	if(isset($user->statuses_count)){
		$tu->status_count	=	$user->statuses_count;
	}else{
		$tu->status_count	=	0;
	}
	
	return $tu;
}

function compare_retweets($x,$y){
    if ($x['retweet_count'] == $y['retweet_count']) return 0;
    return ($x['retweet_count'] < $y['retweet_count']) ? 1 : -1;
}

function compare_volume($x,$y){
	$x['tweet_volume']	=	is_null($x['tweet_volume']) ? 0 : $x['tweet_volume'];
	$y['tweet_volume']	=	is_null($y['tweet_volume']) ? 0 : $y['tweet_volume'];
	
	if ($x['tweet_volume'] == $y['tweet_volume']) return 0;
	return ($x['tweet_volume'] < $y['tweet_volume']) ? 1 : -1;
}

function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');
    
    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
    $bytes /= pow(1024, $pow);
    
    return round($bytes, $precision) . ' ' . $units[$pow];
}