<?php

namespace App\Http\Controllers\HISTORICAL;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

use PhpOffice\PhpSpreadsheet\IOFactory;

use App\Http\Controllers\Controller;

//use App\TwitterData;

use Config,Session,Response;

use App\Helpers\Twitter;

class TweetsController extends Controller
{

    function create(){
        $meta_title='';
        return view('historical.create',compact('meta_title'));   
    }

    function search_tweet_historical(Request $request){
        
        $search_keyword=$request->keyword;
        $limit= $request->limit;

        $twitterObj     =   new Twitter();
        try{
			$connection = $twitterObj->_create_connection();
		}
		catch (\Exception $e){
			return array();
		}
        
		
        $LANG_CODE 	= 	Config::get('tweetkitapp.lang');
       
        $filename   =   $search_keyword.'-'.time().'.xlsx';
        $filepath   =   config('tweetkitapp.historical_data').$filename;
        
        $oldmask	=	umask(0);
        $fp = fopen($filepath, 'w');
        fclose($fp);
        chmod($filepath,0777);
        umask($oldmask);
        
      //  Log::info($filepath);
        
        
        self::_fetch_search_tweets($connection,$search_keyword,$LANG_CODE,$limit,'',$filepath);
        return $this->downloadfile($filepath);

	}
	
	private function _fetch_search_tweets($connection,$search_keyword,$LANG_CODE,$limit,$max_id,$filepath){
		
		ini_set('memory_limit','-1');
        ini_set('max_execution_time', -1);
        
		$start = 0;
        $limitExceed =0;
		
        $spreadsheet = IOFactory::load($filepath);
        $sheet = $spreadsheet->getActiveSheet();
        
		while(1){
			//$twitterData=new TwitterData();
			$params['q']		  =	urlencode($search_keyword);
			$params['count']	  =	100;
			$params['tweet_mode'] = 'extended';
			
		
			if($max_id != ''){
				$params['max_id'] = $max_id;
			}
			
			try{
				$connection->setTimeouts(120,120);
				$search_tweets	=	$connection->get('search/tweets',$params);
				if(!isset($search_tweets->errors)){
					
					$search_feeds=$search_tweets->statuses;
					
					if(sizeof($search_feeds)>0 && bcsub($search_feeds[0]->id_str, $max_id)==0){
						array_shift($search_feeds);
					}
					
					$feed_size = sizeof($search_feeds);
                    
					$limitExceed += $feed_size;

                    if($limitExceed>$limit){
                        break;
                    }

					if($feed_size > 0){

						$insertData=array();
					//	$dbInsertData=array();
						foreach($search_feeds as $tweet){
							if(isset($tweet->id_str) && $tweet->id_str  != '' && isset($tweet->user->id_str) && isset($tweet->user->screen_name)){
								
								$csv_array = array();

								$csv_array['tweet_id']	=	'"'.$tweet->id_str.'"';
								
								if(isset($tweet->created_at))
									$csv_array['tcreated_at']	=	date('d M Y H:i:s',strtotime($tweet->created_at));
								else
									$csv_array['tcreated_at']	=	'';
									
								if(isset($tweet->full_text))
									$csv_array['text']	=	preg_replace('/\\\\/', '', htmlspecialchars(stripslashes($tweet->full_text)));
								else
									$csv_array['text']	=	'';
												
								if(isset($tweet->retweeted_status))
									$csv_array['is_retweet']	=	"Yes";
								else
									$csv_array['is_retweet']	=	"No";
														
														
								if(isset($tweet->in_reply_to_status_id_str) && !empty($tweet->in_reply_to_status_id_str))
									$csv_array['is_reply']		=	"Yes";
								else
									$csv_array['is_reply']		=	"No";
																
																
								if(isset($tweet->retweet_count))
									$csv_array['retweet_count']	=	$tweet->retweet_count;
								else
									$csv_array['retweet_count']	=	0;
																		
																		
								if(isset($tweet->favorite_count))
									$csv_array['favorite_count']	=	$tweet->favorite_count;
								else
									$csv_array['favorite_count']	=	0;
																				
								if(isset($tweet->source)){
									$tweet->source = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $tweet->source);
									$csv_array['source'] 	= 	$tweet->source;
								}else{
									$csv_array['source']	=	'';
								}
																				
								if(isset($tweet->lang) && $tweet->lang != 'und'){
									if(isset($LANG_CODE[$tweet->lang])){
										$tweet->lang = $LANG_CODE[$tweet->lang];
										$csv_array['lang']	=	$tweet->lang;
									}else{
										$csv_array['lang']	=	'';
									}
								}else if(isset($tweet->metadata->iso_language_code) && $tweet->metadata->iso_language_code != 'und'){
									if(isset($LANG_CODE[$tweet->metadata->iso_language_code])){
										$tweet->lang = $LANG_CODE[$tweet->metadata->iso_language_code];
										$csv_array['lang']	=	$tweet->lang;
									}else{
										$csv_array['lang']	=	'';
									}
								}else{
									$csv_array['lang']		=	'';
								}
																				
								$csv_array['user_id']	=	'"'.$tweet->user->id_str.'"';
																				
								if(isset($tweet->user->name	)){
									$csv_array['name']	=	preg_replace('/\\\\/', '', htmlspecialchars(stripslashes($tweet->user->name)));
								}else{
									$csv_array['name']	=	'';
								}
																				
								$csv_array['username']	=	$tweet->user->screen_name;
																				
								if(isset($tweet->user->location) && $tweet->user->location != ''){
									$csv_array['location']	=	'"'.$tweet->user->location.'"';
									$csv_array['location_lat_lng']	=	'';
								}
								else{
									$csv_array['location']			=	'';
									$csv_array['location_lat_lng']	=	'';
								}
																				
								if(isset($tweet->user->profile_image_url_https))
									$csv_array['profile_image']		=	$tweet->user->profile_image_url_https;
								else
									$csv_array['profile_image']		=	'';
																						
								if(isset($tweet->user->description))
									$csv_array['description']		=	preg_replace('/\\\\/', '', htmlspecialchars(stripslashes($tweet->user->description)));
								else
									$csv_array['description']		=	'';
																							
								if(isset($tweet->user->url))
									$csv_array['url']				=	$tweet->user->url;
								else
									$csv_array['url']				=	'';
																									
								if(isset($tweet->user->followers_count))
									$csv_array['followers_count']	=	$tweet->user->followers_count;
								else
									$csv_array['followers_count']	=	0;
																												
								if(isset($tweet->user->friends_count))
									$csv_array['friends_count']		=	$tweet->user->friends_count;
								else
									$csv_array['friends_count']		=	0;
																														
								if(isset($tweet->user->statuses_count))
									$csv_array['statuses_count']	=	$tweet->user->statuses_count;
								else
									$csv_array['statuses_count']	=	0;
																																
								if(isset($tweet->user->created_at))
									$csv_array['created_at']		=	date('d M Y H:i:s',strtotime($tweet->user->created_at));
								else
									$csv_array['created_at']		=	'';
																																		
								if(isset($tweet->user->verified)){
									if($tweet->user->verified)
										$csv_array['verified']		=	"Yes";
									else
										$csv_array['verified']      = 	"No";
								}else{
									$csv_array['verified']			=	'No';
								}
																																
								if(isset($tweet->user->profile_banner_url))
									$csv_array['profile_banner_url']		=	$tweet->user->profile_banner_url;
								else
									$csv_array['profile_banner_url']		=	'';
																																			
								if(isset($tweet->user->protected)){
									if($tweet->user->protected)
										$csv_array['protected']		=	'Yes';
									else
										$csv_array['protected']		=	'No';
								}else{
									$csv_array['protected']			=	'No';
								}
																																				
								if(isset($tweet->user->time_zone)){
									$csv_array['timezone']			=	$tweet->user->time_zone;
								}
								else{
									$csv_array['timezone']			=	'';
								}
																																				
                                //Insert Images
                                
								//$user_id = $tweet->user->id_str;
								$image_count = 0;
								$video_count = 0;
                                
								$csv_array['image_count']	=	$image_count;
								$csv_array['video_count']	=	$video_count;
							
								$insertData[]=$csv_array;

								$max_id =	$tweet->id_str;	
                            }
							else{
								break;
							}
						}
						
						
						
						if(!$this->create_excel($start++,$filepath,$insertData,$limitExceed,$sheet)){
						   break;
						}
						
					}else{
						break;
					}
				}
				else{
					break;
				}
			}catch(\Exception $e){
			} 
        }
        
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($filepath);
		
    }

    private function create_excel($start,$filepath,$result,$limitExceed,$sheet){

            try{

                if(count($result)<=0){
                    return false;
				}
				
				if($start == 0){

                    $sheet->setCellValue('A1', 'Tweet Id');
                    $sheet->setCellValue('B1', 'Tweet created at');
                    $sheet->setCellValue('C1', 'Text');
                    $sheet->setCellValue('D1', 'Is retweet');
                    $sheet->setCellValue('E1', 'Is reply');
                    $sheet->setCellValue('F1', 'Retweet count');
                    $sheet->setCellValue('G1', 'Favorite count');
                    $sheet->setCellValue('I1', 'Source');
                    $sheet->setCellValue('H1', 'Lang');
                    $sheet->setCellValue('J1', 'User id');
                    $sheet->setCellValue('K1', 'Name');
                    $sheet->setCellValue('L1', 'Username');
                    $sheet->setCellValue('M1', 'Location');
                    $sheet->setCellValue('N1', 'Location lat lng');
                    $sheet->setCellValue('O1', 'Profile image');
                    $sheet->setCellValue('P1', 'Description');
                    $sheet->setCellValue('Q1', 'Url');
                    $sheet->setCellValue('R1', 'Followers count');
                    $sheet->setCellValue('S1', 'Friends count');
                    $sheet->setCellValue('T1', 'Tweets');
                    $sheet->setCellValue('U1', 'Created at');
                    $sheet->setCellValue('V1', 'Verified');
                    $sheet->setCellValue('W1', 'Profile Banner');
                    $sheet->setCellValue('X1', 'Protected');
                    $sheet->setCellValue('Y1', 'Timezone');
                    
                  //  $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
                  //  Log::info($writer->save($filepath));
                }
                
                $i=($limitExceed-sizeof($result))+1;
                
                
               
                
                foreach($result as $row){
                        $i++;
                   //     Log::info($i);
                        $sheet->setCellValue('A'.$i, $row['tweet_id']); //
                        $sheet->setCellValue('B'.$i, $row['tcreated_at']);
                        $sheet->setCellValue('C'.$i, $row['text']);
                        $sheet->setCellValue('D'.$i, $row['is_retweet']);
                        $sheet->setCellValue('E'.$i, $row['is_reply']);
                        $sheet->setCellValue('F'.$i, $row['retweet_count']);
                        $sheet->setCellValue('G'.$i, $row['favorite_count']);
                        $sheet->setCellValue('I'.$i, $row['source']);
                        $sheet->setCellValue('H'.$i, $row['lang']);
                        $sheet->setCellValue('J'.$i, $row['user_id']);
                        $sheet->setCellValue('K'.$i, $row['name']);
                        $sheet->setCellValue('L'.$i, $row['username']);
                        $sheet->setCellValue('M'.$i, $row['location']);
                        $sheet->setCellValue('N'.$i, $row['location_lat_lng']);
                        $sheet->setCellValue('O'.$i, $row['profile_image']);
                        $sheet->setCellValue('P'.$i, $row['description']);
                        $sheet->setCellValue('Q'.$i, $row['url']);
                        $sheet->setCellValue('R'.$i, $row['followers_count']);
                        $sheet->setCellValue('S'.$i, $row['friends_count']);
                        $sheet->setCellValue('T'.$i, $row['statuses_count']);
                        $sheet->setCellValue('U'.$i, $row['created_at']);
                        $sheet->setCellValue('V'.$i, $row['verified']);
                        $sheet->setCellValue('W'.$i, $row['profile_banner_url']);
                        $sheet->setCellValue('X'.$i, $row['protected']);
                        $sheet->setCellValue('Y'.$i, $row['timezone']);
                 //       Log::info($row);
                }
               
            }
            catch(\Exception $e){
              //  Log::error('Excel Download : '.$e->getMessage().$e->getTraceAsString());
                return false;
            }
            return true;
	}
	
	public function downloadfile($filepath){

	    try{
            if(file_exists($filepath)){
                return Response::download($filepath, basename($filepath))->deleteFileAfterSend(true);
            }
            else{
                Session::flash('message','<div class="alert alert-danger"><strong>Failed !</strong> File does not exist.</div>');
            }
        }
        catch(\Exception $e){
            Session::flash('message','<div class="alert alert-danger"><strong>Failed !</strong> '.$e->getMessage().'</div>');
        }

        return redirect('/historical/create');
	}
	
}
