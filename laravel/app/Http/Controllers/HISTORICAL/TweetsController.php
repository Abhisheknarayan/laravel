<?php

namespace App\Http\Controllers\HISTORICAL;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Config,Session,Response;

use App\Modal\Historical\HistoricalData;

use App\Modal\Historical\Historical_collections;

use App\Jobs\HistoricalDataJob;

class TweetsController extends Controller
{

    function create(){

        $meta_title='';

        $historicalData =  HistoricalData::paginate(25);

        return view('historical.create',compact('meta_title','historicalData')); 

    }

    function search_tweet_historical(Request $request){

    	try{

    		$search_keyword = $request->keyword;

	        $limit 			= $request->limit;

	        $historicalData =  new HistoricalData();

	        $historicalData->keyword   		= $search_keyword; 
	        $historicalData->tweet_count	= $limit;
	        $historicalData->status 		= 0;
	        $historicalData->save();

	        HistoricalDataJob::dispatch($search_keyword,$limit,$historicalData->id)->onQueue('tweetkit_historical_data');

	        Session::flash('message','<div class="alert alert-success"><strong>Success !</strong> Request has been submitted will take some time.</div>');

    	}
    	catch(\Exception $e){

    		Session::flash('message','<div class="alert alert-danger"><strong>Failed !</strong>Something went wrong.</div>');

    	}

        return redirect('/historical/create');

	}
	
	public function downloadfile($filename){

		$filepath   =   config('tweetkitapp.historical_data').$filename;

	    try{
            if(file_exists($filepath)){
                return Response::download($filepath, basename($filepath));
            }
            else{
                Session::flash('message','<div class="alert alert-danger"><strong>Failed !</strong> File does not exist.</div>');
            }
        }
        catch(\Exception $e){
            Session::flash('message','<div class="alert alert-danger"><strong>Failed !</strong> '.$e->getMessage().'</div>');
        }

        return redirect('/historical/create');
	
	}

	public function deletefile($filename,$id){

		$filepath   =   config('tweetkitapp.historical_data').$filename;
        
	    try{

            if(file_exists($filepath)){

            	$historicalData =  new HistoricalData();

                $collectionName = str_replace('.csv','',$filename);

                $historicalData->where(['id'=>$id])->delete();

                $collectionObj  = new Historical_collections($collectionName);

                $collectionObj->truncate();

                unlink($filepath);

                Session::flash('message','<div class="alert alert-success"><strong>Success !</strong> Row deleted successfully.</div>');
            }
            else{
                Session::flash('message','<div class="alert alert-danger"><strong>Failed !</strong> File does not exist.</div>');
            }
        }
        catch(\Exception $e){
            Session::flash('message','<div class="alert alert-danger"><strong>Failed !</strong> '.$e->getMessage().'</div>');
        }

        return redirect('/historical/create');
	
	}
	
}
