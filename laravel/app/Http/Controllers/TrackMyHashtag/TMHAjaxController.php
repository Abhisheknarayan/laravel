<?php
namespace App\Http\Controllers\TrackMyHashtag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modal\TrackMyHashtag\TMHUser;
use App\Modal\TrackMyHashtag\TMHRestrictedKeywords;
use App\Modal\TrackMyHashtag\TMHHistoricalTrackers;

class TMHAjaxController extends Controller{
	
	public function save_user(Request $request){
		if($request->ajax()){
			$id						=	 $request->input('id');
			$is_active				=	 $request->input('is_active');
			$verified               =    $request->input('verified');
			
			$users					=	 TMHUser::find($id);
			$users->is_active		=	 $is_active;
			$users->verified        =    $verified;  
			
			if($users->save()){
				return JsonResponse::create(array('success'),'200');
			}else{
				return JsonResponse::create(array('failed'),'202');
			}
		}
		else{
			return JsonResponse::create(array(),'404');
		}
	}	
	
	public function delete_restricted_keyword(Request $request){
	    if($request->ajax()){
	        $id                     =   $request->input('id');
	        TMHRestrictedKeywords::destroy($id);
	        return JsonResponse::create(array(),'200');
	    }
	}
	
	public function update_restricted_keyword(Request $request){
	    if($request->ajax()){
	        $id                    =   $request->input('id');
	        $action                =   $request->input('action');
	        $tmhObj                =   TMHRestrictedKeywords::find($id);
	        $tmhObj->is_active     =   $action;
	        $tmhObj->save();
	        return JsonResponse::create(array(),'200');
	    }
	}
	
	public function historical_delete(Request $request){
	    if($request->ajax()){
	        $id                    =   $request->input('id');
	        TMHHistoricalTrackers::destroy($id);
	        return JsonResponse::create(array(),'200');
	    }
	}
	
	public function historical_update(Request $request){
	    if($request->ajax()){
	        $id                        =   $request->input('id');
	        $tweets                    =   $request->input('tweets');
	        $ftt                       =   $request->input('ftt');
	        $ltt                       =   $request->input('ltt');
	        $tmhhObj                   =   TMHHistoricalTrackers::find($id);
	        $tmhhObj->total_tweets     =   $tweets;
	        $tmhhObj->first_tweet_time =   $ftt;
	        $tmhhObj->last_tweet_time  =   $ltt;
	        $tmhhObj->save();
	        return JsonResponse::create(array(),'200');
	    }
	}
}