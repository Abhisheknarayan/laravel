<?php
namespace App\Http\Controllers\TrackMyHashtag;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modal\TrackMyHashtag\TMHUser;
use App\Modal\TrackMyHashtag\TMHTrackers;
use App\Modal\TrackMyHashtag\TMHSubscriptions;
use App\Modal\TrackMyHashtag\TMHTransactions;
use App\Modal\TrackMyHashtag\TMHHistoricalTrackers;
use App\Modal\TrackMyHashtag\TMHRestrictedKeywords;

class TMHWebsiteAdminController extends Controller
{	
    public function user(Request $request){
		$pg_val     	=   25;
		$verified		=	$request->input('verified');
		$email			=   $request->input('email');
		$active_account =   $request->input('is_active');
		$users 			=   new TMHUser();
		$total_count	=   $users::count();
		$not_verified   =   $users->where('verified',0)->count(); 
		$not_active     =   $users->where('is_active',0)->count();
		
		if($verified!=''){
			$users 		= 	$users->where('verified',$verified);
		}
		
		if($active_account != ''){
		    $users      =   $users->where('is_active',$active_account);
		}
		if($email!=''){
			$users  	=   $users->where('email',$email);
		}
		
		$search_count	=   $users->count();
		$users 			=   $users->orderBy('last_login_time', 'desc')->paginate($pg_val);
		
		return view('trackmyhashtag.users',compact('users','not_verified','not_active','verified','active_account','email','total_count','search_count'));
	}
	
	public function trackers(Request $request){
	    $trackers          =   TMHTrackers::where('is_deleted',0);
	    $ptrackers 		   =   clone $trackers;
	    
	    $total_trackers    =   $ptrackers->count();
	    $active_trackers   =   $ptrackers->where('is_active',1)->where('tracker_status',1)->count();
	    $finish_trackers   =   $ptrackers->where('tracker_status',2)->count();
	    
	    $active_accounts   =   $request->input('is_active');
	    
	    if($active_accounts != ''){
	        $trackers      =   $trackers->where('is_active',$active_accounts);
	    }
	    $user_id           =   $request->input('user_id');
	    
	    if(!is_null($user_id)){
	        $trackers      =   $trackers->where('user_id',$user_id);
	    }
	    
	    $search_count	   =   $trackers->count();
	    
	    $trackers          =   $trackers->orderBy('created_at','DESC')->paginate(25);
	    return view('trackmyhashtag.trackers',compact('finish_trackers','active_trackers','total_trackers','trackers','active_accounts','search_count'));
	}

	public function transactions(Request $request){
	    $user_trans        = 	TMHTransactions::join('users','transactions.user_id','=','users.id')->select('email','tweets_count','amount','transactions.created_at');
        $total_count	   =    $user_trans->count();
	    $user_id		   =	$request->input('user_id');
	    $email             =    $request->input('email');
	    if(!is_null($user_id)){
	        $user_trans    =    $user_trans->where('users.id',$user_id);
	    }
	    if(!is_null($email)){
	        $user_trans    =    $user_trans->where('email',$email);
	    }
	    
	    
	    $user_trans_count  =	$user_trans->count();
	    
	    $user_trans        =	$user_trans->orderBy('transactions.created_at','desc')->paginate(25);
	    
	    return view('trackmyhashtag.transactions',compact('user_trans','user_trans_count','total_count','email'));
	}

	public function subscriptions(Request $request){
		$user_subs 		    = 	TMHSubscriptions::join('users','subscriptions.user_id','=','users.id')->select('users.id','email','subscriptions.created_at');
		$total_count		=   $user_subs->count();
		$user_id			=	$request->input('user_id');
		$email              =   $request->input('email');
		if(!is_null($user_id)){
			$user_subs 	    =   $user_subs->where('users.id',$user_id);
		}
		if(!is_null($email)){
		    $user_subs      =   $user_subs->where('email',$email);
		}
		
	
		$user_subs_count	=	$user_subs->count();
		
		$user_subs 		=	$user_subs->orderBy('subscriptions.created_at','desc')->paginate(20);
		
		return view('trackmyhashtag.subscriptions',compact('user_subs','user_subs_count','total_count','email'));
	}
	
	public function historical(Request $request){
	    if($request->post()){
	        $req                                   =   $request->all();
	        if(!empty($req['user_id']) && !empty($req['keyword'])){
    	        $tmhhistorical                     =   new TMHHistoricalTrackers;
    	        $tmhhistorical->user_id            =   $req['user_id'];
    	        $tmhhistorical->keyword            =   $req['keyword'];
    	        $tmhhistorical->view_type          =   2;
    	        $tmhhistorical->token              =   md5($req['keyword'].$req['user_id'].date('Y-m-d H:i:s'));
    	        $tmhhistorical->total_tweets       =   (!empty($req['total_tweets']) ? $req['total_tweets'] : 0);
    	        $tmhhistorical->first_tweet_time   =   date('Y-m-d 00:00:00',strtotime($req['first_tweet_date']));
    	        $tmhhistorical->last_tweet_time    =   date('Y-m-d 23:59:59',strtotime($req['last_tweet_date']));
    	        if($tmhhistorical->save()){
    	            $request->session()->flash('s_message','Added Successfully');
    	            return redirect('/tmh/historical');
    	        }
	        }
	    }
	    $tmhhistorical = TMHHistoricalTrackers::orderBy('created_at','DESC')->paginate(20);
	    return view('trackmyhashtag.historical',compact('tmhhistorical'));
	}
	
	public function restricted_keywords(Request $request){
	    if($request->isMethod('post')){
	       $req                                    =   $request->all();
	       $keyword                                =   strtolower(trim($req['keyword']));
	       if($keyword == ''){
	           \Session::flash('k_error','Empty Keyword.'); 
	       }
	       else{
    	       $tmhrestrictedkeyword               =   new TMHRestrictedKeywords();
    	       $tmhrestrictedkeyword->keyword      =   $keyword;
    	      
    	       try {
    	           if($tmhrestrictedkeyword->save()){
                        \Session::flash('k_success','Keyword Addedd Successfully.');    	               
    	           }else{
    	               \Session::flash('k_success','An Error Occurred.');
    	           }
    	       }catch(\Exception $e){
    	           \Session::flash('k_error',$e->getMessage());
    	       }
	       }
	       return redirect()->back();
	    }
	    
	    $tmhrestrictedkeyword = TMHRestrictedKeywords::paginate(10); 
	    return view('trackmyhashtag.restricted_keywords',compact('tmhrestrictedkeyword'));
	}
}