<?php
namespace App\Http\Controllers\MPT;

use App\Http\Controllers\Controller;
use App\Modal\MPT\Reports;

class DownloadController extends Controller
{  
    function download_file($twitter_id,$report_twitter_id,$hash){
      
        $report            =   Reports::where('twitter_id',$twitter_id)
                                ->where('report_twitter_id',$report_twitter_id)
                                ->where('hash',$hash)
                                ->where('status',2)->first();
        if(sizeof($report) > 0){
            $files         =   unserialize(tweetkit_file_download_decrypt($report['file_path']));
            $zip_file_path =   config('mpt.zip_file_dir').'/'.$report_twitter_id.'.zip';
            
            $zip = new \ZipArchive();
            if($zip->open($zip_file_path,\ZipArchive::CREATE) === TRUE){
                $zip->addFile($files['tweets'],basename($files['tweets']));
                $zip->addFile($files['retweets'],basename($files['retweets']));
                $zip->addFile($files['likes'],basename($files['likes']));
            }
            $zip->close();
            
            header('Content-Description: Report Download');
            header("Content-type: application/force-download");
            header("Content-type: application/zip");
            header('Content-Disposition: attachment; filename='.basename($zip_file_path));
            header("Content-Transfer-Encoding: Binary");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($zip_file_path));
            $content =  readfile($zip_file_path);
            unlink($zip_file_path);
            return $content;
        }else{
            return abort(404);
        }
    }
}