<?php
namespace App\Http\Controllers\MPT\API;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Illuminate\Http\Request;
use App\Modal\MPT\Subscriptions;
use App\Modal\MPT\MobileUsers;
use Cache;
use App\Modal\MPT\Transactions;
//use Log;
Class InAppAPIController extends ApiGuardController {
		
    
    function product_receipt(Request $request){
        try {
            $purchase			=	$request->input('purchase');
            $twitter_id			=	$request->input('twitter_id');
            //delete premium cache
            @Cache::delete(md5('is_premium_'.$twitter_id));
            
            /**
            
             [2019-09-22 03:34:44] live.INFO: {
                "orderId":"GPA.3338-4831-1263-99893",
                "packageName":"com.mostpopulartweet",
                "productId":"pqrs_007",
                "purchaseTime":1569123279972,
                "purchaseState":0,
                "purchaseToken":"cfcffgnfgpahlplpjpngdagb.AO-J1OzhkRNQKylrb-gQjHjax4BMLNxR28iqL_NRoClRKeVeAmeou18ezjtBt7aMsdh4tk58gG4EF5PV3Y0_MbAn9hVXEumuRt__gc4gFRYFbvsNlfyxT9c"
               }
              */
            
            
            if(!is_null($purchase) && !is_null($twitter_id)){
                try {
                    $purchase		=	json_decode($purchase);
//                  $purchase		=	json_decode($purchase->mOriginalJson);
                    
                }catch (\Exception $e){
                    return $this->response->errorWrongArgs();
                }
                $key_path			=	app_path('keys').'/Most-Popular-Tweet-69f06d1eb382.json';
                $gclient			=	new \Google_Client();
                $gclient->setScopes(array('https://www.googleapis.com/auth/androidpublisher'));
                $gclient->setApplicationName('Most Popular Tweet for Twitter');
                $gclient->setAuthConfig($key_path);
                putenv('GOOGLE_APPLICATION_CREDENTIALS='.$key_path);
                $gclient->useApplicationDefaultCredentials();
                
                $service 			= 	new \Google_Service_AndroidPublisher($gclient);
                $packageName 		= 	"com.mostpopulartweet";
                
                $product		    =	$service->purchases_products->get($packageName, $purchase->productId, $purchase->purchaseToken);
              
                if (is_null($product)) {
                    return $this->response->errorUnauthorized();
                } elseif (isset($product->error->code)) {
                    return $this->response->errorUnauthorized();
                } elseif (!isset($product->orderId)) {
                    return $this->response->errorUnauthorized();
                }
                
                $subsObj							=	new Transactions();
                $subsObj->twitter_id				=	$twitter_id;

                if(isset($purchase->purchaseToken)){
                    $subsObj->purchase_token		=	$purchase->purchaseToken;
                }else{
                    $subsObj->purchase_token		=	'';
                }

                if(isset($product->consumptionState)){
                    $subsObj->consumption_state		=	$product->consumptionState;
                }else{
                    $subsObj->consumption_state		=	'';
                }
                
                if(isset($product->kind)){
                    $subsObj->kind		            =	$product->kind;
                }else{
                    $subsObj->kind		            =	'';
                }
                
                if(isset($product->orderId)){
                    $subsObj->order_id			    =	$product->orderId;
                }else{
                    $subsObj->order_id			    =	'';
                }
                
                if(isset($product->purchaseState)){
                    $subsObj->purchase_state		=	$product->purchaseState;
                }else{
                    $subsObj->purchase_state		=	'';
                }
                
                // Purchase Type  --  0  Test   1 Promo
                if(isset($product->purchaseType)){
                    $subsObj->purchase_type			=	$product->purchaseType;
                }else{
                    $subsObj->purchase_type			=	'';
                }
                
                if(isset($product->purchaseTimeMillis)){
                    $subsObj->purchase_time_millis	=	$product->purchaseTimeMillis;
                }else{
                    $subsObj->purchase_time_millis	=	'';
                }
                
                if($subsObj->save()){
                    $muObj	=	MobileUsers::where('twitter_id',$twitter_id)->update(['is_premium'=>1,'subscription_id'=>$subsObj->_id]);
                    if($muObj){
                        return $this->response->withArray(array());
                    }else{
                        return $this->response->errorInternalError();
                    }
                }
                else{
                    return $this->response->errorInternalError();
                }
            }else{
                return $this->response->errorWrongArgs();
            }
        }catch (\Exception $e){
            return $this->response->errorUnauthorized();
        }
    }
	
	function subscription_receipt(Request $request){
		try {
			$purchase			=	$request->input('purchase');
			$twitter_id			=	$request->input('twitter_id');
			
			//delete premium cache
			@Cache::delete(md5('is_premium_'.$twitter_id));
			
			if(!is_null($purchase) && !is_null($twitter_id)){
				
			    try {
				    /*
				    autoRenewingAndroid: true
				    dataAndroid: 
				    "{
				    "orderId":"GPA.3317-1411-9298-29849",
				    "packageName":"com.mostpopulartweet",
				    "productId":"001msmpt",
				    "purchaseTime":1558606829154,
				    "purchaseState":0,
				    "purchaseToken":"ajmkfcnclhafacgoopomefkj.AO-J1Ox6bLv_Flc7w1BvQLcuGv466xzpRVzLYR6ikDIcau9-uFJK_u_t_h88pCNFRuhmA_AaF_AwvO1fQakKJnvCSF6qLmT_5MGEkDSyrkSbw2QUsi6yYF4",
				    "autoRenewing":true}"
				    
				    productId: "001msmpt"
				    
				    purchaseToken: "ajmkfcnclhafacgoopomefkj.AO-J1Ox6bLv_Flc7w1BvQLcuGv466xzpRVzLYR6ikDIcau9-uFJK_u_t_h88pCNFRuhmA_AaF_AwvO1fQakKJnvCSF6qLmT_5MGEkDSyrkSbw2QUsi6yYF4"
				    signatureAndroid: "Yk/VkbWeblbAOsye3d3UIZ9hJbmy42xr9QR2KgWodTw73U+nQmmM6qv2Gi3FkzavC3X4jhRSUWHFjZ73GzmBHaJ3LeRYJHjxnZrJu887rVRrLnpn+Hv4XN717u4eWepV6QMswPPpFrSDQ9SIz3kaIQrjylZBlEqG7mae2tzn02VrfUjloaRAME4SwkjZKUENnkbwbRau5mlPqTOfwz9NzC2XPrSypab0hee+x+vzZyC5bccHgyzdAWC3rynTHVOElog/GuZWWgznY4/jJS7vDHTmFxsu7ThwoybNM8NJ5jlcfJticA7hsheGftMEzH9kvPLiVg6nB1Zd7aEL0eCB1A=="
				    transactionDate: "1558606829154"
				    transactionId: "GPA.3317-1411-9298-29849"
				    
				    transactionReceipt: 
				    "{"orderId":"GPA.3317-1411-9298-29849",
				    "packageName":"com.mostpopulartweet",
				    "productId":"001msmpt",
				    "purchaseTime":1558606829154,
				    "purchaseState":0,
				    "purchaseToken":"ajmkfcnclhafacgoopomefkj.AO-J1Ox6bLv_Flc7w1BvQLcuGv466xzpRVzLYR6ikDIcau9-uFJK_u_t_h88pCNFRuhmA_AaF_AwvO1fQakKJnvCSF6qLmT_5MGEkDSyrkSbw2QUsi6yYF4",
				    "autoRenewing":true
				    }"
				    */
				    
					/*** 
					{
						"mOriginalJson":
							"{
								\"orderId\":\"GPA.3316-3321-1458-88767\",
								\"packageName\":\"com.tweetkit.app\",
								\"productId\":\"001hs\",
								\"purchaseTime\":1525161973319,
								\"purchaseState\":0,
								\"purchaseToken\":\"deindhnhjgkapdhbchkbncak.AO-J1OzNNiEjSGLWjbHE_TC2xwwCWZo0i_ZYbaad77kn_ZycpxXD5KAD3URjkkGePSh5beAQ0Z8nIJvx43kl2qVnENbOmBok8o-MXWpWDGIGoc0HBXWm5tA\",
								\"autoRenewing\":true
							}",
						"mParsedJson":{
							"nameValuePairs":{
								"orderId":"GPA.3316-3321-1458-88767",
								"packageName":"com.tweetkit.app",
								"productId":"001hs",
								"purchaseTime":1525161973319,
								"purchaseState":0,
								"purchaseToken":"deindhnhjgkapdhbchkbncak.AO-J1OzNNiEjSGLWjbHE_TC2xwwCWZo0i_ZYbaad77kn_ZycpxXD5KAD3URjkkGePSh5beAQ0Z8nIJvx43kl2qVnENbOmBok8o-MXWpWDGIGoc0HBXWm5tA",
								"autoRenewing":true
							}
						},
						"mSignature":"fVQyzGy2CQBagkI+GVq6jYEqnZRI9X4a+a+S00rjPyYMP270a6LOwlsBNsmNOao60IWEOTwJ3rfiqYJ5AQ90DEmE8RyS1zXibGhpIhyrc4ypEEiGD2Z6cCS+dqvoycahGgUPWWUPEBa7+rmO5FViWXHa0dJqa5BJ755BgdOLWYwfK8AcSivPD26h5g4fLJdhsRKsVFIFvt5s5w8ivinjYi1WYEVHqHbLCfuUymHO3dxv/vQbu03YYZRb5r0UPl7vNlNUZOxz8znWlMmhcIyCJ8jLfTO8+xPS4nTdSop21iIVbICUwlEpscR2DrcxP0CDQQTRWMn8lreBLp9FjGCa7Q\u003d\u003d"
					} 
					**/
			    
				    $purchase	=	json_decode($purchase);
				}catch (\Exception $e){
					return $this->response->errorWrongArgs();
				}
				
				$key_path			=	app_path('keys').'/Most-Popular-Tweet-69f06d1eb382.json';
				$gclient			=	new \Google_Client();
				$gclient->setScopes(array('https://www.googleapis.com/auth/androidpublisher'));
				$gclient->setApplicationName('Most Popular Tweet for Twitter');
				$gclient->setAuthConfig($key_path);
				putenv('GOOGLE_APPLICATION_CREDENTIALS='.$key_path);
				$gclient->useApplicationDefaultCredentials();
				
				$service 			= 	new \Google_Service_AndroidPublisher($gclient);
				$packageName 		= 	"com.mostpopulartweet";
				
				$product			=	$service->purchases_subscriptions->get($packageName, $purchase->productId, $purchase->purchaseToken);
				
				if (is_null($product)) {
					return $this->response->errorUnauthorized();
				} elseif (isset($product->error->code)) {
				    return $this->response->errorUnauthorized();
				} elseif (!isset($product->orderId)) {
					return $this->response->errorUnauthorized();
				}
				
				//add time validation
				//https://developers.google.com/android-publisher/api-ref/purchases/subscriptions
				//Log::info(serialize($product));
				
				$subsObj							=	new Subscriptions();
				$subsObj->twitter_id				=	$twitter_id;
				$subsObj->is_cancel					=	0;
				
				if(isset($purchase->purchaseToken)){
					$subsObj->purchase_token		=	$purchase->purchaseToken;
				}else{
					$subsObj->purchase_token		=	'';
				}
				
				if(isset($product->linkedPurchaseToken)){
					$subsObj->linkedPurchaseToken	=	$product->linkedPurchaseToken;
				}else{
					$subsObj->linkedPurchaseToken	=	'';
				}
				
				if(isset($product->autoRenewing)){
					$subsObj->auto_renewing			=	$product->autoRenewing;
				}else{
					$subsObj->auto_renewing			=	false;
				}
				
				//0 User canceled the subscription
				//1 Subscription was cancelled by the system, for example because of a billing problem
				//2 Subscription was replaced with a new subscription
				//3 Subscription was cancelled by the developer
				
				if(isset($product->cancelReason)){
					$subsObj->cancel_reason		=	$product->cancelReason;
				}else{
					$subsObj->cancel_reason		=	0;
				}
				
				if(isset($product->countryCode)){
					$subsObj->country_code		=	$product->countryCode;
				}else{
					$subsObj->country_code		=	'';
				}
				
				if(isset($product->emailAddress)){
					$subsObj->email				=	$product->emailAddress;
				}else{
					$subsObj->email				=	'';
				}
				
				if(isset($product->expiryTimeMillis)){
					$subsObj->expiry_time		=	$product->expiryTimeMillis;
				}else{
					$subsObj->expiry_time		=	'';
				}
				
				if(isset($product->familyName)){
					$subsObj->family_name		=	$product->familyName;
				}else{
					$subsObj->family_name		=	'';
				}
				
				if(isset($product->givenName)){
					$subsObj->given_name		=	$product->givenName;
				}else{
					$subsObj->given_name		=	'';
				}
				
				if(isset($product->orderId)){
					$subsObj->order_id			=	$product->orderId;
				}else{
					$subsObj->order_id			=	'';
				}
				
				
				// 0 Payment Pending
				// 1 Payment Received
				// 2 Free trial
				if(isset($product->paymentState)){
					$subsObj->payment_state		=	$product->paymentState;
				}else{
					$subsObj->payment_state		=	'';
				}
				
				if(isset($product->priceAmountMicros)){
					$subsObj->price_amount		=	$product->priceAmountMicros;
				}else{
					$subsObj->price_amount		=	'';
				}
				
				if(isset($product->priceCurrencyCode)){
					$subsObj->price_currency	=	$product->priceCurrencyCode;
				}else{
					$subsObj->price_currency	=	'';
				}
				
				if(isset($product->profileId)){
					$subsObj->profile_id		=	$product->profileId;
				}else{
					$subsObj->profile_id		=	'';
				}
				
				if(isset($product->profileName)){
					$subsObj->profile_name				=	$product->profileName;
				}else{
					$subsObj->profile_name				=	'';
				}
				
				// Purchase Type  --  0  Test   1 Promo
				if(isset($product->purchaseType)){
					$subsObj->purchase_type				=	$product->purchaseType;
				}else{
					$subsObj->purchase_type				=	'';
				}
				
				if(isset($product->startTimeMillis)){
					$subsObj->start_time_millis			=	$product->startTimeMillis;
				}else{
					$subsObj->start_time_millis			=	'';
				}
				
				if(isset($product->userCancellationTimeMillis)){
					$subsObj->user_cancellation_time	=	$product->userCancellationTimeMillis;
				}else{
					$subsObj->user_cancellation_time	=	'';
				}
				
				if($subsObj->save()){
					$muObj	=	MobileUsers::where('twitter_id',$twitter_id)->update(['is_premium'=>1,'subscription_id'=>$subsObj->_id]);
					if($muObj){
						return $this->response->withArray(array());
					}else{
						return $this->response->errorInternalError();
					}
				}
				else{
					return $this->response->errorInternalError();
				}
			}else{
				return $this->response->errorWrongArgs();
			}
		}catch (\Exception $e){
			return $this->response->errorUnauthorized();
		}
	}
}