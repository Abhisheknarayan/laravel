<?php
namespace App\Http\Controllers\MPT\API;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Modal\MPT\MobileUsers;
use App\Modal\Core\UserTweetsQueue;
use App\Jobs\RegisteredUserTweetsMPT;
use App\Modal\Core\TwitterUsers;
use Illuminate\Http\Request;
use App\Modal\Core\TargetAccounts;
use App\Helpers\Twitter;
use App\Modal\MPT\Reports;
use App\Jobs\MPTTweets;
use Cache;
use Log;

Class UserAPIController extends ApiGuardController {
	
	private function _push_to_queue_ut($user,$target_twitter_id){
		try {
			$job_exists = UserTweetsQueue::find($target_twitter_id);
			if(is_null($job_exists)){
				$trackusertweets 			=	new UserTweetsQueue;
				$trackusertweets->_id		=	$target_twitter_id;
				if($trackusertweets->save()){
					$job =	(new RegisteredUserTweetsMPT($user,$target_twitter_id))->onQueue('mpt_registered_user_tweets');
					dispatch($job);
				}
			}
		}catch(\Exception $e){}
	}
	
	private function _create_target_account($target_twitter_id){
		$target_accountObj 					=	TargetAccounts::find($target_twitter_id);
		if(is_null($target_accountObj)){
			$target_account					=	new TargetAccounts();
			
			$target_account->_id			=	$target_twitter_id;
			$target_account->last_tweet_id	=	'';
			try {
				$target_account->save();
			}catch(\Exception $e){}
		}
	}
	
	
	
	private function _create_user_tweet_Obj($user_tweets,$twitter_id,$target_twitter_id){
		$tweets		=	array();
		foreach ($user_tweets as $tweet){
			$temp						=	array();
			$temp['id_str']				=	$tweet['_id'];
			
			if(isset($tweet['is_retweet'])){ 
				$temp['is_retweet']		=	($tweet['is_retweet'] ==  'Yes') ? true:false;
			}else{
				$temp['is_retweet']		=	false;
			}
			
			if(isset($tweet['tweet_time'])){
				$temp['tweet_time']		=	date("d M Y",strtotime($tweet['tweet_time']));
			}else{
				$temp['tweet_time']		=	'';
			}
			
			if(isset($tweet['tweet_text'])){
			     /*
			    $tweet_text             =    $tweet['tweet_text'];
			    $m_tweet_text		    =	 htmlspecialchars_decode(trim(preg_replace("/(https?:\/\/[^\/]+(?:[\d\w\/\._]*)\s*)$/i",'', $tweet['tweet_text'])), ENT_NOQUOTES);
			    if($m_tweet_text == ''){
			        $m_tweet_text = $tweet_text;
			    }
			    $temp['tweet_text']  = $m_tweet_text;
			    */
			    $temp['tweet_text']  =  $tweet['tweet_text'];
			}else{
				$temp['tweet_text']		=	'';
			}
			
			if(isset($tweet['tweet_source'])){
				$temp['tweet_source']	=	$tweet['tweet_source'];
			}else{
				$temp['tweet_source']	=	'';
			}
			
			if(isset($tweet['tweet_retweet_count'])){
				$temp['retweet_count']		=	$tweet['tweet_retweet_count'];
			}else{
				$temp['retweet_count']		=	0;
			}
			
			if(isset($tweet['tweet_favorite_count'])){
				$temp['favorite_count']		=	$tweet['tweet_favorite_count'];
			}else{
				$temp['favorite_count']		=	0;
			}

			$temp['user']				    =	TwitterUsers::Select('screen_name','name','verified','profile_image_url_https')->find($target_twitter_id)->toArray();
			//unset($temp['user']['_id']);
		
			if($temp['is_retweet']){
				$temp['rtuser']				=	TwitterUsers::Select('screen_name','name','verified','profile_image_url_https')->find($tweet['user_id'])->toArray();
			}else{
				$temp['rtuser']			=	null;
			}
			
			
			if(isset($tweet['media'])){
				$temp['media']	=	$tweet['media'];
			}else{
				$temp['media']	=	null;
			}


			if($temp['is_retweet']){
				if($twitter_id  != $target_twitter_id) {
					$temp['is_retweet']	=	false;
				}
			}
			
			$tweets[]	=	$temp;
		}
		return array('tweets'=>$tweets);
	}
		
	
	/**
	 * Add New  Mobile User
	 * @param Request $request
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed
	 */
	public function add_user(Request $request){
		$twitter_id			                =	$request->input('twitter_id');
		$username			                =	$request->input('username');
		$oauth_token		                =	$request->input('oauth_token');
		$oauth_token_secret	                =	$request->input('oauth_token_secret');
	
		if(!is_null($twitter_id)  && !is_null($username) && !is_null($oauth_token) && !is_null($oauth_token_secret)){
			$user 							= 	MobileUsers::where('twitter_id',$twitter_id)->first();
			$is_updated						=	1;
			if(is_null($user)){
				$is_updated					=	0;
				$user 						= 	new MobileUsers;
				$user->is_premium			=	0;
				$user->is_active			=	1;
				$user->is_blocked			=	0;
			}
			
			$user->twitter_id			    =	$twitter_id;
			$user->username				    =	strtolower($username);
			$user->oauth_token			    =	$oauth_token;
			$user->oauth_token_secret	    =	$oauth_token_secret;
			
			if($request->input('name') != null){
				$user->name                 =   trim($request->input('name'));
			}else{
				$user->name	                =	'';
			}
			
			if($request->input('email') != null){
				$user->email	            =	trim(strtolower($request->input('email')));
			}else{
				$user->email	            =	'';
			}
			
			if($request->input('user_bio') != null){
				$user->user_bio	            =	trim(strtolower($request->input('user_bio')));
			}else{
				$user->user_bio	            =	'';
			}
			
			if($request->input('timezone') != null){
				$user->timezone	            =	trim(strtolower($request->input('timezone')));
			}else{
				$user->timezone	            =	'';
			}
			
			if($request->input('image_url') != null){
				$user->image_url	        =	str_replace('_normal', '_bigger', trim(strtolower($request->input('image_url'))));
			}else{
				$user->image_url	        =	'';
			}
			
			if($request->input('banner_url') != null){
				$user->banner_url	        =	$request->input('banner_url');
			}else{
				$user->banner_url	        =	'';
			}
			
			if($request->input('account_create_date') != null){
				$user->account_create_date	=	$request->input('account_create_date');
			}else{
				$user->account_create_date	=	'';
			}
			
			if($request->input('verified_account') != null){
				$user->verified_account	    =	$request->input('verified_account');
			}else{
				$user->verified_account	    =	0;
			}
			
			if($request->input('account_lang') != null){
				$user->account_lang	        =	$request->input('account_lang');
			}else{
				$user->account_lang	        =	'';
			}
			
			if($request->input('location') != null){
				$user->location 	        =	trim($request->input('location'));
			}else{
				$user->location		        =	'';
			}
			
			if($request->input('friends_count') != null){
				$user->friends_count	    =	$request->input('friends_count');
			}else{
				$user->friends_count	    =	0;
			}
			
			if($request->input('followers_count') != null){
				$user->followers_count	    =   $request->input('followers_count');
			}else{
				$user->followers_count  	=	0;
			}
			
			if($request->input('favorites_count') != null){
				$user->favorites_count	    =	$request->input('favorites_count');
			}else{
				$user->favorites_count	    =	0;
			}
			
			if($request->input('status_count') != null){
				$user->status_count	        =	$request->input('status_count');
			}else{
				$user->status_count	        =	0;
			}
			$user->login_ip		            =	$request->ip();
			
			if($user->save()){
				$this->_create_target_account($user->twitter_id);
				$this->_push_to_queue_ut($user,$user->twitter_id);
				
				if($is_updated){
					return $this->response->withArray(array('msg'=>'updated'));
				}
				else{
					return $this->response->withArray(array('msg'=>'added'));
				}
			}else{
				return $this->response->errorInternalError('An error occured. Please try again.');
			}
		}else{
			return $this->response->errorWrongArgs('Wrong Arguments');
		}
	}
	
	/**
	 * 2 mins caching
	 * //delete caching after upgrade
	 */
	public function is_premium($twitter_id){
		if(!is_null($twitter_id)){
		    $key                  =   md5('is_premium_'.$twitter_id);
		    $is_premium           =   Cache::get($key);
		    if(is_null($is_premium)){
		        $muObj	          =    MobileUsers::select('is_premium')->where('twitter_id',$twitter_id)->first();
		        if(is_null($muObj)){
		            $is_premium   =   false;
		        }else{
		            $is_premium   =   ($muObj['is_premium']) ? true : false ;
		            @Cache::put($key,$is_premium,2);
		        }
		    }
		    return json_encode($is_premium);
		}else{
			return $this->response->errorWrongArgs();
		}
	}
	
	
	/**
	 * 5 mins caching
	 * 
	 * @param string $loggedin_twitter_id
	 * @param string $query
	 * @return \Illuminate\Contracts\Routing\ResponseFactory
	 */
	public function get_user_search($loggedin_twitter_id,$query){
	   $key    =   md5('query_'.$loggedin_twitter_id.'_'.urlencode(strtolower($query)));
	   $users  =   Cache::get($key);
	   if(is_null($users)){
            $muser		   =   MobileUsers::where('twitter_id',$loggedin_twitter_id)->first();
            $twitterObj	   =   new Twitter();
            $search_result =   $twitterObj->user_search($query, $muser,1);
            if(count($search_result) > 0){
                $users     =   array();
                foreach ($search_result as $sr){
                    $user       =   array('id'=>$sr->id_str,'name'=>$sr->name,'username'=>$sr->screen_name,'description'=>$sr->description,'image'=>$sr->profile_image_url_https,'verified'=>$sr->verified);
                    $users[]    =   $user;
                }
                @Cache::put($key, $users,5); 
            }else{
                $users  =   array();
            }
	   }	   
	   return $this->response->withArray($users);
	}

	function files($twitter_id){
	    $reports            =   Reports::where('twitter_id',$twitter_id)->orderBy('created_at','desc')->limit(30)->get();
	    $files              =   array();
	    foreach ($reports as $report){
	        $twitterUser    =   TwitterUsers::find($report->report_twitter_id);
	        if(!is_null($twitterUser)){
	            $files[]    =   array(
	                'report_twitter_id' =>  $report->report_twitter_id,
	                'hash'              =>  $report->hash,
	                'status'            =>  $report->status,
	                'progress'          =>  $report->progress,
	                'name'              =>  $twitterUser->name,
	                'username'          =>  $twitterUser->screen_name,
	                'image'             =>  $twitterUser->profile_image_url_https
	            );
	        }
	    }
	    return $this->response->withArray($files);
	}
	
	public function file_request($loggedin_twitter_id,$twitter_id){
	    try {
	        //Create Base Folder
	        $user_folder_path   =   config('mpt.reports_dir').'/'.$loggedin_twitter_id;
	        
	        $thash			    =   md5($twitter_id.'3'.$loggedin_twitter_id.'34'.time().rand(0,100));
	        
	        $files			    =   array(
                        	            'tweets'	    =>	$user_folder_path.'/'.$thash.'/tweets.csv',
                        	            'retweets'	    =>	$user_folder_path.'/'.$thash.'/retweets.csv',
                        	            'likes'	        =>	$user_folder_path.'/'.$thash.'/likes.csv'
                        	        );
	        
	        $oldmask = umask(0);
	        if(!file_exists($user_folder_path) || !file_exists($user_folder_path.'/'.$thash)){
	            @mkdir($user_folder_path.'/'.$thash,0777,true);
	        }
	        
	        foreach ($files as $filepath){
	            $fp = fopen($filepath, 'w');
	            fclose($fp);
	            chmod($filepath,0777);
	        }
	        umask($oldmask);
	        
	        $reportObj						=	new Reports();
	        $reportObj->twitter_id          =   $loggedin_twitter_id;
	        $reportObj->report_twitter_id   =   $twitter_id;
	        $reportObj->file_path			=	tweetkit_file_download_encrypt(serialize($files));
	        $reportObj->hash                =   $thash;
	        $reportObj->progress			=	0;
	        $reportObj->status				=	0; //0 - not started  1 - in progress  - 2 completed - 3 failed
	        $reportObj->is_active			=	1;
	        $reportObj->is_deleted			=	0;
	        $reportObj->save();
	        
	        
	        $job	=	(new MPTTweets($loggedin_twitter_id,$twitter_id,$reportObj->_id))->onQueue('mpt_user_tweets');
	        dispatch($job);
	        
	        return $this->response->withArray(array());
	    }catch (\Exception $e){
	        return $this->response->errorUnauthorized();
	    }
	}
	
	public function user_tweets($twitter_id,$target_twitter_id,$page,$filter=1){
	    if(!is_null($twitter_id) && !is_null($target_twitter_id) && !is_null($page)){
		    $user	                        =	MobileUsers::where('twitter_id',$twitter_id)->first();
		    $count                          =   ($user['is_premium']) ? 10 : 5;
		    
		    $page							=	(int)$page;
		    $filter							=	(int)$filter;
		    	    
		    $user_tweets				    =	 array();
		    
		    $ut_key                         =    md5('t_'.$twitter_id.'_tt_'.$target_twitter_id.'_p_'.$page.'_f_'.$filter);
		    $user_tweets                    =    Cache::get($ut_key);
		    
		    if(is_null($user_tweets)){
    			if($filter == 1){ 							//show latest tweets
    			    if($page != 0){
    					$user_tweets		    =	\DB::connection('mongodbut')->collection($target_twitter_id)->orderBy('tweet_time','DESC')->skip($count*$page)->limit($count)->get();
    				}else{
    					$this->_create_target_account($target_twitter_id);
    					
    					$twitterObj			    =	new Twitter;
    					$user_tweets	 	    =	$twitterObj->download_200_user_tweets($user,$target_twitter_id,$count,1);
    				}
    			}else{			
        			$job_exists                  =   true;
        			do{
        			    $c_job_exists            =   UserTweetsQueue::find($target_twitter_id);
        			    if(is_null($c_job_exists)){
        			        $job_exists = false;
        			    }else{
        			        sleep(2);
        			    }
        			}while($job_exists);
    			}
    
    			if($filter == 3){ //sort based on number of retweets
    			    $user_tweets			=	\DB::connection('mongodbut')->collection($target_twitter_id)->where('is_retweet',"No")->orderBy('tweet_retweet_count','DESC')->skip($count*$page)->limit($count)->get();
    			}else if($filter == 2){ //sort based on number of likes
    				$user_tweets			=	\DB::connection('mongodbut')->collection($target_twitter_id)->where('is_retweet',"No")->orderBy('tweet_favorite_count','DESC')->skip($count*$page)->limit($count)->get();
    			}
    			
    			if(sizeof($user_tweets) > 0){
    			     @Cache::put($ut_key, $user_tweets,5);
    			}
		    }
			
			$tweets_count               =   \DB::connection('mongodbut')->collection($target_twitter_id)->count();
			$tweets_count               =   ($tweets_count == 0) ? 3200 : $tweets_count;
			
			if(sizeof($user_tweets) > 0){
				$ut_resp				=	$this->_create_user_tweet_Obj($user_tweets,$twitter_id,$target_twitter_id);
				$next_page              =   $page+1;
				if($next_page >= 10){
				    $next_page          =   0;
				}
				$ut_resp['next_page']	=	($user['is_premium']) ? $next_page : 0;
				$ut_resp['tweets_count']=   $tweets_count;
				return $this->response->withArray($ut_resp);
			}else{
				return $this->response->withArray(array('tweets'=>array(),'next_page'=>0,'tweets_count'=>$tweets_count));
			}
		}else{
			return $this->response->errorWrongArgs();
		}
	}
		
	public function get_user_details($loggedin_twitter_id,$twitter_id){
		if(!is_null($twitter_id) && !is_null($loggedin_twitter_id)){
			$key                                    =   md5('user_details_'.$twitter_id);
            $user_detail                            =   Cache::get($key);
			
            if(is_null($user_detail)){
                $muser	     	                    =	MobileUsers::where('twitter_id',$loggedin_twitter_id)->first();
                $this->_push_to_queue_ut($muser,$twitter_id);
                
                $twitterusersObj	                =	TwitterUsers::find($twitter_id);
    			
    			if(is_null($twitterusersObj)){
    				$twitterObj	                    =	new Twitter();

    				$user_details	                =	$twitterObj->get_user_details('',$twitter_id,$muser,1);
                   
    				if(is_array($user_details['userinfo']) && count($user_details['userinfo']) == 0){
    				    $user_detail                    =   array();
    				}else{
    					$user						=	$user_details['userinfo'];
    					$tu							=	new TwitterUsers;
    					$tu->_id					=	$user->id_str;
    					$tu->name					=	$user->name;
    					$tu->screen_name			=	strtolower($user->screen_name);
    					if(isset($user->description)){
    						$tu->description		=	$user->description;
    					}else{
    						$tu->description		=	'';
    					}
    					
    					/*
    					if(isset($user->time_zone)){
    						$tu->timezone			=	$user->time_zone;
    					}else{
    						$tu->timezone			=	'';
    					}
    					*/
    					
    					if(isset($user->profile_image_url_https)){
    						$tu->profile_image_url_https	=	str_replace('_normal','_bigger',$user->profile_image_url_https);
    					}else{
    						$tu->profile_image_url_https	=	'';
    					}
    					if(isset($user->profile_banner_url)){
    						$tu->profile_banner_url =	$user->profile_banner_url;
    					}else{
    						$tu->profile_banner_url	=	'';
    					}
    					if(isset($user->created_at)){		//"Thu Dec 30 04:28:34 +0000 2010"
    						$tu->created_at			=	date('Y-m-d H:i:s',strtotime($user->created_at));
    					}else{
    						$tu->created_at			=	'';
    					}
    					if(isset($user->verified)){
    						$tu->verified			=	$user->verified;
    					}else{
    						$tu->verified			=	false;
    					}
    					if(isset($user->location)){
    						$tu->location			=	trim($user->location);
    					}else{
    						$tu->location			=	'';
    					}
    					if(isset($user->friends_count)){
    						$tu->friends_count		=	$user->friends_count;
    					}else{
    						$tu->friends_count		=	0;
    					}
    					if(isset($user->followers_count)){
    						$tu->followers_count	=	$user->followers_count;
    					}else{
    						$tu->followers_count	=	0;
    					}
    					
    					/*
    					if(isset($user->favourites_count)){
    						$tu->favorites_count	=	$user->favourites_count;
    					}else{
    						$tu->favorites_count	=	0;
    					}
                        
                        
    					if(isset($user->listed_count)){
    						$tu->listed_count		=	$user->listed_count;
    					}else{
    						$tu->listed_count		=	0;
    					}
    					*/
    					
    					if(isset($user->statuses_count)){
    						$tu->statuses_count		=	$user->statuses_count;
    					}else{	
    						$tu->statuses_count		=	0;
    					}
    					/*
    					if(isset($user->lang))
    						$tu->lang				=	$user->lang;
    					else
    						$tu->lang			=	'';
    					*/
    					if(isset($user->protected))
    						$tu->protected			=	$user->protected;
    					else
    						$tu->protected			=	false;
    									
    					if(isset($user->url))
    						$tu->url				=	$user->url;
    					else
    						$tu->url				=	'';
    					
    				    try {
        					if($tu->save()){
        					    $user_detail				=	$tu->toArray();
        					    $user_detail['id_str']		=	$user_detail['_id'];
        					    $user_detail['created_at']  =	date("D M j G:i:s O Y",strtotime($user_detail['created_at']));
    
        					    unset($user_detail['_id']);
        					    unset($user_detail['updated_at']);
    
        						@Cache::put($key, $user_detail,10);
        					}else{
        					    $user_detail                =   array();
        					}
    				    }
    					catch(\Exception $e){
    					    Log::info($e->getMessage());
    					}
    				}
    			}else{
    			    $user_detail				        =	$twitterusersObj->toArray();
    			    $user_detail['id_str']		        =	$user_detail['_id'];
    			    $user_detail['created_at']	        =	date("D M j G:i:s O Y",strtotime($user_detail['created_at']));
    				
    			    unset($user_detail['updated_at']);
    			    unset($user_detail['_id']);
    				
    				@Cache::put($key, $user_detail,10);
    			}
            }
            return $this->response->withArray($user_detail);
			
		}else{
			return $this->response->errorWrongArgs();
		}		
	}	
}