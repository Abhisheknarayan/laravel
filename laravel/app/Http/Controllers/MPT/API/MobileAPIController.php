<?php
namespace App\Http\Controllers\MPT\API;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Helpers\Twitter;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Modal\MPT\MobileUsers;
use Cache;

Class MobileAPIController extends ApiGuardController {      
	
	function keys(){
		return $this->response->withArray(array('client_id'=>config('services.twitter_mpt.client_id'),'client_secret'	=>	config('services.twitter_mpt.client_secret')));
	}
	
	// 5 mins caching
	function valid_user($twitter_id){
	    if(!is_null($twitter_id)){
    	   $key                        =   md5('validate_user_'.$twitter_id);
    	   $app_status                 =   Cache::get($key);
    	   if(is_null($app_status)){
        	   $mobileuser             =   MobileUsers::where('twitter_id',$twitter_id)->first();	     
        	   if(!is_null($mobileuser)){
                   $oauth_token        =   $mobileuser->oauth_token;
                   $oauth_token_secret =   $mobileuser->oauth_token_secret;
                   $client_id          =   config('services.twitter_mpt.client_id');
                   $client_secret      =   config('services.twitter_mpt.client_secret');
            	   
                   $twitterObj	       =	new Twitter;
                   $connection		   =	new TwitterOAuth($client_id,$client_secret,$oauth_token,$oauth_token_secret);
                   $app_status		   =	$twitterObj->validate_token($connection);
                   if($app_status){
                       @Cache::put($key, $app_status,5);
                   }
               }else{
                   $app_status         =    0;
               }
    	   }
    	   return $this->response->withArray(array('status'=>$app_status));
	    }else{
	        return $this->response->errorWrongArgs();
	    }
	}
}