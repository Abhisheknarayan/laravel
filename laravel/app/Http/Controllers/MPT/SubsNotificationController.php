<?php
namespace App\Http\Controllers\MPT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modal\MPT\Subscriptions;
use Google\Cloud\PubSub\PubSubClient;
use Log;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class SubsNotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     //   $this->middleware('auth');
    }
    
    function subscriptions_notification(Request $request){
    	
    	//  https://developer.android.com/google/play/billing/billing_subscriptions#java
    	//	https://developer.android.com/google/play/billing/realtime_developer_notifications
    	// 	Notification Type
    	
    	//	2. An active subscription was renewed.
    	//	3. Sent for both voluntary and involuntary cancellation. For voluntary cancellation, sent when the user cancels.
    	//	4. A new subscription was purchased.

    	//  1. A subscription was recovered from account hold. // NOT POSSIBLE
    	//	5. A subscription has entered account hold (if enabled). // NOT POSSIBLE
    	//	6. A subscription has entered grace period (if enabled). // NOT POSSIBLE
    	//	7. User has reactivated their subscription from Play - Account - Subscriptions (requires opt-in for subscription restoration) // NOT POSSIBLE 
    	
    	
    	//new 
    	$req	=	'a:2:{s:7:"message";a:6:{s:4:"data";s:452:"eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNDgzMDY5ODk5NiIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6NCwicHVyY2hhc2VUb2tlbiI6Im9taGRjZmZqYW1pa2Rqa21iY2xpZ2xtYS5BTy1KMU95RUlWR0NZVHJpOXNaRFlZVTdMdGk1eHZ5cTkyRnBuUFFRQUttdlUxaXlsdW1PWHgxaXpJaXQ0bGFNaWM2QXBBTWhmeUVnd3JqOUR1bFR4MXZSVXNpdXhwUnpVYnhKM1NUemxQRUdpNDNQQ2VNR0stSSIsInN1YnNjcmlwdGlvbklkIjoiMDAxbXMifX0=";s:10:"attributes";a:0:{}s:10:"message_id";s:14:"79942716745199";s:9:"messageId";s:14:"79942716745199";s:12:"publish_time";s:24:"2018-04-27T12:04:59.061Z";s:11:"publishTime";s:24:"2018-04-27T12:04:59.061Z";}s:12:"subscription";s:66:"projects/api-5145596038817374769-158471/subscriptions/notification";}';
		
    	//cancel	
    	$da2	=	'a:2:{s:7:"message";a:6:{s:4:"data";s:452:"eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNDgzMDc0ODQyOCIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6MywicHVyY2hhc2VUb2tlbiI6Im9taGRjZmZqYW1pa2Rqa21iY2xpZ2xtYS5BTy1KMU95RUlWR0NZVHJpOXNaRFlZVTdMdGk1eHZ5cTkyRnBuUFFRQUttdlUxaXlsdW1PWHgxaXpJaXQ0bGFNaWM2QXBBTWhmeUVnd3JqOUR1bFR4MXZSVXNpdXhwUnpVYnhKM1NUemxQRUdpNDNQQ2VNR0stSSIsInN1YnNjcmlwdGlvbklkIjoiMDAxbXMifX0=";s:10:"attributes";a:0:{}s:10:"message_id";s:14:"79934416429295";s:9:"messageId";s:14:"79934416429295";s:12:"publish_time";s:24:"2018-04-27T12:05:48.495Z";s:11:"publishTime";s:24:"2018-04-27T12:05:48.495Z";}s:12:"subscription";s:66:"projects/api-5145596038817374769-158471/subscriptions/notification";}';
 		
    	//renew	
    	$da3	=	'a:2:{s:7:"message";a:6:{s:4:"data";s:452:"eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNDgzMjQwODI4NCIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6MiwicHVyY2hhc2VUb2tlbiI6ImRsYWdtZmdsa2FjZmNnZWZwaXBqZGlkcC5BTy1KMU93YTVZc0RBNGxSc0pQcjN3dmYwcHNvcUtEREtuU2sxOTk3YVZYa1UycURvVDFxNEUzalN1VTVuVXNZcmtNOXdGMlRsYUo5Y3RTWGVxaEwyTVNVeXNmQVZpT2c0SGxKNjV2RVBBbm1tUzNaaDNmc1RmTSIsInN1YnNjcmlwdGlvbklkIjoiMDAxaHMifX0=";s:10:"attributes";a:0:{}s:10:"message_id";s:14:"79991873810299";s:9:"messageId";s:14:"79991873810299";s:12:"publish_time";s:24:"2018-04-27T12:33:28.363Z";s:11:"publishTime";s:24:"2018-04-27T12:33:28.363Z";}s:12:"subscription";s:66:"projects/api-5145596038817374769-158471/subscriptions/notification";}';
    	
    	//cancel
    	$da4	=	'a:2:{s:7:"message";a:6:{s:4:"data";s:452:"eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNDgzMjY0NTQ4NSIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6MywicHVyY2hhc2VUb2tlbiI6ImRsYWdtZmdsa2FjZmNnZWZwaXBqZGlkcC5BTy1KMU93YTVZc0RBNGxSc0pQcjN3dmYwcHNvcUtEREtuU2sxOTk3YVZYa1UycURvVDFxNEUzalN1VTVuVXNZcmtNOXdGMlRsYUo5Y3RTWGVxaEwyTVNVeXNmQVZpT2c0SGxKNjV2RVBBbm1tUzNaaDNmc1RmTSIsInN1YnNjcmlwdGlvbklkIjoiMDAxaHMifX0=";s:10:"attributes";a:0:{}s:10:"message_id";s:14:"79946484158958";s:9:"messageId";s:14:"79946484158958";s:12:"publish_time";s:24:"2018-04-27T12:37:25.578Z";s:11:"publishTime";s:24:"2018-04-27T12:37:25.578Z";}s:12:"subscription";s:66:"projects/api-5145596038817374769-158471/subscriptions/notification";}';
    	
    	//print_r(unserialize($da1));
    	
 //   	$req		=	'a:2:{s:7:"message";a:6:{s:4:"data";s:452:"eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNTE3NzY1NjQ0OCIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6MywicHVyY2hhc2VUb2tlbiI6ImZraGlrcGpuY2toaGxnY2dmaWpwaWdlbC5BTy1KMU96Q3ZTUDZJMXlqSlBxMnlyZkQ2NkRWN0JiSzVUamE1MmM3dWJERnFEVGQ4SHdvNzh2R19vc1ZSekhvN0tXUnFJRGhJcTUxQ0RxMVF0VTZEWG5tU3VLRGFjSWhpTXR1UnF1NGpnajBReEZ2NFdWNVpxbyIsInN1YnNjcmlwdGlvbklkIjoiMDAxaHMifX0=";s:10:"attributes";a:0:{}s:10:"message_id";s:14:"81838870234313";s:9:"messageId";s:14:"81838870234313";s:12:"publish_time";s:24:"2018-05-01T12:27:36.590Z";s:11:"publishTime";s:24:"2018-05-01T12:27:36.590Z";}s:12:"subscription";s:66:"projects/api-5145596038817374769-158471/subscriptions/notification";}';
//    	$req		=	unserialize($req);
    	$req		=	$request->input();
    	Log::info($req);
//    	die;
    //	Log::info(serialize($req));
    	if(isset($req['message']['data'])){
    		$data				=	$req['message']['data'];
			$decode_data		=	json_decode(base64_decode($data));
			
			$notification_type	=	$decode_data->subscriptionNotification->notificationType;
			$purchase_token		=	$decode_data->subscriptionNotification->purchaseToken;
			$subscriptionId		=	$decode_data->subscriptionNotification->subscriptionId;
				
			//check purchase token in DB
			$subsObj			=	Subscriptions::where('purchase_token',$purchase_token)->first();
		//	Log::info("Purchase Token ".$purchase_token);
		//	Log::info('Subscription Id '.$subscriptionId);
			if(!is_null($subsObj)){
				if($notification_type == 3){
					//	3. Sent for both voluntary and involuntary cancellation. For voluntary cancellation, sent when the user cancels.
					//Log::info("cancel");
					Subscriptions::where('purchase_token',$purchase_token)->update(['is_cancel'=>1]);
				}else if($notification_type == 2){
					//	2. An active subscription was renewed.
					// based on subscription id
					Log::info("renew");
					//@todo
				}
			}
			//acw the packet
			$projectId			=	'most-popular-tweet';
			$pubsub				=	new PubSubClient([
										'projectId'	=>	$projectId
									]);
			$pubsub->consume($req);
			
			/*
			$key_path			=	app_path('keys').'/tweetkitapp-service-account-4d40ddc4a5d5.json';
			$gclient			=	new \Google_Client();
			$gclient->setScopes(array('https://www.googleapis.com/auth/androidpublisher'));
			$gclient->setApplicationName('TweetKit for Twitter');
			$gclient->setAuthConfig($key_path);
			putenv('GOOGLE_APPLICATION_CREDENTIALS='.$key_path);
			$gclient->useApplicationDefaultCredentials();
			
			$service 			= 	new \Google_Service_AndroidPublisher($gclient);
			$packageName 		= 	"com.tweetkit.app";
	
			$product			=	$service->purchases_subscriptions->get($packageName, $subscriptionId, $purchase_token);
			if (is_null($product)) {
				return $this->response->errorUnauthorized();
			} elseif (isset($product->error->code)) {
				return $this->response->errorUnauthorized();
			} elseif (!isset($product->orderId)) {
				return $this->response->errorUnauthorized();
			}
			var_dump($product);
			print_r($decode_data);
			*/
    	}
 //   	die;
    	
    	//new
    	//var_dump(base64_decode('eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNDgzMDY5ODk5NiIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6NCwicHVyY2hhc2VUb2tlbiI6Im9taGRjZmZqYW1pa2Rqa21iY2xpZ2xtYS5BTy1KMU95RUlWR0NZVHJpOXNaRFlZVTdMdGk1eHZ5cTkyRnBuUFFRQUttdlUxaXlsdW1PWHgxaXpJaXQ0bGFNaWM2QXBBTWhmeUVnd3JqOUR1bFR4MXZSVXNpdXhwUnpVYnhKM1NUemxQRUdpNDNQQ2VNR0stSSIsInN1YnNjcmlwdGlvbklkIjoiMDAxbXMifX0='));
    	//string(338) "{"version":"1.0","packageName":"com.tweetkit.app","eventTimeMillis":"1524830698996","subscriptionNotification":{"version":"1.0","notificationType":4,"purchaseToken":"omhdcffjamikdjkmbcliglma.AO-J1OyEIVGCYTri9sZDYYU7Lti5xvyq92FpnPQQAKmvU1iylumOXx1izIit4laMic6ApAMhfyEgwrj9DulTx1vRUsiuxpRzUbxJ3STzlPEGi43PCeMGK-I","subscriptionId":"001ms"}}"
    	
    	//cancel
    	//var_dump(base64_decode('eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNDgzMDc0ODQyOCIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6MywicHVyY2hhc2VUb2tlbiI6Im9taGRjZmZqYW1pa2Rqa21iY2xpZ2xtYS5BTy1KMU95RUlWR0NZVHJpOXNaRFlZVTdMdGk1eHZ5cTkyRnBuUFFRQUttdlUxaXlsdW1PWHgxaXpJaXQ0bGFNaWM2QXBBTWhmeUVnd3JqOUR1bFR4MXZSVXNpdXhwUnpVYnhKM1NUemxQRUdpNDNQQ2VNR0stSSIsInN1YnNjcmlwdGlvbklkIjoiMDAxbXMifX0='));
    	//string(338) "{"version":"1.0","packageName":"com.tweetkit.app","eventTimeMillis":"1524830748428","subscriptionNotification":{"version":"1.0","notificationType":3,"purchaseToken":"omhdcffjamikdjkmbcliglma.AO-J1OyEIVGCYTri9sZDYYU7Lti5xvyq92FpnPQQAKmvU1iylumOXx1izIit4laMic6ApAMhfyEgwrj9DulTx1vRUsiuxpRzUbxJ3STzlPEGi43PCeMGK-I","subscriptionId":"001ms"}}"
    	
    	
    	//renew
    	//var_dump(base64_decode('eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNDgzMjQwODI4NCIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6MiwicHVyY2hhc2VUb2tlbiI6ImRsYWdtZmdsa2FjZmNnZWZwaXBqZGlkcC5BTy1KMU93YTVZc0RBNGxSc0pQcjN3dmYwcHNvcUtEREtuU2sxOTk3YVZYa1UycURvVDFxNEUzalN1VTVuVXNZcmtNOXdGMlRsYUo5Y3RTWGVxaEwyTVNVeXNmQVZpT2c0SGxKNjV2RVBBbm1tUzNaaDNmc1RmTSIsInN1YnNjcmlwdGlvbklkIjoiMDAxaHMifX0='));
    	//string(338) "{"version":"1.0","packageName":"com.tweetkit.app","eventTimeMillis":"1524832408284","subscriptionNotification":{"version":"1.0","notificationType":2,"purchaseToken":"dlagmfglkacfcgefpipjdidp.AO-J1Owa5YsDA4lRsJPr3wvf0psoqKDDKnSk1997aVXkU2qDoT1q4E3jSuU5nUsYrkM9wF2TlaJ9ctSXeqhL2MSUysfAViOg4HlJ65vEPAnmmS3Zh3fsTfM","subscriptionId":"001hs"}}"
    
    	//cancel
//    	var_dump(base64_decode('eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20udHdlZXRraXQuYXBwIiwiZXZlbnRUaW1lTWlsbGlzIjoiMTUyNDgzMjY0NTQ4NSIsInN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvbiI6eyJ2ZXJzaW9uIjoiMS4wIiwibm90aWZpY2F0aW9uVHlwZSI6MywicHVyY2hhc2VUb2tlbiI6ImRsYWdtZmdsa2FjZmNnZWZwaXBqZGlkcC5BTy1KMU93YTVZc0RBNGxSc0pQcjN3dmYwcHNvcUtEREtuU2sxOTk3YVZYa1UycURvVDFxNEUzalN1VTVuVXNZcmtNOXdGMlRsYUo5Y3RTWGVxaEwyTVNVeXNmQVZpT2c0SGxKNjV2RVBBbm1tUzNaaDNmc1RmTSIsInN1YnNjcmlwdGlvbklkIjoiMDAxaHMifX0='));
    	//string(338) "{"version":"1.0","packageName":"com.tweetkit.app","eventTimeMillis":"1524832645485","subscriptionNotification":{"version":"1.0","notificationType":3,"purchaseToken":"dlagmfglkacfcgefpipjdidp.AO-J1Owa5YsDA4lRsJPr3wvf0psoqKDDKnSk1997aVXkU2qDoT1q4E3jSuU5nUsYrkM9wF2TlaJ9ctSXeqhL2MSUysfAViOg4HlJ65vEPAnmmS3Zh3fsTfM","subscriptionId":"001hs"}}"    	    
    }
    	
}