<?php
namespace App\Http\Controllers\MPT\PANEL;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modal\MPT\MobileUsers;

use Form;
use App\Modal\MPT\Transactions;

class MobileController extends Controller{

	public function mobileusers(Request $request)
	{
		$search 	 				= 	trim($request->input('usersearch'));
		$twitter_id					=	$request->input('twitter_id');
		$verified 	 				= 	$request->input('verified');
		$premium                    =   $request->input('premium');
		
		$mu			 				=	MobileUsers::select('_id','twitter_id','username','verified_account','login_ip','is_active','is_premium','created_at','updated_at');
		$total_mobileusers			=   MobileUsers::count();
		$total_active_mobileusers	=   MobileUsers::where('is_active',1)->count();
		$total_premium_users		=	MobileUsers::where('is_premium',1)->count();
        		
		
		if($search !='' ){
			$mu->where('username','like','%'.$search.'%');
		}
        
		if($twitter_id != ''){
		    $mu->where('twitter_id',$twitter_id);
		}
		
		if($verified!=''){
			$mu->where('verified_account',(int)$verified);
		}
		
		if($premium != ''){
		    $mu->where('is_premium',(int)$premium);
		}
		
		
		$search_count	            =	$mu->count();
		$mu				            =   $mu->orderby('created_at','desc')->paginate(50);	
		
		$data                       =   '';
		foreach ($mu as $user){
		    $order_id = '';
		    if($user->is_premium){
		        $transaction = Transactions::where('twitter_id',$user->twitter_id)->first();
		        if(!is_null($transaction)){
		          $order_id = $transaction->order_id;
		        }
		    }
		    
		    $data                  .=  "<tr id='mobileusertbl".$user->id."'>
                              		        <td><small>". $user->twitter_id ."</small></td>
		                                    <td><small>". $user->username ."<br />(".$user->login_ip .")</small></td>
		                                    <td><small>". date('d-M-y h:i:s A',strtotime($user->created_at)) ."</small></td>
		                                    <td><small>". date('d-M-y h:i:s A',strtotime($user->updated_at)) ."</small></td>
		                                    <td><small>".$order_id."</small></td>
		                                    <td><small>".Form::select('status', ['1' => 'Enable', '0' => 'Disable'],$user->is_active, array('id'=>'status_'.$user->_id,'class'=>'user_status')) ."</small></td>
		                                    <td><small>".Form::select('is_premium', ['1' => 'Enable', '0' => 'Disable'],$user->is_premium, array('id'=>'premium_'.$user->_id,'class'=>'user_premium'))."</small></td>
		                                    <td><small><input type='button' id='". $user->_id ."' class='btn btn-success savebtn' value='Save'></small></td>
		                                 </tr>";
		}
		
		return view('mpt.mobileusers',compact('mu','data','twitter_id','search','verified','total_mobileusers','total_active_mobileusers','total_premium_users','search_count'));
	}	
}