<?php
namespace App\Http\Controllers\MPT\PANEL;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Modal\MPT\MobileUsers;

class AjaxController extends Controller{
		
	public function save_mobileuser(Request $request)
	{
		if($request->ajax()){
			$id	 					=	$request->input('id');
			$status					=   (int)$request->input('status');
			$is_premium				=	(int)$request->input('is_premium');
			$mobuser				= 	MobileUsers::find($id);
			$mobuser->is_active     =   $status;
			$mobuser->is_premium	=	$is_premium;
			
			if($mobuser->save()){
				return JsonResponse::create(array('success'),'200');
			}else{
				return JsonResponse::create(array('failed'),'202');
			}
		}
		else{
			return JsonResponse::create(array(),'404');
		}
	}
}