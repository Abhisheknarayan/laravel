<?php
namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use MongoDB\Client;
use MongoDB\Database;
use Auth;
use Cache;

class MongoController extends Controller {
    
    public function view_databases(){
        if(Auth::user()->email == 'api@tweetkitapp.com'){
            $mongodclient   =   new Client('mongodb://tkamongoAdmin:123sL%25%40310ask%25%5E%2312sas4xaaa1%21ks@'.config('tweetkitapp.mongo_ip').'/');
            $databases      =   $mongodclient->listDatabases();
            
            $mongodclient   =   new Client('mongodb://tkamongoAdmin:123sL%25%40310ask%25%5E%2312sas4xaaa1%21ks@'.config('tweetkitapp.mongo_ut_ip').'/');
            $databases_     =   $mongodclient->listDatabases();
            
            return view('core.view_databases',compact('databases','databases_'));
        }else{
            return view('errors.403');
        }
    }
    
    public function count_tweet_users(){ 
        $key                =   md5('users_tweets_count');
        $i                  =   Cache::get($key);
        if(is_null($i)){
            $mongodclient   =   new Client('mongodb://tweetkiturapi_ut:L77s%40K%4011312%40SL4%25%5E3S%40%3Fdsd@'.config('tweetkitapp.mongo_ut_ip').'/',array('authSource'=>'tweetkit_ut'));
            $manager        =   $mongodclient->getManager();
            $databaseObj    =   new Database($manager, 'tweetkit_ut');
            $collections    =   $databaseObj->listCollections();
            $i              =   0;
            foreach ($collections as $c){
                $i++;
            }
            
            if($i >0){
                @Cache::put($key, $i,60);
            }
        }
        return JsonResponse::create(array('total_users'=>$i),'200');
    }
    
    public function count_twitter_users(){
        $key                =   md5('twitter_users');
        $count              =   Cache::get($key);
        if(is_null($count)){
            $mongodclient   =   new Client('mongodb://twitter_users:T77A%251%2821%25%5E3%40a4%21D%29%40%25%23%24%25@'.config('tweetkitapp.mongo_ut_ip').'/',array('authSource'=>'twitter_users'));
            $manager        =   $mongodclient->getManager();
            $collections    =   new \MongoDB\Collection($manager, 'twitter_users', 'twitterusers');
            $count          =   1;//$collections->EstimatedDocumentCount();
            if($count > 0)
             @Cache::put($key,$count,60);
        }
        return JsonResponse::create(array('total_users'=>$count),'200');
    }
}