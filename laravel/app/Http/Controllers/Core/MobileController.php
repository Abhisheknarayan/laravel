<?php
namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Modal\Core\AccessToken;
use App\Modal\Core\TargetAccounts;
use App\Modal\Core\Accounts;
use App\Helpers\Twitter;
use League\Csv\Writer;
use App\Modal\Core\TwitterUsers;
use Log;

class MobileController extends Controller{
		
	private function validate_access_token($data){
		return Validator::make($data,[
				'email'				=>	array('max:255','required'),
				'api_key'			=>	array('max:255','required'),
				'api_key_secret'	=>	array('max:500','required'),
				'access_token'		=>	array('max:500'),
				'access_token_secret'=>	array('max:500')
		],$this->add_validate_access_token_message());
	}
	
	private function add_validate_access_token_message(){
		return [
				'email.required'				=>	'Email Can\'t be empty',
				'email.max'						=>	'Invalid Email',
				'api_key.required'				=>	'Api Can\'t be empty',
				'api_key.max'					=>	'Invalid API Key',
				'api_key_secret.required'		=>	'Api Key Secret can\'t be empty',
				'api_key_secret.max'			=>	'Invalid Api Key',
				'access_token.max'				=>	'Invalid Access Token',
				'access_token_secret.max'		=>	'Invalid Access Token Secret'
		];
	}
	
	
	public function access_token(Request $request){
		$resp         =     $request->all();
		if(sizeof($resp) > 0){
			$validator 			=	$this->validate_access_token($resp);
			
			if ($validator->fails()) {
				$this->throwValidationException(
						$request, $validator
						);
			}
			
			$email 				= 	$resp['email'];
			$api_key 			= 	$resp['api_key'];
			$api_key_secret  	= 	$resp['api_key_secret'];
			$access_token 		=	$resp['access_token'];
			$access_token_secret= 	$resp['access_token_secret'];
			
			$access_token_obj 						=	new AccessToken;
			$access_token_obj->api_key				=	$api_key;
			$access_token_obj->api_secret			=	$api_key_secret;
			$access_token_obj->access_token			=	$access_token;
			$access_token_obj->access_token_secret	=	$access_token_secret;
			$access_token_obj->email				=	$email;
			$access_token_obj->app_status			=	1;
			$access_token_obj->is_active			=	1;
			if($access_token_obj->save()){
				Session::flash('add_user_success','Access Token added successfully');
			}else{
				Session::flash('add_user_failed','An error occured. Please try again');
			}
			return redirect('/core/access-tokens');
		}
		else{
			$total_access_token  			= 	AccessToken::count();
			$total_not_working_access_token	=	AccessToken::where('app_status',0)->count();
			$total_disabled_access_token	=	AccessToken::where('is_active',0)->count();
			$access_tokens 					= 	AccessToken::orderBy('app_status','desc')->get();
			return view('core.access_tokens',compact('access_tokens','total_access_token','total_not_working_access_token','total_disabled_access_token'));
		}
	}
	
	
	private function add_account_validator(array $data){
	    return Validator::make($data, [
	        'account'       =>	array('max:16','required','regex:/^[a-zA-Z0-9_]+$/'),
	    ],$this->add_account_validator_message());
	}
	
	private function add_account_validator_message(){
	    return [
	        'account.required'			=>	  	'Account is required',
	        'account.max'               =>    	'Account should be less than 16 characters',
	        'account.regex'            	=>    	'Invalid Account'
	    ];
	}
	
	public function accounts(Request $request)
	{
	    $search         = 	strtolower($request->input('q'));
	    
	    $accounts		=  	Accounts::select('twitter_id','username','last_tweet_id','is_active','is_deleted','updated_at');
	    $paccounts 		=	clone $accounts;
	    $total_count	=	$paccounts->where('is_active',1)->count();
	    
	    if($search	!= 	''){
	        $accounts->where('username','like','%'.$search.'%');
	        $accounts->orWhere('twitter_id',$search);
	    }
	    
	    $total_accounts               =     Accounts::count();
	    $accounts				      = 	$accounts->where('is_deleted',0)->orderBy('is_active','DESC')->orderby('updated_at','ASC')->paginate(config('tweetkitapp.pagination'));
	    $inactive_accounts_tracker	  =	    Accounts::where('is_active',1)->where('updated_at','<',new \DateTime('-168 hours'))->count();
	    return view('core.accounts',compact('inactive_accounts_tracker','total_accounts','accounts','search','total_count'));
	}
	
	public function add_account(Request $request){
	    
	    $resp		  	=	$request->all();
	    $this->add_account_validator($resp)->validate();
	    
	    $account 		= 	strtolower(trim($_POST['account']));
	    $is_exists		=	Accounts::where('username',$account)->count();
	    //var_dump($is_exists);die;
	    if($is_exists > 0){
	        Session::flash('add_account_failed','User already exists');
	        return redirect('/core/accounts');
	    }
	    else{
	        $twitterObj	=	new Twitter();
	        $user		=   $twitterObj->get_user_details($account);
	        if(is_array($user['userinfo']) && count($user['userinfo']) == 0){
	            Session::flash('add_user_failed','User not found');
	            return redirect('/core/accounts')->withInput();
	        }else{
	            $user 					= 	$user['userinfo'];
	            
	            $account				=	new Accounts();
	            $account->twitter_id	=	$user->id_str;
	            $account 				= 	create_user_Obj($account, $user);
	            if($account->save()){
	                
	                // Add to target Accounts
	                $target_account     =   new TargetAccounts();
	                $target_account->_id=   $user->id_str;
	                $target_account->last_tweet_id =   '';
	                $target_account->save();
	                
	                Session::flash('add_user_success','User Added Successfully');
	                return redirect('/core/accounts');
	            }else{
	                Session::flash('add_user_failed','An error occured');
	                return redirect('/core/accounts')->withInput();
	            }
	        }
	    }
	}
	
	
	public function trackuser_search(Request $request) {
		$twitter_id			=	$request->input('twitter_id');
		if(is_null($twitter_id) && isset($_GET['twitter_id'])){
			$twitter_id		=	$_GET['twitter_id'];
		}
		
		$total_accounts		=	TargetAccounts::count();
		
		$last_updated_ids	=	TargetAccounts::orderBy('updated_at','DESC')->take(25)->get();
		
		$tweets_count       =   0;
		$taccount           =   null;
		if(!is_null($twitter_id)){
			$taccount		=	TargetAccounts::find($twitter_id);
			$tweets_count	=	\DB::connection('mongodbut')->collection($twitter_id)->count();
		}
		if($tweets_count > 0 && is_null($taccount)){
		    $target_account					=	new TargetAccounts();
		    
		    $target_account->_id			=	$twitter_id;
		    $target_account->last_tweet_id	=	'';
		    try {
		        $target_account->save();
		    }catch(\Exception $e){}
		}
		
		return view('core.trackuser_search',compact('last_updated_ids','tweets_count','twitter_id','total_accounts','taccount'));
	}
	
	function user_tweets_download($twitter_id){
	    if(!is_null($twitter_id)){
	        
	        $tweets_path	=	'/tmp/'.$twitter_id.'.csv';
	        $images_path	=	'/tmp/'.$twitter_id.'-images.csv';
	        $videos_path	=	'/tmp/'.$twitter_id.'-videos.csv';
	        
	        $tweets_csv 	=	Writer::createFromPath($tweets_path,'w');
	        $images_csv		=	Writer::createFromPath($images_path,'w');
	        $videos_csv		=	Writer::createFromPath($videos_path,'w');
	        
	        
	        $alltweets		=	array();
	        
	        $tweets_csv->insertOne(array('Tweet Id','Tweet URL','Tweet Posted Time','Tweet Content','Tweet Type','Client','Retweets received','Likes received',
	            'User Id','Name','Username','Verified or Non-Verified','Profile URL','Protected or Not Protected'));
	        
	        $alltweets		=	array();
	        
	        $usertweets		=	\DB::connection('mongodbut')->collection($twitter_id)->orderBy('tweet_time','DESC')->get();
	        $usertweets		=	$usertweets->toArray();
	        
	        if(sizeof($usertweets) > 0){
	            
	            foreach ($usertweets as $tweet){
	                $user		=	TwitterUsers::find($tweet['user_id']);
	                if(!is_null($user)){
	                    if(isset($tweet['media'])){
	                        foreach ($tweet['media'] as $media){
	                            if(isset($media['id_str']) && isset($media['type']) && isset($media['media_url_https']) && isset($media['url'])){
	                                
	                                if($media['type'] == 'photo'){
	                                    $alltweets['images'][]	= 	array('tweet_id'=>'"'.$tweet['_id'].'"','id'=>'"'.$media['id_str'].'"','media_url'=>$media['media_url_https'],'url'=>$media['url']);
	                                }
	                                
	                                if($media['type'] == 'video' || $media['type'] == 'animated_gif'){
	                                    if(isset($media['video_info']['variants'][0]['url'])){
	                                        $alltweets['videos'][]	=   array('tweet_id'=>'"'.$tweet['_id'].'"','id'=>'"'.$media['id_str'].'"','media_url'=>$media['video_info']['variants'][0]['url'],'url'=>$media['url']);
	                                    }
	                                }
	                            }
	                        }
	                    }
	                    $tweet_type = 'Tweet';
	                    
	                    if($tweet['is_retweet'] == 'Yes'){
	                        $tweet_type =   'Retweet';
	                    }
	                    else if($tweet['is_reply'] == 'Yes'){
	                        $tweet_type =   'Reply';
	                    }
	                    $tweets_csv->insertOne(array('"'.$tweet['_id'].'"','https://twitter.com/'.$user->screen_name.'/status/'.$tweet['_id'],
	                        $tweet['tweet_time'],'"'.$tweet['tweet_text'].'"',$tweet_type,$tweet['tweet_source'],
	                        $tweet['tweet_retweet_count'],$tweet['tweet_favorite_count'],'"'.$tweet['user_id'].'"',
	                        '"'.$user->name.'"',$user->screen_name,($user->verified) ? "Verified":"Non-Verified",
	                        'https://twitter.com/'.$user->screen_name,($user->protected)?"Protected":"Not Protected")
	                        );
	                }else{
	                    Log::info('User Tweets not found '.$tweet['user_id']);
	                }
	            }
	        }
	        
	        
	        $media_header =	array('Tweet Id','Media Id','Media URL','Tweet URL');
	        
	        $images_csv->insertOne($media_header);
	        if(isset($alltweets['images'])){
	            foreach ($alltweets['images'] as $image){
	                $images_csv->insertOne($image);
	            }
	        }
	        
	        $videos_csv->insertOne($media_header);
	        if(isset($alltweets['videos'])){
	            foreach ($alltweets['videos'] as $video){
	                $videos_csv->insertOne($video);
	            }
	        }
	        
	        $zip_file_path 	= 	config('tweetkitapp.zip_file_dir').'/'.$twitter_id.'.zip';
	        
	        $zip = new \ZipArchive();
	        if($zip->open($zip_file_path,\ZipArchive::CREATE) === TRUE){
	            $zip->addFile($tweets_path,basename($tweets_path));
	            $zip->addFile($images_path,basename($images_path));
	            $zip->addFile($videos_path,basename($videos_path));
	        }
	        $zip->close();
	        
	        header('Content-Description: Report Download');
	        header("Content-type: application/force-download");
	        header("Content-type: application/zip");
	        header('Content-Disposition: attachment; filename='.basename($zip_file_path));
	        header("Content-Transfer-Encoding: Binary");
	        header('Expires: 0');
	        header('Cache-Control: must-revalidate');
	        header('Pragma: public');
	        header('Content-Length: ' . filesize($zip_file_path));
	        $content =  readfile($zip_file_path);
	        unlink($zip_file_path);
	        return $content;
	    }else{
	        abort(404);
	    }
	}
	
}