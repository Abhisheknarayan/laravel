<?php
namespace App\Http\Controllers\FollowersAnalysis;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Validator;
use App\Modal\FA\Coupons;
use App\Modal\FA\FAUser;
use App\Http\Controllers\Controller;

class FAWebsiteAdminController extends Controller
{	
    private function add_coupon_validator(array $data){
        return Validator::make($data, [
            'coupon_code'       =>  array('max:20','required','regex:/^[a-zA-Z\-0-9_]+$/'),
            'coupon_amount'     =>	array('max:5','required','regex:/^[0-9.]+$/'),
            'date'		 	 	=>  array('required'),
            'coupon_type'		=>	array('required'),
            'coupon_products'	=> 	array('required'),
            'coupon_per_user'	=>	array('max:5','required','regex:/^[0-9]+$/'),
            'c_max_product'		=>	array('max:5','required','regex:/^[0-9]+$/')
        ],$this->add_coupon_validator_message());
    }
    
    private function add_coupon_validator_message(){
        return [
            'coupon_code.required'		=>	'Coupon code is required',
            'coupon_code.max'   		=>  'Code should be less than 20 characters',
            'coupon_code.regex'     	=>  'Invalid Code Name',
            
            'coupon_amount.required'	=>	'Coupon Amount is required',
            'coupon_amount.max'   		=>  'Code should be less than 5 digits',
            'coupon_amount.regex'   	=>  'Invalid Coupon Amount. Dot and Number Allowed',
            
            'coupon_per_user.required'	=>	'Coupon Per User is required',
            'coupon_per_user.regex'		=>	'Invalid Coupon Per User. Only numeric value allow',
            'coupon_type.required'		=>	'Coupon type is required',
            'coupon_products.required'	=>	'Please select product',
            'c_max_product.required'	=>	'Please enter max product in cart',
            'date.required'				=>	'Expiry date is required',
        ];
    }
    
	public function coupons(Request $request)
	{
		$coupons		=	Coupons::select('id','coupon_code','discount_type','amount','users','products','max_products_in_cart','per_user_coupon','expired','updated_at');
		$search 		=   $request->input('q');
		$total_coupons	=   $coupons->count();
		
		if($search!= ''){
			$coupons->where('coupon_code','like','%'.$search.'%');
			$coupons->orWhere('users','like','%'.$search.'%');
		}
		$search_count	=	$coupons->count();
		$coupons		=	$coupons->orderBy('updated_at','desc')->paginate(config('etpanel.pagination'));
		
		
		return view('followersanalysis.coupons',compact('coupons','search','total_coupons','search_count'));
	}
	
	public function add_coupons(Request $request)
	{
		$resp		  		=	$request->all();
		$id	  		  		=	$resp['hidden_id'];
		$coupon_code	 	=	trim($resp['coupon_code']);
		$coupon_type	 	=	trim($resp['coupon_type']);
		$coupon_amount	 	=	trim($resp['coupon_amount']);
		$coupon_users	 	=	trim($resp['coupon_users']);
		$coupon_per_user 	=	trim($resp['coupon_per_user']);
		$c_max_products		=	trim($resp['c_max_product']);
		$coupon_products	=	isset($resp['coupon_products']) ? $resp['coupon_products']:array();
		$date		  		=	$resp['date'];
		
		
		if($id > 0){
			
			$coupon					=	Coupons::find($id);
			if($coupon_code != $coupon->coupon_code){
				$is_exists = Coupons::where('coupon_code',$coupon_code)->count();
				if($is_exists > 0){
					Session::flash('add_coupon_failed','Coupon code already exists');
					return redirect('/fa/coupons')->withInput();
				}
			}
			
			$coupon->coupon_code			=	$coupon_code;
			$coupon->discount_type			=	$coupon_type;
			$coupon->amount					=	$coupon_amount;
			$coupon->users					=	$coupon_users;
			$coupon->per_user_coupon		=	$coupon_per_user;
			$coupon->products				=	implode(',',$coupon_products);
			$coupon->max_products_in_cart	=	$c_max_products;
			
			$old_date			=   $coupon->expired;
			
			if($old_date != $date)
				$coupon->expired	=   $date.' 23:59:59';
				
				$validator	  		=	$this->add_coupon_validator($resp);
				
				if ($validator->fails()) {
				    return redirect('/fa/coupons')->withErrors($validator)->withInput();
				}
				else{
					
					if($coupon->save()){
						Session::flash('update_coupon_success','Coupon Updated Successfully');
						return redirect('/fa/coupons');
					}
					else{
						Session::flash('update_coupon_failed','An error occured');
						return redirect('/fa/coupons')->withInput();
					}
				}
		}
		else{
			
			$validator    =     $this->add_coupon_validator($resp);
			
			if ($validator->fails()) {
			    return redirect('/fa/coupons')->withErrors($validator)->withInput();
			}
			else
			{
				$is_exists = Coupons::where('coupon_code',$coupon_code)->count();
				if($is_exists > 0){
					Session::flash('add_coupon_failed','Coupon code already exists');
					return redirect('/fa/coupons')->withInput();
				}else{
					
					
					$coupon							=	new Coupons;
					$coupon->coupon_code			=	$coupon_code;
					$coupon->discount_type			=	$coupon_type;
					$coupon->amount					=	$coupon_amount;
					$coupon->users					=	$coupon_users;
					$coupon->per_user_coupon		=	$coupon_per_user;
					$coupon->products				=	implode(',',$coupon_products);
					$coupon->max_products_in_cart	=	$c_max_products;
					$coupon->expired				=   $date.' 23:59:59';
					
					if($coupon->save()){
						Session::flash('add_coupon_success','Coupon Added Successfully');
						return redirect('/fa/coupons');
					}
					else{
						Session::flash('add_coupon_failed','An error occured');
						return redirect('/fa/coupons')->withInput();
					}
				}
			}
		}
	}
	
	public function user(Request $request){
		$pg_val     	=   25;
		$status   		=	$request->input('status');
		$twitter_id		=	$request->input('twitter-id');
		$username		=	$request->input('username');
		$email			=   $request->input('email');
		$users 			=   new FAUser();
		$total_count	=   $users::count();
		
		if($status!=''){
			$users 		= 	$users->where('is_active',$status);
		}
		
		if($twitter_id!=''){
			$users  	=   $users->where('twitter_id',$twitter_id);
		}
		if($username!=''){
			$users  	=   $users->where('username',$username);
		}
		if($email!=''){
			$users  	=   $users->where('twitter_email',$email);
		}
		
		$search_count	=   $users->count();
		$users 			=   $users->orderBy('last_login', 'desc')->paginate($pg_val);
		
		$not_active_account = FAUser::where('is_active',0)->count();
		
		return view('followersanalysis.users',compact('not_active_account','users','status','twitter_id','username','email','total_count','search_count'));
	}

	public function transactions(Request $request){
		$pg_val     		=   25;
		$user_trans 		= 	FAUser::join('transactions','users.twitter_id','=','transactions.twitter_id')->select('transactions.id','users.twitter_id','users.username','transactions.first_name','transactions.last_name','transactions.payer_email','transactions.transaction_state','transactions.transaction_date')->where('transactions.twitter_id','!=','51468247')->where('transactions.twitter_id','!=','1107566096')->where('transactions.payer_email','!=','arjunjain08@gmail.com')->where('transactions.payer_email','!=','contact@followersanalysis.com');
		$total_count		=   $user_trans->count();
		$username			=	$request->input('username');
		$twitter_id			=	$request->input('twitter_id');
		
		if($username != ''){
			$user_trans 	=   $user_trans->where('users.username',$username);
		}
		
		if($twitter_id != ''){
			$user_trans 	=   $user_trans->where('users.twitter_id',$twitter_id);
		}
		
		$user_trans_count	=	$user_trans->count();
		
		$user_trans 		=	$user_trans->orderBy('transactions.transaction_date','desc')->paginate($pg_val);
		
		return view('followersanalysis.transactions',compact('user_trans','username','user_trans_count','twitter_id','total_count'));
	}
}