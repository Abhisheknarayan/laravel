<?php
namespace App\Http\Controllers\FollowersAnalysis;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Modal\FA\Coupons;
use App\Modal\FA\FAUser;
use App\Http\Controllers\Controller;

class FAAjaxController extends Controller{
	
	public function delete_coupons(Request $request)
	{
		if($request->ajax()){
			$id						=   $request->input('id');
			$coupon_del				=	Coupons::findorFail($id);
			
			if($coupon_del->delete()){
				return JsonResponse::create(array('success'),'200');
			}else{
				return JsonResponse::create(array('failed'),'202');
			}
		}
		else{
			return JsonResponse::create(array(),'404');
		}
	}
	
	public function save_user(Request $request){
		if($request->ajax()){
			$id						=	$request->input('id');
			$status					=	$request->input('status');
			
			$users					=	FAUser::find($id);
			$users->is_active		=	$status;
			
			if($users->save()){
				return JsonResponse::create(array('success'),'200');
			}else{
				return JsonResponse::create(array('failed'),'202');
			}
		}
		else{
			return JsonResponse::create(array(),'404');
		}
	}	
}