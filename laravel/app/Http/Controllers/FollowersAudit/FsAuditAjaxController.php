<?php
namespace App\Http\Controllers\FollowersAudit;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Modal\FollowersAudit\FSAuditUser;
use App\Modal\FollowersAudit\FSAuditUserLimit;
use App\Http\Controllers\Controller;

class FsAuditAjaxController extends Controller {
	
	public function save_user(Request $request){

		if($request->ajax()){
			$id						=	$request->input('id');
			$status					=	$request->input('status');
			
			$users					=	FSAuditUser::find($id);
			$users->is_active		=	$status;
			
			if($users->save()){
				return JsonResponse::create(array('success'),'200');
			}else{
				return JsonResponse::create(array('failed'),'202');
			}
		}
		else{
			return JsonResponse::create(array(),'404');
		}
		
	}

	public function saveUserLimit(Request $request){

		if($request->ajax()){


			$updateData = array(
				'audits' 	 	=>	$request->audits,
				'audits_fo' 	=>	$request->audits_fo,
				'profiles' 		=> 	$request->profiles,
				'profiles_fo' 	=>	$request->profiles_fo,
				'rows' 			=>	$request->rows,
				'plan_id' 		=>	$request->plan,
			);

			if(FSAuditUserLimit::where(['user_id' => $request->user_id])->update($updateData)){
				return JsonResponse::create(array('success'),'200');
			}else{
				return JsonResponse::create(array('failed'),'202');
			}

		}
		else{
			return JsonResponse::create(array(),'404');
		}
		
	}	


}