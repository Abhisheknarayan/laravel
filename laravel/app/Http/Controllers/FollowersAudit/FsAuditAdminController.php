<?php
namespace App\Http\Controllers\FollowersAudit;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Validator;
use App\Modal\FollowersAudit\FSAuditUser;
use App\Modal\FollowersAudit\FSAuditSubscriptions;
use App\Modal\FollowersAudit\FSAuditUserLimit;
use App\Modal\FollowersAudit\FSAuditProfiles;
use App\Modal\FollowersAudit\FSAuditTracker;
use App\Http\Controllers\Controller;

class FsAuditAdminController extends Controller
{	
	
	public function subscription(Request $request){

		$pg_val     	=   25;

		$email			=   $request->input('email');

		$user_id		=   $request->input('user_id');

		$status	 		=   $request->input('status');

		$subscriptions	=   new FSAuditSubscriptions();

		$total_count	=   $subscriptions::count();
		
		$subscriptions  = 	$subscriptions->join('users','users.id','=','subscriptions.user_id');
		
		if($email!=''){
			$subscriptions  =   $subscriptions->where('users.email','like','%'.$email.'%');
		}

		if($user_id!=''){
			$subscriptions  =   $subscriptions->where('users.id',$user_id);	
		}

		if($status != ''){

			if($status == '-1'){
				$subscriptions  =   $subscriptions->where('subscriptions.stripe_status',null);
			}
			else{
				$subscriptions  =   $subscriptions->where('subscriptions.stripe_status',$status);
			}
				
		}
		
		$search_count		=   $subscriptions->count();

		$subscriptions 		=   $subscriptions->orderBy('subscriptions.created_at', 'desc')->paginate($pg_val);
		
		return view('followersaudit.subscription',compact('subscriptions','email','total_count','search_count','user_id','status'));

	}

	public function user(Request $request){

		$pg_val     	=   25;
		$status   		=	$request->input('status');
		$twitter_id		=	$request->input('twitter-id');
		$username		=	$request->input('username');
		$email			=   $request->input('email');
		$users 			=   new FSAuditUser();
		$total_count	=   $users::count();
		
		if($status!=''){
			$users 		= 	$users->where('is_active',$status);
		}
		
		if($twitter_id!=''){
			$users  	=   $users->where('twitter_id','like','%'.$twitter_id.'%');
		}
		if($username!=''){
			$users  	=   $users->where('username','like','%'.$username.'%');
		}
		if($email!=''){
			$users  	=   $users->where('email','like','%'.$email.'%');
		}
		
		$search_count	=   $users->count();

		$users 			=   $users->orderBy('created_at', 'desc')->paginate($pg_val);
		
		$not_active_account = FSAuditUser::where('is_active',0)->count();
		
		return view('followersaudit.users',compact('not_active_account','users','status','twitter_id','username','email','total_count','search_count'));

	}

	public function userLimit(Request $request){


		$pg_val     	=   25;

		$twitter_id		=	$request->input('twitter-id');

		$user_id		=	$request->input('user-id');

		$userLimitObj   = 	new FSAuditUserLimit();

		$userLimitObj   = 	$userLimitObj->join('users','users.id','=','subs_limit.user_id');

		$total_count	=   $userLimitObj->count();

		if($twitter_id!=''){
			$userLimitObj  	=   $userLimitObj->where('users.twitter_id','like','%'.$twitter_id.'%');
		}

		if($user_id!=''){
			$userLimitObj  	=   $userLimitObj->where('users.id','like','%'.$user_id.'%');
		}
		
		$search_count	=   $userLimitObj->count();

		$userLimitObj 	=   $userLimitObj->orderBy('subs_limit.created_at', 'desc')->paginate($pg_val);

		return view('followersaudit.user-limit',compact('userLimitObj','twitter_id','total_count','search_count','user_id'));

	}

	public function trackerProfiles(Request $request){

		$users 			= 	[];

		$tracker 		= 	[];

		$pg_val     	=   25;

		$keyword		=	$request->input('keyword');

		$user_id		=	$request->input('user_id');

		$profilesObj    = 	new FSAuditProfiles();

		$profilesObj 	= 	$profilesObj->where(['is_archive'=>0,'is_delete'=>0]);

		$total_count	=   $profilesObj->count();

		if($keyword!=''){
			$profilesObj  	=   $profilesObj->where('search_keyword','like','%'.$keyword.'%');
		}

		if($user_id!=''){
			$profilesObj  	=   $profilesObj->where('user_id',(int)$user_id);
		}

		$search_count	=   $profilesObj->count();

		$profilesObj 	=   $profilesObj->orderBy('created_at', 'desc')->paginate($pg_val);

		foreach ($profilesObj as $key => $value){

			$users[$value->user_id] 			= FSAuditUser::find($value->user_id);

			$track_stats                        = New FSAuditTracker("track_stats_".$value->search_user_id);

			$tracker[$value->search_user_id] 	= $track_stats->orderBy('created_at', 'desc')->first();

		}

		return view('followersaudit.tracker-profiles',compact('profilesObj','total_count','search_count','keyword','users','tracker','user_id'));

	}


}