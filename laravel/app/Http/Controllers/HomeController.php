<?php
namespace App\Http\Controllers;

use App\Modal\Core\AccessToken;

use App\Modal\Core\UserTweetsQueue;
use App\Modal\MPT\MobileUsers;
use App\Modal\Core\Accounts;
use App\Modal\Core\TargetAccounts;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     */
    public function index()
    {
    	return view('adminlte::home');
    }
    
    public function dashboard(){
        
    	$access_token				=	AccessToken::where('is_active',1)->count();
    	$mobile_users				=	MobileUsers::where('is_active',1)->count();
    	$user_tweets_not_index      =   TargetAccounts::where('is_index',0)->count();
    	$inactive_accounts_tracker	=	Accounts::where('is_active',1)->where('updated_at','<',new \DateTime('-12 hours'))->count();
		$jobs_in_queue				=	UserTweetsQueue::count();
		return view('dashboard',compact('user_tweets_not_index','inactive_accounts_tracker','jobs_in_queue','access_token','mobile_users'));
    }
}