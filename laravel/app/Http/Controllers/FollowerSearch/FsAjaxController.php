<?php

namespace App\Http\Controllers\FollowerSearch;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Modal\FollowerSearch\FsUser;
use App\Modal\FollowerSearch\FsUserLimit;
use App\Http\Controllers\Controller;

class FsAjaxController extends Controller {
	
	public function save_user(Request $request){

		if($request->ajax()){
			$id						=	$request->input('id');
			$status					=	$request->input('status');
			
			$users					=	FsUser::find($id);
			$users->is_block		=	$status;
			
			if($users->save()){
				return JsonResponse::create(array('success'),'200');
			}else{
				return JsonResponse::create(array('failed'),'202');
			}
		}
		else{
			return JsonResponse::create(array(),'404');
		}
		
	}

	public function saveUserLimit(Request $request){

		if($request->ajax()){


			$updateData = array(
				
				'plan_id'      	=>  $request->plan_id,
				'b_srch_p_d'	=>	$request->b_srch_p_d,
				'b_rs_p_srch'	=>	$request->b_rs_p_srch,
				'pr_trk_srt'	=>	$request->pr_trk_srt,
				'cmt_trk_srt'	=>	$request->cmt_trk_srt,
				'trk_srt_upto'	=>	$request->trk_srt_upto,
				'analyze_prf'	=>	$request->analyze_prf,
				'analyze_upto'	=>	$request->analyze_upto,
				'cmpr_upto'		=>	$request->cmpr_upto,
				'cmpr_pr_d'		=>	$request->cmpr_pr_d,
				'dwnld_pr_d'	=>	$request->dwnld_pr_d,
				'dwnld_row'		=>	$request->dwnld_row,

			);

			if(FsUserLimit::where(['user_id' => $request->user_id])->update($updateData)){
				return JsonResponse::create(array('success'),'200');
			}else{
				return JsonResponse::create(array('failed'),'202');
			}

		}
		else{
			return JsonResponse::create(array(),'404');
		}
		
	}	


}