<?php
namespace App\Http\Controllers\FollowerSearch;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Validator;
use App\Modal\FollowerSearch\FsUser;
use App\Modal\FollowerSearch\FsSubscriptions;
use App\Modal\FollowerSearch\FsUserLimit;
use App\Modal\FollowerSearch\FsCompare;
use App\Modal\FollowerSearch\FsTracker;
use App\Modal\FollowerSearch\FsProfile;
use App\Modal\FollowerSearch\FsAccountAnalysis;
use App\Http\Controllers\Controller;

class FsAdminController extends Controller
{	
	
	public function subscription(Request $request){

		$pg_val     	=   25;

		$email			=   $request->input('email');

		$subscriptions	=   new FsSubscriptions();

		$total_count	=   $subscriptions::count();
		
		$subscriptions  = 	$subscriptions->join('users','users.id','=','subscriptions.user_id');
		
		if($email!=''){
			$subscriptions  =   $subscriptions->where('users.email','like','%'.$email.'%');
		}
		
		$search_count		=   $subscriptions->count();

		$subscriptions 		=   $subscriptions->orderBy('subscriptions.created_at', 'desc')->paginate($pg_val);
		
		return view('followersearch.subscription',compact('subscriptions','email','total_count','search_count'));

	}

	public function user(Request $request){

		$pg_val     	=   25;
		$status   		=	$request->input('status');
		$stripe_id		=	$request->input('stripe_id');
		$email			=   $request->input('email');
		$users 			=   new FsUser();
		$total_count	=   $users::count();
		
		if($status!=''){
			$users 		= 	$users->where('is_block',$status);
		}
		
		if($stripe_id!=''){
			$users  	=   $users->where('stripe_id','like','%'.$stripe_id.'%');
		}

		
		if($email!=''){
			$users  	=   $users->where('email','like','%'.$email.'%');
		}
		
		$search_count	=   $users->count();

		$users 			=   $users->orderBy('created_at', 'desc')->paginate($pg_val);
		
		$not_active_account = FsUser::where('is_block','1')->count();
		
		return view('followersearch.users',compact('not_active_account','users','status','stripe_id','email','total_count','search_count'));

	}

	public function userLimit(Request $request){


		$pg_val     	=   25;

		$email_id		=	$request->input('email-id');

		$userLimitObj   = 	new FsUserLimit();

		$userLimitObj   = 	$userLimitObj->join('users','users.id','=','user_access_limit.user_id');

		$total_count	=   $userLimitObj->count();

		if($email_id != ''){
			$userLimitObj  	=   $userLimitObj->where('users.email','like','%'.$email_id.'%');
		}
		
		$search_count	=   $userLimitObj->count();

		$userLimitObj 	=   $userLimitObj->orderBy('user_access_limit.created_at', 'desc')->paginate($pg_val);

		return view('followersearch.user-limit',compact('userLimitObj','email_id','total_count','search_count'));

	}

	public function trackingReport(Request $request){

		$users 			= 	[];

		$tracker 		= 	[];

		$pg_val     	=   25;

		$keyword		=	$request->input('profile');

		$profilesObj    = 	new FsProfile();

		$profilesObj 	= 	$profilesObj->where(['is_archive'=>0,'is_deleted'=>0]);

		$total_count	=   $profilesObj->count();

		if($keyword!=''){
			$profilesObj  	=   $profilesObj->where('t_user_n','like','%'.$keyword.'%');
		}

		$search_count	=   $profilesObj->count();

		$profilesObj 	=   $profilesObj->orderBy('created_at', 'desc')->paginate($pg_val);

		foreach ($profilesObj as $key => $value){

			$users[$value->user] 				= FsUser::find($value->user);

			$track_stats                    	= New FsTracker("fs_user_stats_".$value->user."_".$value->t_user);

			$tracker[$value->t_user] 	= $track_stats->orderBy('created_at', 'desc')->first();

		}

		return view('followersearch.tracker-profiles',compact('profilesObj','total_count','search_count','keyword','users','tracker'));

	}

	public function compareReport(Request $request){

		$users 			= 	[];

		$tracker 		= 	[];

		$pg_val     	=   25;

		$keyword		=	ltrim($request->input('profile'),'@');

		$profilesObj    = 	new FsCompare();

		$profilesObj 	= 	$profilesObj->where(['status'=>'1']);

		$total_count	=   $profilesObj->count();

		if($keyword!=''){
			$profilesObj  	=   $profilesObj->where('t_users','like','%'.$keyword.'%');
		}

		$search_count	=   $profilesObj->count();

		$profilesObj 	=   $profilesObj->orderBy('created_at', 'desc')->paginate($pg_val);

		foreach($profilesObj as $key => $value){

			$users[$value->user] 				= FsUser::find($value->user);

		}

		return view('followersearch.compare-profiles',compact('profilesObj','total_count','search_count','keyword','users'));

	}

	public function accountAnalysisReport(Request $request){

		$users 			= 	[];

		$tracker 		= 	[];

		$pg_val     	=   25;

		$keyword		=	ltrim($request->input('profile'),'@');

		$profilesObj    = 	new FsAccountAnalysis();

		$profilesObj 	= 	$profilesObj->where(['status'=>'1']);

		$total_count	=   $profilesObj->count();

		if($keyword!=''){
			$profilesObj  	=   $profilesObj->where('t_users','like','%'.$keyword.'%');
		}

		$search_count	=   $profilesObj->count();

		$profilesObj 	=   $profilesObj->orderBy('created_at', 'desc')->paginate($pg_val);

		foreach($profilesObj as $key => $value){

			$users[$value->user] 				= FsUser::find($value->user);

		}

		return view('followersearch.account-analysis',compact('profilesObj','total_count','search_count','keyword','users'));

	}

}