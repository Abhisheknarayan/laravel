<?php
namespace App\Modal\FA;
use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
	protected $fillable = ['coupon_code','discount_type','amount','users','products','max_products_in_cart','per_user_coupon','expired'];
	protected $table = 'coupons';
	protected $guarded = array('id'); //prevents people from changing the value
    protected $connection = 'mysql1';
}
