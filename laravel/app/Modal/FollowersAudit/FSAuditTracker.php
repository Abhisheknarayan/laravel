<?php

namespace App\Modal\FollowersAudit;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FSAuditTracker extends Eloquent
{

    protected $connection = 'twitteraudittrack';

    protected $collection;

    function __construct($collection=null){

    	$this->collection = $collection;
    	
    }


}