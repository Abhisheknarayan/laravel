<?php

namespace App\Modal\FollowersAudit;

use Illuminate\Database\Eloquent\Model;

class FSAuditUserLimit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $connection = 'mysql2';
    protected $table      = 'subs_limit';
}
