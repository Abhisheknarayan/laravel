<?php
namespace App\Modal\TrackMyHashtag;
use Illuminate\Database\Eloquent\Model;

class TMHHistoricalTrackers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected 	$fillable 	= 	['user_id','keyword','collection','view_type','token','total_tweets','first_tweet_time','last_tweet_time'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
            
    protected $connection = 'mysql_tmh';
    protected $table 	  = 'historical_trackers';
    protected $guarded    =  array('id'); //prevents people from changing the value
}