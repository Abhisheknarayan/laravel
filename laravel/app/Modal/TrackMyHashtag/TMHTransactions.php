<?php
namespace App\Modal\TrackMyHashtag;
use Illuminate\Database\Eloquent\Model;

class TMHTransactions extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $connection = 'mysql_tmh';
    protected $table 	  = 'transactions';
    protected $guarded    =  array('id'); //prevents people from changing the value
}