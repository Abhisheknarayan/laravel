<?php
namespace App\Modal\TrackMyHashtag;
use Illuminate\Database\Eloquent\Model;

class TMHUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'id','is_active','verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $connection = 'mysql_tmh';
    protected $table 	  = 'users';
    protected $guarded    =  array('id'); //prevents people from changing the value
}