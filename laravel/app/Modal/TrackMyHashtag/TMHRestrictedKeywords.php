<?php
namespace App\Modal\TrackMyHashtag;
use Illuminate\Database\Eloquent\Model;

class TMHRestrictedKeywords extends Model
{
    protected $connection   =   'mysql_tmh';
    protected $fillable     =   ['keyword'];
    protected $table        =   'restricted_keywords';
    protected $guarded      =   array('id'); //prevents people from changing the value
}
