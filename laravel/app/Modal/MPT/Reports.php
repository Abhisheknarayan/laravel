<?php
namespace App\Modal\MPT;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Reports extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $collection 	= 	'mpt_reports';
    protected $connection	=	'mongodb';
  //  protected $guarded = array('_id'); //prevents people from changing the value
}
