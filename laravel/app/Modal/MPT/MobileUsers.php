<?php
namespace App\Modal\MPT;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MobileUsers extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    protected	$connection	=	'mongodb';
    protected 	$collection = 	'mpt_mobile_users';
    protected 	$guarded 	= 	array('_id','twitter_id'); //prevents people from changing the value
}