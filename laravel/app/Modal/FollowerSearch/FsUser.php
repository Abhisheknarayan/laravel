<?php

namespace App\Modal\FollowerSearch;

use Illuminate\Database\Eloquent\Model;

class FsUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'id','is_block'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $connection = 'mysql3';
    protected $table 	  = 'users';
    protected $guarded    =  array('id'); //prevents people from changing the value
}