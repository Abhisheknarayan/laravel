<?php

namespace App\Modal\FollowerSearch;

use Illuminate\Database\Eloquent\Model;

class FsUserLimit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $connection = 'mysql3';
    protected $table      = 'user_access_limit';
}
