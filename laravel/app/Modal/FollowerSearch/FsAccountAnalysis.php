<?php

namespace App\Modal\FollowerSearch;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FsAccountAnalysis extends Eloquent
{

    protected $connection = 'mongodbfsreport';
    protected $collection = 'analyze_request';

}