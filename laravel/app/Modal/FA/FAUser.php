<?php
namespace App\Modal\FA;
use Illuminate\Database\Eloquent\Model;

class FAUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'id','is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $connection = 'mysql1';
    protected $table 	  = 'users';
    protected $guarded    =  array('id'); //prevents people from changing the value
}