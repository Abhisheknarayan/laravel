<?php

namespace App\Modal\Historical;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Historical_collections extends Eloquent
{
    
    protected $connection = 'historicaldb';
    protected $collection;
    public  $timestamps      =    false;

    public function __construct($collection=null){
    	$this->collection = $collection;
    }


}
