<?php
namespace App\Modal\Core;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserTweetsQueue extends  Eloquent
{
	protected $fillable 	=	['_id'];
	protected $guarded	 	= 	array('_id'); //prevents people from changing the value
	protected $table 		= 	'usertweetsqueue';
	protected $connection	= 	'mongodb';
	
	/** Check whether TrackUserTweets Jobs finish or not **/
}