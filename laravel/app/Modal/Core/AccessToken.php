<?php
namespace App\Modal\Core;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AccessToken extends Eloquent{
	protected $fillable 	= 	['_id','api_key','api_secret','access_token','access_token_secret','email','is_active'];
	protected $guarded 		= 	array('_id'); 
	protected $connection 	= 	'mongodb';
	protected $collection 	= 	'access_token';
}