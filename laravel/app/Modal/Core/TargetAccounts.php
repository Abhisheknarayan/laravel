<?php
namespace App\Modal\Core;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TargetAccounts extends Eloquent
{
	protected $fillable 	= 	['twitter_id','last_tweet_id'];
	protected $collection	= 	'target_accounts';
	protected $connection	=	'mongodb';
	protected $guarded 		= 	array('_id'); //prevents people from changing the value
}