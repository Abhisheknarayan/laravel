<?php
namespace App\Modal\Core;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Accounts extends Eloquent
{
	protected $fillable 	= 	['twitter_id','username','last_tweet_id','is_active'];
	protected $collection	= 	'accounts';
	protected $connection	=	'mongodb';
	protected $guarded 		= 	array('_id'); //prevents people from changing the value
}