<?php
namespace App\Modal\Core;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TwitterUsers extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected 	$fillable 	= 	[];
    protected	$connection	=	'mongodbuserdetails';
    protected	$collection = 	'twitterusers';
    
  //  protected 	$guarded 	= 	array('_id'); //prevents people from changing the value
}