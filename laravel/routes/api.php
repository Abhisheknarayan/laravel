<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1/mpt'],function(){
    Route::get('/app/keys','MPT\API\MobileAPIController@keys');
    Route::get('/verify/user/{twitter_id}','MPT\API\MobileAPIController@valid_user');

    Route::post('/subscription/store/receipt','MPT\API\InAppAPIController@subscription_receipt');
    Route::post('/product/store/receipt','MPT\API\InAppAPIController@product_receipt');
    
    Route::post('/user/add','MPT\API\UserAPIController@add_user');
    Route::get('/user/premium/{twitter_id}','MPT\API\UserAPIController@is_premium');
    Route::get('/users/search/{loggedin_twitter_id}/{query}','MPT\API\UserAPIController@get_user_search');
    Route::get('/files/{twitter_id}','MPT\API\UserAPIController@files');
    Route::get('/file/request/{twitter_id}/{target_twitter_id}','MPT\API\UserAPIController@file_request');
    
    Route::get('/user/tweets/{twitter_id}/{target_twitter_id}/{page}/{filter}','MPT\API\UserAPIController@user_tweets');  

    Route::get('/user/details/{loggedin_twitter_id}/{twitter_id}','MPT\API\UserAPIController@get_user_details');
});

/** END *******************/