<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Default */

Route::get('/', function (){
    	if(!Auth::check()){
    		return redirect('/login');
    	}else{
    		return redirect('/dashboard');

    	}
    });

Route::get('/register',function(){
    	return redirect('/login');
    });

Route::get('/password/reset',function(){
		return redirect('/login');
    });

/* END */


/** MPT **/

Route::get('/file/download/{twitter_id}/{report_twitter_id}/{hash}','MPT\DownloadController@download_file');

Route::post('/mpt/subscriptions/notification','MPT\SubsNotificationController@subscriptions_notification');

Route::group(['middleware' => ['auth','check-permission']], function (){

    $routes = DB::table('app_menus')->where(['is_deleted' => 0, 'status' =>1 ])->get();

    foreach ($routes as $key => $value) {
       
        if($value->method=='get'){
            
            Route::get($value->route,$value->controller);

        }
        else if($value->method == 'post'){

            Route::post($value->route,$value->controller);
            
        }

    }
        
});


/*** END ****/
